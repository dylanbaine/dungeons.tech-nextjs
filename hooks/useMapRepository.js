import { useContext } from "react";
import StateContext from "../context/StateContext";

export default function useMapRepository() {
  const { mapRepository } = useContext(StateContext);
  if (typeof windw !== "undefined" && window._mapRepository) {
    return window._mapRepository;
  }
  return mapRepository;
}
