import { MediaRepository } from "../media-repository";
import StateContext from "../context/StateContext";
import { useContext } from "react";

export default function useMediaRepository(): MediaRepository | null {
    const ctx = useContext(StateContext);
    if (ctx && ctx.mediaRepository) {
        return ctx.mediaRepository;
    }
    return null;
}