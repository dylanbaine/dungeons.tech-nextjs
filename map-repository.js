import nullMapsRepository from "./map-repository/nullMapsRepository";

function mapRepository() {
  let repo;
  if (typeof window === "undefined") {
    repo = nullMapsRepository;
  } else {
    repo = window._mapRepo;
  }
  return repo;
}

const repo = mapRepository();
console.log(repo.type);
export default repo;
