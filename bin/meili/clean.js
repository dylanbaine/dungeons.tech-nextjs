import meiliClient from "../../utils/meiliClient";

async function main() {
  const meili = meiliClient();
  const indexes = await meili.getIndexes();

  await Promise.all(
    indexes.map((index) => {
      return meili.deleteIndex(index.name);
    })
  );
}

main()
  .then(() => {
    process.exit();
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
