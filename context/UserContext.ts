import { useContext, createContext, useState, useEffect } from "react";
import User from "../objects/User.ts";

const UserContext = createContext({});
export default UserContext;

export function useUser(): User | null {
    const context = useContext(UserContext);
    if (context) {
        return User.fromContext(context);
    }
    return null;
}

export function useUserSubscriptionFeatures() {
    const user = useUser();
    const [features, setFeatures] = useState({});
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        if (user) {
            setLoading(true);
            user.getSubscriptionFeatures().then((_features) => {
                setFeatures(_features)
                setLoading(false);
            });
        }
    }, []);
    console.log({ features, loading });
    return [features, loading];
}
