import router from "next/router";
import { createContext, useContext, useState } from "react";
import Loader from "../components/Loader";
import useMapRepository from "../hooks/useMapRepository";
import generateACity from "../utils/generateCity";

const ContentGeneratorContext = createContext();

export function ContentGeneratorProvider(props) {
  const [loading, setLoading] = useState(false);
  const [loadingMessage, setLoadingMessage] = useState("");
  const mapRepository = useMapRepository();

  function load(message) {
    setLoading(true);
    setLoadingMessage(message);
  }

  function unload() {
    setLoading(false);
    setLoadingMessage("");
  }

  const contextValue = {
    generateACity(customData = {}) {
      load("Generating a city...");
      const cleanedCustomData =
        customData.constructor.name === "SyntheticBaseEvent" ? {} : customData;
      generateACity(cleanedCustomData, mapRepository).then((generated) => {
        router.push(`/app/map/${generated.id}`);
        setTimeout(() => {
          unload();
        }, 300);
      });
    },
  };
  return (
    <ContentGeneratorContext.Provider value={contextValue}>
      {loading && <Loader message={loadingMessage} />}
      {props.children}
    </ContentGeneratorContext.Provider>
  );
}

export function useContentGenerator() {
  const context = useContext(ContentGeneratorContext);
  return context;
}

export default ContentGeneratorContext;
