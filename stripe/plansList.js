import stripe from "./stripe";

const staticPlansList = [
  {
    name: "Free",
    features: [
      "Virtual Table Top",
      "Up to 300 Saved Objects",
      "Up to 3 Uploaded Maps",
    ],
    isFree: true,
  },
  {
    name: "Lite",
    features: [
      "Virtual Table Top",
      "Up to 1000 Saved Objects",
      "Up to 10 Uploaded Maps",
    ],
  },
  {
    name: "Premium",
    features: [
      "Virtual Table Top",
      "Unlimited Saved Objects",
      "Up to 50 Uploaded Maps",
      "Map Phases",
    ],
  },
];

export default async function plansList() {
  const stripePlans = (await stripe.plans.list()).data;
  const litePrices = stripePlans
    .filter((p) => p.nickname.toLowerCase().includes("lite"))
    .map((p) => ({
      id: p.id,
      interval: p.interval,
      price: p.amount / 100,
    }));
  const premiumPrices = stripePlans
    .filter((p) => p.nickname.toLowerCase().includes("premium"))
    .map((p) => ({
      id: p.id,
      interval: p.interval,
      price: p.amount / 100,
    }));
  return staticPlansList.map((plan) => {
    if (plan.name === "Premium") {
      const monthly = premiumPrices.find((p) => {
        return p.interval === "month";
      });
      plan.monthly = {
        id: monthly.id,
        monthlyPrice: monthly.price.toFixed(2),
        pay: monthly.price.toFixed(2),
      };
      const yearly = premiumPrices.find((p) => p.interval === "year");
      plan.yearly = {
        id: yearly.id,
        monthlyPrice: (yearly.price / 12).toFixed(2),
        pay: yearly.price,
      };
      plan.yearlyPriceId = yearly.id;
      plan.yearlyPrice = (yearly.price / 12).toFixed(2);
      plan.yearlyPay = yearly.price;
    } else if (plan.name === "Lite") {
      const monthly = litePrices.find((p) => p.interval === "month");
      plan.monthly = {
        id: monthly.id,
        monthlyPrice: monthly.price.toFixed(2),
        pay: monthly.price.toFixed(2),
      };
      const yearly = litePrices.find((p) => p.interval === "year");
      plan.yearly = {
        id: yearly.id,
        monthlyPrice: (yearly.price / 12).toFixed(2),
        pay: yearly.price,
      };
    }
    return plan;
  });
}
