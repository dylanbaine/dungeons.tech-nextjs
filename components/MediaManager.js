import React, { useState, useEffect, useRef } from "react";
import ModalContainer from "./ModalContainer";
import Card from "./Card";
import Icon from "./Icon";
import useMediaRepository from "../hooks/useMediaRepository.ts";
import compressImage from "../utils/compressImage";
import Spinner from "../components/Spinner";
import { useUser } from "../context/UserContext.ts";
import Link from "next/link";
import normalizeMediaUrl from "../utils/normalizeMediaUrl";

function MediaManager({
  onMediaUploaded = () => null,
  onMediaSelected = () => null,
}) {
  const mediaRepository = useMediaRepository();
  const [media, setMedia] = useState([]);
  const user = useUser();
  const [loading, setLoading] = useState(true);
  const [canCreate, setCanCreate] = useState(false);

  async function checkIfCanCreateMaps() {
    return new Promise(async (resolve, reject) => {
      setLoading(true);
      const canCreate = await mediaRepository.canCreateMaps();
      setCanCreate(canCreate);
      setLoading(false);
      resolve();
    });
  }
  useEffect(() => {
    (async () => {
      if (!user) return;
      const media = await mediaRepository.getAllMedia();
      await checkIfCanCreateMaps();
      if (!media.length) {
        const usedImages = await mediaRepository.getDocumentImages();
        const mediaObjects = usedImages.map((url, i) => {
          return {
            id: Date.now().toString() + i,
            title: "",
            description: "",
            url,
          };
        });
        setMedia(mediaObjects);
        mediaObjects.forEach(mediaRepository.saveMedia);
      } else {
        setMedia(media);
      }
      setLoading(false);
    })();
  }, []);

  function mediaUploaded(newMedia) {
    onMediaUploaded(newMedia);
    setMedia([...media, newMedia]);
    setTimeout(checkIfCanCreateMaps, 1000);
  }

  function deleteMedia(toDelete) {
    if (confirm("Are you sure you want to delete this media?")) {
      mediaRepository.deleteMedia(toDelete.id);
      setMedia(media.filter((m) => m.id !== toDelete.id));
      setTimeout(checkIfCanCreateMaps, 400, false);
    }
  }

  if (!user) {
    return (
      <div className="alert alert--info margin-md--bottom">
        <div className="flex">
          <Icon className="margin-md--right" name="info" /> You must have an
          account to use the media manager.
        </div>
        <Link href="/register">
          <button className="button--info margin-md--top">Sign Up</button>
        </Link>
      </div>
    );
  }

  return (
    <div>
      <h1 className="sm:flex flex--align-center">
        Media Manager {loading && <Spinner className="margin-md--left" />}
      </h1>
      <div
        style={{ maxHeight: "80vh", overflow: "auto" }}
        className="sm:flex flex--wrap margin-auto"
      >
        <AddMediaCard
          canCreate={canCreate}
          loading={loading}
          onMediaUploaded={mediaUploaded}
        />
        {media.map((media) => (
          <MediaCard
            onMediaDelete={deleteMedia}
            onMediaSelected={onMediaSelected}
            key={media.id + media.url}
            media={media}
          />
        ))}
      </div>
    </div>
  );
}

function AddMediaCard({ onMediaUploaded, canCreate }) {
  const inputRef = useRef();
  const mediaRepository = useMediaRepository();
  const [loading, setLoading] = useState(false);

  async function fileInput(e) {
    setLoading(true);
    const reader = new FileReader();
    if (!e.target.files[0]) return;
    const file = await compressImage(e.target.files[0]);
    if (!file) return;
    reader.onloadend = async () => {
      try {
        const newMedia = {
          id: Date.now().toString(),
          title: "",
          description: "",
        };
        await mediaRepository.saveMedia(newMedia, file);
        onMediaUploaded(newMedia);
        setLoading(false);
      } catch (e) {
        alert("Sorry, there was an error with the uploding of this image.");
      }
    };
    reader.readAsDataURL(file);
  }

  if (loading) {
    return (
      <div className="map-preview map-preview--manager sm:flex flex--all-center">
        <div>
          <Spinner />
        </div>
      </div>
    );
  }

  return (
    <div
      onClick={() => {
        if (canCreate) {
          !loading && inputRef.current.click();
        } else {
          if (
            confirm(
              "You have reached your storage limit. Update your subscription to create more."
            )
          ) {
            window.location.href = "/pricing";
          }
        }
      }}
      className="pointer map-preview map-preview--manager"
      style={addMediaCardStyle}
    >
      <div>
        {loading ? <Spinner /> : <Icon style={{ color: "white" }} name="add" />}
        {!loading && (
          <input
            accept="image/*"
            onInput={fileInput}
            style={{ display: "none" }}
            ref={inputRef}
            type="file"
          ></input>
        )}
      </div>
    </div>
  );
}
const addMediaCardStyle = {
  background: "#afafaf",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

function MediaCard({
  media,
  onMediaSelected = () => null,
  onMediaDelete = () => null,
}) {
  return (
    <div
      onClick={() => onMediaSelected(media)}
      className="map-preview map-preview--manager pointer"
      style={mediaCardStyle(media)}
    >
      <div className="flex absolute top right">
        <button
          onClick={(e) => {
            e.stopPropagation();
            onMediaDelete(media);
          }}
          className="button--small button--error"
        >
          <Icon className="size-sm" name="delete" />
        </button>
      </div>
    </div>
  );
}
const mediaCardStyle = (media) => ({
  background: `url(${normalizeMediaUrl(media.url)})`,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  backgroundPosition: "center",
  position: "relative",
});

function MediaManagerModal(props) {
  const [showing, setShowing] = useState(false);

  useEffect(() => {
    function eventListener() {
      setShowing(true);
    }
    document.addEventListener(
      "showMediaManagerModal" + props.uid,
      eventListener
    );
    return () => {
      document.removeEventListener(
        "showMediaManagerModal" + props.uid,
        eventListener
      );
    };
  });

  function mediaSelected(media) {
    props.onMediaSelected && props.onMediaSelected(media);
    setShowing(false);
  }

  return (
    showing && (
      <ModalContainer onSelfHide={setShowing}>
        <Card
          className="relative"
          style={{ minWidth: "300px", maxWidth: "850px", margin: "auto" }}
        >
          <div className="text-right">
            <Icon
              className="pointer"
              name="close"
              onClick={() => setShowing(false)}
            />
          </div>
          <MediaManager {...props} onMediaSelected={mediaSelected} />
        </Card>
      </ModalContainer>
    )
  );
}
MediaManager.Modal = MediaManagerModal;

function MediaManagerModalButton({
  children = "Media Manager",
  onMediaSelected = () => null,
  onMediaUploaded = () => null,
  ...props
}) {
  const uid =
    Math.random().toString(36).substring(2, 15) +
    Math.random().toString(36).substring(2, 15);
  return (
    <React.Fragment>
      <MediaManagerModal
        onMediaUploaded={onMediaUploaded}
        onMediaSelected={onMediaSelected}
        uid={uid}
        {...props}
      />
      <button
        onClick={() => {
          document.dispatchEvent(
            new CustomEvent("showMediaManagerModal" + uid)
          );
        }}
        {...props}
      >
        {children}
      </button>
    </React.Fragment>
  );
}
MediaManager.ModalButton = MediaManagerModalButton;

export default MediaManager;
