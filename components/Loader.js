import styles from "./Loader.module.scss";

export default function Loader({ message }) {
  return (
    <div>
      <div className={styles["message-container"]}>
        <div className={styles["message"]}>{message}</div>
      </div>
      <div className={styles["lines-container"]}>
        <div>
          <div className={styles["spinner"]}></div>
          <div className={styles["spinner"]}></div>
          <div className={styles["spinner"]}></div>
        </div>
      </div>
    </div>
  );
}
