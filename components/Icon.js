// https://fonts.google.com/icons
export default function Icon({ name, className = "", style = {}, ...props }) {
  return (
    <span
      style={style}
      className={`${className} material-icons`.trim()}
      {...props}
    >
      {name}
    </span>
  );
}
