import { useState, useEffect, useMemo, useRef } from "react";
import styles from "./SearchUserContent.module.scss";
import Card from "./Card";
import { useUser } from "../context/UserContext.ts";
import Link from "next/link";

function defaultRenderHit(hit, onItemClick) {
  return (
    <Card
      onClick={() => onItemClick(hit)}
      className={styles["hit"]}
      key={hit.id}
    >
      {hit.name} ({hit.type})
    </Card>
  );
}

function SearchUserContent({
  onItemClick = (hit) => null,
  inputProps = "Search all of your content...",
  renderHit = defaultRenderHit,
  showRegister = true,
}) {
  const user = useUser();
  const [searchValue, setSearchValue] = useState("");
  const [currentTimout, setCurrentTimout] = useState(null);
  const [searchResults, setSearchResults] = useState({});
  const [inputFocused, setInputFocused] = useState(false);
  const [hoveringOnResults, setHoveringOnResults] = useState(false);
  const searchInput = useRef();
  function onSearchInputChange({ target: { value } }) {
    setSearchValue(value);
  }

  function onClearButtonClick() {
    setSearchValue("");
  }

  function onInputFocus(e) {
    setInputFocused(true);
  }

  function onInputBlur(e) {
    setInputFocused(false);
  }

  useEffect(() => {
    function listenForEvent(e) {
      if (inputFocused && e.code === "Escape") {
        searchInput.current.blur();
      }
    }
    document.addEventListener("keyup", listenForEvent);
    return () => document.removeEventListener("keyup", listenForEvent);
  }, [inputFocused]);

  useEffect(() => {
    function listenForEvents(e) {
      const { target = {} } = e;
      if (
        (target.classList && target.classList.contains("ck")) ||
        target.value !== undefined
      ) {
        return;
      }
      if (e.code === "Slash") {
        searchInput.current.focus();
      }
    }
    document.addEventListener("keyup", listenForEvents);
    return () => document.removeEventListener("keyup", listenForEvents);
  }, []);

  useEffect(() => {
    clearTimeout(currentTimout);
    setCurrentTimout(
      setTimeout(async () => {
        const searchResults = user && (await user.meili().search(searchValue));
        setSearchResults(searchResults);
      }, 500)
    );
  }, [searchValue]);

  const showHits = useMemo(() => {
    return (
      inputFocused ||
      (inputFocused && searchValue.length > 0) ||
      hoveringOnResults
    );
  }, [inputFocused, searchValue, hoveringOnResults]);

  if (!user) {
    return showRegister ? (
      <div>
        <p>Create an account to search all of your content. </p>
        <Link href="/register">
          <button className="button--success">Register</button>
        </Link>
      </div>
    ) : null;
  }
  return (
    <div className={styles["container"]}>
      <div className={styles["input-wrap"]}>
        <input
          ref={searchInput}
          style={{
            width: searchValue.length === 0 && "100%",
          }}
          placeholder={inputProps}
          onBlur={onInputBlur}
          onFocus={onInputFocus}
          type="text"
          className={styles["input"]}
          value={searchValue}
          onInput={onSearchInputChange}
        />
        {searchValue.length > 0 && (
          <button
            onClick={onClearButtonClick}
            className={`${styles["clear-button"]} button--error`}
          >
            CLEAR
          </button>
        )}
      </div>
      {showHits && searchResults.hits && (
        <Card
          onMouseEnter={() => setHoveringOnResults(true)}
          onMouseLeave={() => setHoveringOnResults(false)}
          className={styles["results"]}
        >
          {searchResults.hits.map((hit) => renderHit(hit, onItemClick))}
        </Card>
      )}
    </div>
  );
}

export default SearchUserContent;
