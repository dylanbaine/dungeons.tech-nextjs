export default function WysiwygEditor({
  data = "",
  onDescriptionChange = () => null,
}) {
  if (typeof window == "undefined") {
    return null;
  }
  const { CKEditor } = require("@ckeditor/ckeditor5-react");
  const ClassicEditor = require("@ckeditor/ckeditor5-build-classic");
  return (
    <CKEditor
      editor={ClassicEditor}
      data={`${data}`}
      onChange={onDescriptionChange}
    />
  );
}
