import { useEffect, useState } from "react";
import { useUser } from "../context/UserContext.ts";
import EmailLeadForm from "./EmailLeadForm";

const style = {
  position: "fixed",
  bottom: "10px",
  left: "10px",
  backgroundColor: "#fff",
};

export default function LeadGenEmailForm() {
  const user = useUser();
  const [foceClosed, setFoceClosed] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      document.cookie = "leadgen=true; max-age=31536000";
    }, 1500);
  }, []);
  return null;

  if (typeof document === "undefined" || user) {
    return null;
  }
  if (
    document.cookie.includes("leadgen") ||
    window.location.href.includes("price") ||
    foceClosed ||
    user
  ) {
    return null;
  }
  return (
    <div style={style}>
      <EmailLeadForm
        formUrl="/api/generic-lead"
        text="Sign up for updates from Dungeons.Tech"
        actions={() => (
          <button onClick={() => setFoceClosed(true)} className="button--error">
            Close
          </button>
        )}
      />
    </div>
  );
}
