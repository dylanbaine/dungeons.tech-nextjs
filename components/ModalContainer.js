import { useEffect, useState } from "react";
import styles from "./ModalContainer.module.scss";

export default function ModalContainer(props) {
  const [containerStyle, setContainerStyle] = useState({});
  const onSelfHide = props.onSelfHide;

  function hideModal() {
    setContainerStyle({ ...containerStyle, top: "-20px", opacity: "0" });
    setTimeout(() => {
      onSelfHide && onSelfHide(false);
    }, 500);
  }

  function hideModalKeyup(e) {
    if (e.code === "Escape") {
      hideModal();
    }
  }

  useEffect(() => {
    setContainerStyle({ ...containerStyle, top: "0", opacity: "1" });
    document.addEventListener("keyup", hideModalKeyup);
    document.addEventListener("hideModal", hideModal);
    return () => {
      document.removeEventListener("hideModal", hideModal);
      document.removeEventListener("keyup", hideModalKeyup);
    };
  }, []);

  return (
    <div
      onClick={() => {
        onSelfHide && hideModal();
      }}
      style={{ ...props.style, ...containerStyle }}
      className={styles["container"]}
    >
      <div onClick={(e) => e.stopPropagation()}>{props.children}</div>
    </div>
  );
}
