import Card from "./Card";
import { useState } from "react";
import axios from "axios";

export default function EmailLeadForm({ formUrl, text, actions = () => null }) {
  const [email, setEmail] = useState("");
  const [hasJoinedEmail, setHasJoinedEmail] = useState(false);

  async function postMarketingLead() {
    gtag("event", "conversion", {
      send_to: "AW-626684793/O4-eCKngg4MDEPnm6aoC",
    });
    if (email && email.length && email.includes("@")) {
      await axios.post(formUrl, { email });
      setHasJoinedEmail(true);
    }
  }
  return hasJoinedEmail ? (
    <div className="success-state padded-md">
      <p>Thanks for joining!</p>
    </div>
  ) : (
    <Card style={{ maxWidth: "100%", margin: "auto" }}>
      <strong>{text}</strong>
      <form
        style={{ marginTop: "8px" }}
        onSubmit={(e) => {
          e.preventDefault();
          postMarketingLead();
        }}
      >
        <input
          style={{ display: "block", maxWidth: "90%", width: "300px" }}
          type="email"
          id="email"
          placeholder="john@example.com"
          onChange={(e) => setEmail(e.target.value)}
        />
        <div className="flex flex-wrap align-center">
          <button type="submit">Submit</button>
          {actions()}
        </div>
      </form>
    </Card>
  );
}
