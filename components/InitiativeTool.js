import { useEffect, useRef, useState, useMemo } from "react";
import styles from "./InitiativeTool.module.scss";
import Card from "./Card";
import {
  addItemToPersistedInitiative,
  getPersistedInitiative,
  setPersistedInitiative,
} from "../user-data-repository";
import listenForKeyInput from "../utils/listenForKeyInput";
import Icon from "./Icon";
import router, { useRouter } from "next/router";

function Member({
  member,
  onMemberUpdate = () => null,
  onMemberDeleteButtonClick = () => null,
  active = false,
}) {
  const [name, setName] = useState(null);
  const [initiative, setInitiative] = useState(null);
  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setName(member.name);
    setInitiative(member.initiative);
    setMounted(true);
  }, [member]);

  useEffect(() => {
    if (mounted) {
      const windowVarKey = `_initiative_tool_${member.id}`;
      clearTimeout(window[windowVarKey]);
      window[windowVarKey] = setTimeout(() => {
        if (name && initiative) {
          onMemberUpdate({ ...member, name, initiative });
        }
      }, 500);
    }
  }, [name, initiative]);

  function onInitiativeInput(e) {
    if (!e.target.value) {
      setInitiative("");
    } else {
      setInitiative(parseInt(e.target.value));
    }
  }

  function onNameInput(e) {
    setName(e.target.value);
  }

  return (
    <tr
      style={{
        border: active && "solid 1px var(--info)",
      }}
      key={member.id}
    >
      <td>
        {initiative !== null && (
          <input
            type="number"
            value={initiative}
            onInput={onInitiativeInput}
          ></input>
        )}
      </td>
      <td>
        {name !== null && (
          <input type="text" value={name} onInput={onNameInput}></input>
        )}
      </td>
      <td>
        <button
          onClick={() => onMemberDeleteButtonClick(member.id)}
          className="button--error button--small"
        >
          Delete
        </button>
      </td>
    </tr>
  );
}

const initialNewMember = { name: "", initiative: "" };
export default function InitiativeTool() {
  const [showing, setShowing] = useState(false);
  const [members, setMembers] = useState([]);
  const [newMember, setNewMember] = useState(initialNewMember);
  const [currentIndex, setCurrentIndex] = useState(0);
  const newMemberInitiativeRef = useRef();
  const router = useRouter();

  const membersSorted = useMemo(() => {
    return Array.isArray(members)
      ? members.sort((a, b) => b.initiative - a.initiative)
      : [];
  }, [members]);

  useEffect(() => {
    (async () => {
      setMembers(await getPersistedInitiative());
    })();
  }, []);

  useEffect(() => {
    const event = new CustomEvent("publishInitiativeToVtt");
    event.members = membersSorted;
    event.currentIndex = currentIndex;
    document.dispatchEvent(event);
  }, [currentIndex, membersSorted]);

  function increaseCurrentIndex() {
    console.log(currentIndex);
    if (members.length === currentIndex + 1) {
      setCurrentIndex(0);
    } else {
      setCurrentIndex(currentIndex + 1);
    }
  }

  function decreaseCurrentIndex() {
    if (currentIndex === 0) {
      setCurrentIndex(members.length - 1);
    } else {
      setCurrentIndex(currentIndex - 1);
    }
  }

  function onInitiativeButtonClick() {
    setShowing(true);
  }

  function onCloseButtonClick() {
    setShowing(false);
  }

  function onMemberDeleteButtonClick(id) {
    const newMembers = members.filter((member) => member.id !== id);
    setMembers(newMembers);
    setPersistedInitiative(newMembers);
  }

  function onInitiativeInput(e) {
    setNewMember({
      ...newMember,
      initiative: parseInt(e.target.value),
    });
  }

  function onNameInput(e) {
    setNewMember({
      ...newMember,
      name: e.target.value,
    });
  }

  function addNewInitiativeMember() {
    if (!newMember.name || !newMember.initiative) {
      return;
    }
    newMember.id = Date.now();
    addItemToPersistedInitiative(newMember);
    members.push(newMember);
    setNewMember(initialNewMember);
    newMemberInitiativeRef.current.focus();
  }

  function onMemberUpdate(newMemberData) {
    const newMembers = members.map((member) => {
      if (member.id == newMemberData.id) {
        return newMemberData;
      }
      return member;
    });
    setMembers(newMembers);
    setPersistedInitiative(newMembers);
  }

  if (router.asPath.includes("/published-vtt")) {
    return null;
  }

  return (
    <div>
      {!showing && (
        <button onClick={onInitiativeButtonClick} className={styles.button}>
          Initiative
        </button>
      )}
      {showing && (
        <Card className={styles.card}>
          <div>
            <div className="sm:flex flex--justify-end flex--items-center">
              {router.asPath.includes("/app/map/") && (
                <button
                  className="button--small"
                  onClick={() => {
                    const event = new CustomEvent("publishInitiativeToVtt");
                    event.members = membersSorted;
                    event.currentIndex = currentIndex;
                    document.dispatchEvent(event);
                  }}
                >
                  <Icon name="share" className="pointer" />
                  Publish To VTT
                </button>
              )}
              <Icon
                name="close"
                onClick={onCloseButtonClick}
                className="pointer"
              />
            </div>
            <h2>Initiative</h2>
            <div className="sm:flex flex--justify-center">
              <Icon
                onClick={decreaseCurrentIndex}
                className="pointer"
                name="chevron_left"
              />
              <Icon
                onClick={increaseCurrentIndex}
                className="pointer"
                name="chevron_right"
              />
            </div>
            <table style={{ width: "100%" }}>
              <thead>
                <tr style={{ fontWeight: "bold", width: "100px" }}>
                  <td>Initiative</td>
                  <td>Name</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {membersSorted.map((member, i) => (
                  <Member
                    active={i === currentIndex}
                    onMemberDeleteButtonClick={onMemberDeleteButtonClick}
                    onMemberUpdate={onMemberUpdate}
                    key={member.id}
                    member={member}
                  />
                ))}
                <tr>
                  <td>
                    <input
                      ref={newMemberInitiativeRef}
                      type="number"
                      value={newMember.initiative}
                      onInput={onInitiativeInput}
                    />
                  </td>
                  <td>
                    <input
                      value={newMember.name}
                      onInput={onNameInput}
                      onKeyUp={(e) =>
                        listenForKeyInput(e, "Enter", addNewInitiativeMember)
                      }
                      type="text"
                    />
                  </td>
                  <td>
                    <button
                      onClick={addNewInitiativeMember}
                      style={{
                        marginLeft: "auto",
                        display: "block",
                      }}
                    >
                      Add
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
            <button
              className="button--error"
              onClick={() => {
                setPersistedInitiative([]);
                setMembers([]);
              }}
            >
              Delete all initiative members
            </button>
          </div>
        </Card>
      )}
    </div>
  );
}
