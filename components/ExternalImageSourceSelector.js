/* eslint-disable  @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import styles from "./ExternalImageSourceSelector.module.scss";

import getSubredditImagePosts from "../utils/getSubredditImagePosts";
import Icon from "./Icon";
import Card from "./Card";
import ModalContainer from "./ModalContainer";
import Spinner from "./Spinner";

function formatRedditPostForExternalImagesource(post) {
  return {
    thumb: post.data.thumbnail,
    source: post.data.url_overridden_by_dest,
  };
}

export default function ExternalImageSourceSelector({
  onImageSelect = () => null,
}) {
  const [gridLoding, setGridLoading] = useState(false);
  const [externalImageSources, setExternalImageSources] = useState([]);
  const [externalImageSrouceNextPage, setExternalImageSourceNextPage] =
    useState("");
  const [externalImageSrouceLastPage, setExternalImageSourceLastPage] =
    useState("");
  const [externalImageSourceName, setExternalImageSourceName] = useState();
  const [externalImageSearch, setExternalImageSearch] = useState("");

  useEffect(() => {
    if (externalImageSources.length > 0) {
      const timeoutKey = "_search_external_image_source_timout";
      clearTimeout(window[timeoutKey]);
      window[timeoutKey] = setTimeout(() => {
        window._current_subreddit = null;
        loadRedditImages(externalImageSourceName, false, externalImageSearch);
      }, 500);
    }
  }, [externalImageSearch]);

  function loadRedditImages(subReddit, goBack = false, search) {
    const newSubreddit = subReddit !== window._current_subreddit;
    if (newSubreddit) {
      setExternalImageSourceLastPage("");
      setExternalImageSourceNextPage("");
    }
    setGridLoading(true);
    setExternalImageSourceName(subReddit);
    getSubredditImagePosts(
      subReddit,
      newSubreddit
        ? ""
        : goBack
        ? externalImageSrouceLastPage
        : externalImageSrouceNextPage,
      goBack,
      search
    ).then((response) => {
      window._current_subreddit = subReddit;
      setExternalImageSourceLastPage(
        newSubreddit ? "" : externalImageSrouceNextPage
      );
      setExternalImageSourceNextPage(response.data.after);
      const images = response.data.children
        .filter(
          (post) =>
            post.data.url_overridden_by_dest &&
            post.data.url_overridden_by_dest.match(/\.(png|jpg)/g)
        )
        .map(formatRedditPostForExternalImagesource);
      setExternalImageSources(images);
      setGridLoading(false);
    });
  }

  async function selectExternalImage(image, event) {
    onImageSelect(image.source);
    setExternalImageSources([]);
  }

  async function searchExternalImageSource(e) {
    // setHasAlteredImageSearch(true);
    setExternalImageSearch(e.target.value);
  }

  return (
    <div>
      <div className={styles.container}>
        {
          <div>
            <button
              onClick={() => loadRedditImages("fantasymaps")}
              className={`button--small ${
                externalImageSourceName === "fantasymaps" ? "button--info" : ""
              }`.trim()}
            >
              Choose from r/fantasymaps
            </button>
            <button
              onClick={() => loadRedditImages("battlemaps")}
              className={`button--small ${
                externalImageSourceName === "battlemaps" ? "button--info" : ""
              }`.trim()}
            >
              Choose from r/battlemaps
            </button>
          </div>
        }
      </div>
      {externalImageSources.length > 0 && (
        <ModalContainer>
          <Card style={{ maxWidth: "900px" }}>
            <div className="flex justify-center">
              {gridLoding && <Spinner />}
            </div>
            <div className="flex">
              <h2>Select a map.</h2>
              <button
                onClick={() => {
                  setExternalImageSources([]);
                  setExternalImageSourceName("");
                }}
                className="button--error button--small margin-auto--left"
              >
                <Icon name="close" />
              </button>
            </div>
            <fieldset className="flex">
              <input
                value={externalImageSearch}
                className="input--full-width"
                placeholder="Search..."
                id="search-input"
                onInput={searchExternalImageSource}
              />
              {externalImageSearch && (
                <button
                  onClick={() => setExternalImageSearch("")}
                  className="button--small button--error"
                >
                  Clear
                </button>
              )}
            </fieldset>
            <div className={styles["external-map-list"]}>
              {externalImageSources
                .filter((i) => i.thumb !== "default")
                .map((image) => (
                  <div
                    key={image.thumb}
                    className={styles["external-map-image"]}
                  >
                    <img
                      onClick={(event) => selectExternalImage(image, event)}
                      className="pointer margin-md--bottom"
                      width="200px"
                      src={image.thumb}
                      alt=""
                    ></img>
                  </div>
                ))}
            </div>
            <div className="flex margin-md--top">
              {externalImageSrouceLastPage && externalImageSources.length > 0 && (
                <button
                  style={{
                    width: "40%",
                  }}
                  onClick={() =>
                    loadRedditImages(externalImageSourceName, true)
                  }
                >
                  <Icon name="chevron_left" /> Previous Page
                </button>
              )}
              <button
                className="justify-end"
                style={{
                  width: "40%",
                  marginLeft: "auto",
                }}
                onClick={() => loadRedditImages(externalImageSourceName)}
              >
                Next Page <Icon name="chevron_right" />
              </button>
            </div>
          </Card>
        </ModalContainer>
      )}
    </div>
  );
}
