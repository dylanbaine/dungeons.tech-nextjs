import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useContentGenerator } from "../context/ContentGeneratorContext";
import { useUser } from "../context/UserContext.ts";
import useMapRepository from "../hooks/useMapRepository";
import styles from "./FrontEndMenu.module.scss";
import Hamburger from "./svg/Hamburger";

function ListItem({ item }) {
  return (
    <li className={styles["list-item"]}>
      {item.onClick ? (
        <span className={styles["list-item__link"]} onClick={item.onClick}>
          {item.text}
        </span>
      ) : (
        <Link passHref={true} href={item.href}>
          <a title={item.title} className={styles["list-item__link"]}>
            {item.text}
          </a>
        </Link>
      )}
      {item.children && Array.isArray(item.children) && (
        <List items={item.children} role="child" />
      )}
    </li>
  );
}

function List({ items, role = "main" }) {
  return (
    <ul className={`${styles["list"]} ${styles[`list--${role}`]}`}>
      {items.map((item, key) => (
        <ListItem key={`${key}--${item.href}`} item={item} />
      ))}
    </ul>
  );
}

export default function FrontEndMenu(props) {
  const [mobileOpened, setMobileOpened] = useState(false);
  const currentUser = useUser();
  const router = useRouter();
  const generator = useContentGenerator();
  const [hasMaps, setHasMaps] = useState(false);
  const { count: mapsCount } = useMapRepository();

  useEffect(() => {
    function routeChanged(e) {
      setMobileOpened(false);
    }
    mapsCount().then((count) => setHasMaps(count > 0));
    router.events.on("complete", routeChanged);
    return () => {
      router.events.off("complete", routeChanged);
    };
  }, []);
  const navItems = [
    {
      text: "Dungeons.Tech",
      href: "/",
    },
    {
      text: "Pricing",
      href: "/pricing",
    },
    !currentUser &&
      !hasMaps && {
        text: "GENERATE A CITY",
        onClick() {
          generator.generateACity();
        },
      },
    {
      text: "Dungeons & Dragons Content",
      href: "/explore-content",
      children: [
        {
          text: "Oneshot Escapades Podcast",
          title: "A podcast where we play and review modules we find online.",
          href: "/oneshot-escapades",
        },
        {
          text: "Dungeons and Dragons News",
          title: "News we've gathered from around the internet.",
          href: "/dungeons-and-dragons-news",
        },
      ],
    },
  ].filter(Boolean);
  if (currentUser) {
    navItems.push({
      text: "Dashboard",
      href: "/dashboard",
    });
  } else {
    navItems.push({
      text: "Register",
      href: "/register",
    });
    navItems.push({
      text: "Log In",
      href: "/login",
    });
  }
  return (
    <>
      <button
        onClick={() => setMobileOpened(!mobileOpened)}
        className={styles["menu-button"]}
      >
        <Hamburger />
      </button>
      <List role={mobileOpened ? "opened" : "main"} items={navItems} />
    </>
  );
}
