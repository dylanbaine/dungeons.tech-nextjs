import FrontEndMenu from "./FrontEndMenu";

export default function FrontEndLayout({ children }) {
  return (
    <>
      <FrontEndMenu />
      {children}
    </>
  );
}
