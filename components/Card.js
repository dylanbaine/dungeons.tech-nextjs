import styles from "./Card.module.scss";
export default function Card(props) {
  return (
    <div
      {...props}
      style={props.style || {}}
      className={`${styles.card} ${props.className}`.trim()}
    >
      {props.children}
    </div>
  );
}
