import { useRouter } from "next/dist/client/router";
import { useEffect } from "react";

export default function RouteRedirect(to, status = 302) {
  function RedirectPageComponent() {
    const router = useRouter();
    useEffect(() => {
      router.push(to);
    }, []);

    return `redirecting to ${to}`;
  }
  RedirectPageComponent.getInitialProps = ({ res }) => {
    if (res) {
      res.writeHead(status, { Location: "/oneshot-escapades" });
      res.end();
    }
    return {};
  };
  return RedirectPageComponent;
}
