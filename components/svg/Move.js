function Move({ state = "info", ...props }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      viewBox="0 0 32 32"
      {...props}
    >
      <g>
        <path
          className={`state-${state}`}
          d="M24 20.8c0-.1 0-.3-.1-.4 0 0 0-.1-.1-.1-.1-.1-.1-.2-.2-.2l-2.7-2.2c-.4-.3-1.1-.3-1.4.2-.3.4-.3 1.1.2 1.4l.5.4h-.9c-.2 0-.5-.1-.6-.4l-3.5-7.8c-.4-.9-1.4-1.6-2.4-1.6H9c-.6 0-1 .4-1 1s.4 1 1 1h3.8c.2 0 .5.1.6.4l3.5 7.8c.4.9 1.4 1.6 2.4 1.6h.9l-.5.4c-.4.3-.5 1-.2 1.4.2.2.5.4.8.4.2 0 .4-.1.6-.2l2.7-2.2.2-.2.1-.1c.1-.3.1-.4.1-.6z"
        ></path>
        <path
          className={`state-${state}`}
          d="M15.2 17.8c-.5-.2-1.1 0-1.3.5l-.5 1.2c-.1.2-.3.4-.6.4H9c-.6 0-1 .4-1 1s.4 1 1 1h3.8c1 0 2-.6 2.4-1.6l.5-1.2c.2-.5 0-1.1-.5-1.3zM23.8 10.6c-.1-.1-.1-.2-.2-.2l-2.7-2.2c-.4-.3-1.1-.3-1.4.2-.3.4-.3 1.1.2 1.4l.5.4h-.9c-1 0-2 .6-2.4 1.6l-.5 1.2c-.2.5 0 1.1.5 1.3.1.1.3.1.4.1.4 0 .7-.2.9-.6l.5-1.2c.1-.2.3-.4.6-.4h.9l-.5.4c-.4.3-.5 1-.2 1.4.2.2.5.4.8.4.2 0 .4-.1.6-.2l2.7-2.2.2-.2.1-.1c.1-.1.1-.3.1-.4s0-.4-.2-.7c.1.1.1.1 0 0z"
        ></path>
        <path
          className={`state-${state}`}
          d="M16 2C8.3 2 2 8.3 2 16s6.3 14 14 14 14-6.3 14-14S23.7 2 16 2zm0 26C9.4 28 4 22.6 4 16S9.4 4 16 4s12 5.4 12 12-5.4 12-12 12z"
        ></path>
      </g>
    </svg>
  );
}

export default Move;
