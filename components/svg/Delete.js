// https://freeicons.io/icon-list/multimedia-2#
// https://svg2jsx.com/

function Delete({ state = "info", ...props }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      viewBox="0 0 32 32"
      {...props}
    >
      <g>
        <path
          className={`state-${state}`}
          d="M30 10c0-1.7-1.4-3-3-3h-4.7c-.7-2.9-3.2-5-6.3-5-3.1 0-5.6 2.1-6.3 5H5.1c-1.7 0-3 1.4-3 3s1.3 2.9 2.9 3v13c0 2.2 1.8 4 4 4h14c2.2 0 4-1.8 4-4V13c1.7 0 3-1.3 3-3zM16 4c2 0 3.6 1.2 4.2 3h-8.5c.7-1.8 2.3-3 4.3-3zm9 22c0 1.1-.9 2-2 2H9c-1.1 0-2-.9-2-2V13h18v13zm2-15H5.1c-.6 0-1-.5-1-1 0-.6.5-1 1-1H27c.6 0 1 .5 1 1 0 .6-.5 1-1 1z"
        ></path>
        <path
          className={`state-${state}`}
          d="M16 27c.6 0 1-.4 1-1V15c0-.6-.4-1-1-1s-1 .4-1 1v11c0 .6.4 1 1 1zM11 25c.6 0 1-.4 1-1v-7c0-.6-.4-1-1-1s-1 .4-1 1v7c0 .6.4 1 1 1zM21 25c.6 0 1-.4 1-1v-7c0-.6-.4-1-1-1s-1 .4-1 1v7c0 .6.4 1 1 1z"
        ></path>
      </g>
    </svg>
  );
}

export default Delete;
