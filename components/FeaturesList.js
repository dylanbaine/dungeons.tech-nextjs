import styles from "./FeaturesList.module.scss";
import { useRef, useEffect } from "react";
import Card from "./Card";
import Link from "next/link";
import Image from "next/image";

function ExampleVideo({ src, playbackRate = 2 }) {
  const video = useRef();

  useEffect(() => {
    // try {
    //   video.current.play();
    // } catch (e) {}
    video.current.loop = true;
    video.current.playbackRate = playbackRate;
  }, [video, playbackRate]);

  return (
    <Card style={{ padding: "0px" }}>
      <video
        style={{ width: "100%" }}
        ref={video}
        src={src}
        autoPlay
        loop
      ></video>
    </Card>
  );
}

function NPCHighlight(props) {
  return (
    <div className="flex flex--wrap">
      <div className="column text-right">
        <h2>Generate NPCs</h2>
        <p>
          Add unique NPC&apos;s with personalities to your setting on the fly.
        </p>
      </div>
      <div className="column">
        <ExampleVideo src="/media/generate-content.mov" />
      </div>
      <div className="flex margin-auto">
        <Card className="margin-md">
          <h3>Manage Hit Points</h3>
          <a className="block text-center" href="/media/hit-points.png">
            <Image
              className="margin-auto"
              width={200}
              height={100}
              alt="Manage Hit Points"
              src="/media/hit-points.png"
            />
          </a>
        </Card>
        <Card className="margin-md">
          <h3>Manage Spells</h3>
          <a className="block text-center" href="/media/manage-spells.png">
            <Image
              className="margin-auto"
              width={200}
              height={200}
              alt="Manage Spells"
              src="/media/manage-spells.png"
            />
          </a>
        </Card>
        <Card className="margin-md">
          <h3>Manage Equipment</h3>
          <a className="block text-center" href="/media/manage-equipment.png">
            <Image
              className="margin-auto"
              width={200}
              height={100}
              alt="Manage Equipment"
              src="/media/manage-equipment.png"
            />
          </a>
        </Card>
      </div>
    </div>
  );
}

function MapPhaseHighlight(props) {
  return (
    <div className="flex flex--wrap">
      <div className="column">
        <ExampleVideo src="/media/map-phases.mov" />
      </div>
      <div className="column">
        <h2>Map Phases</h2>
        <p>Use phases to add suspense to your encounters.</p>
        {props.cta && (
          <Link passHref href="/pricing">
            <a>
              <button className="button--primary">Sign up for Premium</button>
            </a>
          </Link>
        )}
      </div>
    </div>
  );
}

function ShopsHighlight(props) {
  return (
    <div className="flex flex--wrap">
      <div className="column text-right">
        <h2>Generate Shops and Inventory</h2>
        <p>
          Add taverns, blacksmiths, or shops to your maps on the fly with the
          click of a button.
        </p>
      </div>
      <div className="column">
        <ExampleVideo src="/media/generate-shop.mov" />
      </div>
    </div>
  );
}

export default function FeaturesList({ cta = true }) {
  return (
    <div className={styles["container"]}>
      <NPCHighlight cta={cta} />
      <MapPhaseHighlight cta={cta} />
      <ShopsHighlight cta={cta} />
    </div>
  );
}
