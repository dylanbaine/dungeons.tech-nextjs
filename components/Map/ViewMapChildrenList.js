import Link from "next/link";
import { useState } from "react";
import { detailLink } from "../../utils/pointLlinks";
import Card from "../Card";

export default function ViewMapChildrenList({
  mapChildren = [],
  preview = false,
}) {
  return (
    <div>
      {mapChildren.map((child) => (
        <Card key={child.id} style={{ marginBottom: "6px" }}>
          <Link href={detailLink(child)}>
            <a>
              {preview ? (
                <div>
                  <p>
                    {child.name} {child.type && `(${child.type})`}
                  </p>
                  <div
                    dangerouslySetInnerHTML={{ __html: child.description }}
                  ></div>
                </div>
              ) : (
                <span>{child.name}</span>
              )}
            </a>
          </Link>
        </Card>
      ))}
    </div>
  );
}
