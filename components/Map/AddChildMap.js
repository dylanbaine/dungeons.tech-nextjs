/* eslint-disable  @next/next/no-img-element */

import { useContext, useEffect, useRef, useState } from "react";
import WysiwygEditor from "../WysiwygEditor";
import styles from "./AddChildMap.module.scss";
import MapContext from "../../context/MapContext";
import ModalContainer from "../ModalContainer";
import hideCurrentModal from "../../utils/hideCurrentModal";
import router from "next/router";
import { generateRandomCityName } from "../../utils/generate";
import useMapRepository from "../../hooks/useMapRepository";
import MediaManager from "../MediaManager";

export default function AddChildMap({ parentMap, redirect = true }) {
  const { setAddingChild, pendingPosition, reloadMaps } =
    useContext(MapContext);
  const { storeUserMap } = useMapRepository();

  const [newMap, setNewMap] = useState({
    name: "",
    description: "",
    image: "",
  });

  useEffect(() => {
    generateRandomName();
  }, []);

  function generateRandomName() {
    setNewMap({
      ...newMap,
      name: generateRandomCityName(),
    });
  }

  const [formErrors, setFormErrors] = useState({});
  const formRef = useRef();

  function onNameInput(e) {
    setNewMap({ ...newMap, name: e.target.value });
  }

  function onDescriptionChange(event, editor) {
    const description = editor.getData();
    setNewMap({ ...newMap, description });
  }

  function onMapUpload(image) {
    setNewMap({ ...newMap, image });
  }

  function addNewMap() {
    newMap.parent_map_id = parentMap && parentMap.id;
    newMap.left = pendingPosition.left;
    newMap.top = pendingPosition.top;
    newMap.id = Date.now();
    storeUserMap(newMap);
    setTimeout(() => {
      reloadMaps();
    }, 500);
    hideCurrentModal().then(() => {
      if (redirect) {
        if (newMap.image) {
          router.push(`/${newMap.id}`);
        } else {
          router.push(`/${newMap.id}/edit`);
        }
      }
      setAddingChild(false);
    });
  }

  function onSaveButtonClick() {
    const errors = {};
    if (!newMap.name) {
      errors.name = "A name is required";
    }
    if (Object.keys(errors).length > 0) {
      setFormErrors(errors);
    } else {
      addNewMap();
    }
  }

  function onCancelButtonClick() {
    hideCurrentModal().then(() => setAddingChild(false));
  }

  const submitButtonDisabled = Object.keys(formErrors).length > 0;

  return (
    <ModalContainer onSelfHide={setAddingChild}>
      <div className={styles["card"]} ref={formRef}>
        <fieldset style={{ display: "flex", alignItems: "center" }}>
          <label htmlFor="name" style={{ marginRight: "8px" }}>
            Name:
          </label>
          <input
            onInput={onNameInput}
            value={newMap.name}
            type="text"
            id="name"
          />
          <button onClick={generateRandomName}>Generate</button>
        </fieldset>
        {formErrors.name && (
          <span
            className="error-state"
            style={{
              borderRadius: "4px",
              padding: "8px",
              display: "block",
              marginBottom: "8px",
            }}
          >
            {formErrors.name}
          </span>
        )}
        <fieldset>
          <WysiwygEditor
            data={newMap.description}
            onDescriptionChange={onDescriptionChange}
          />
        </fieldset>
        <fieldset style={{ display: "flex" }}>
          <MediaManager.ModalButton
            onMediaSelected={(m) => onMapUpload(m.url)}
          ></MediaManager.ModalButton>
          {newMap.image && <img width="100px" src={newMap.image}></img>}
        </fieldset>
        <button disabled={submitButtonDisabled} onClick={onSaveButtonClick}>
          Save
        </button>
        <button className="button--error" onClick={onCancelButtonClick}>
          Cancel
        </button>
      </div>
    </ModalContainer>
  );
}
