import { useCallback, useContext, useEffect, useRef, useState } from "react";
import styles from "./PointOnMap.module.scss";
import MapContext from "../../context/MapContext";
import dragElement from "../../utils/dragElement";
import router from "next/router";
import { guessLink } from "../../utils/pointLlinks";
import useMapRepository from "../../hooks/useMapRepository";

export default function PointOnMap({ item }) {
  const { map, imageRef, context, previewItem } = useContext(MapContext);
  const { updatePoint } = useMapRepository();
  const [computedPointStyles, setComputedPointStyles] = useState({});
  const [dragging, setDragging] = useState(false);
  const [hasSetPointPositions, setHasSetPointPositions] = useState(false);
  const itemRef = useRef();
  const [timesClicked, setTimesClicked] = useState(0);
  const [clickTimeout, setClickTimeout] = useState(null);
  const itemSize = item.size || 30;
  const isAdmin = context === "admin";

  useEffect(() => {
    if (router.query.item == item.id) {
      previewItem(item);
    }
  }, [router.query]);

  useEffect(() => {
    if (isAdmin) {
      dragElement(itemRef.current, {
        onDrag() {
          setDragging(true);
        },
        onEnd(newTop, newLeft) {
          const imageEl = imageRef.current;
          const left =
            (newLeft + itemSize * 0.5 - imageEl.offsetLeft) / imageEl.width;
          const top =
            (newTop + itemSize * 0.5 - imageEl.offsetTop) / imageEl.height;
          const newPointData = { ...item, left, top };
          updatePoint(newPointData);
          setTimeout(() => {
            setDragging(false);
          }, 300);
        },
      });
    }
    function imageLoaded(e) {
      computePointPositions();
    }
    setTimeout(computePointPositions, 500);
    imageRef.current.addEventListener("load", imageLoaded);
    window.addEventListener("resize", computePointPositions);
    return () => {
      window.removeEventListener("resize", computePointPositions);
    };
  }, [item]);

  function computePointPositions() {
    const size = `${itemSize}px`;
    const fontSize = `${itemSize * 0.5}px`;
    function imageEl() {
      if (!imageRef) {
        return {};
      }
      return imageRef.current;
    }

    const left =
      item.left * imageEl().width + imageEl().offsetLeft - itemSize * 0.5;

    const top =
      item.top * imageEl().height + imageEl().offsetTop - itemSize * 0.5;
    setComputedPointStyles({
      width: size,
      height: size,
      fontSize,
      left,
      top,
    });
    setHasSetPointPositions(true);
  }

  const visitItemPreview = useCallback(() => {
    const newClickCount = dragging ? 0 : timesClicked + 1;
    setTimesClicked(newClickCount);
    clearTimeout(clickTimeout);
    setClickTimeout(
      setTimeout(() => {
        if (isAdmin && !dragging) {
          if (newClickCount > 1) {
            router.push(guessLink(item));
            setTimesClicked(0);
          } else if (newClickCount === 1) {
            setTimesClicked(0);
            const query = {
              item: item.id,
              pointId: map.id,
            };
            if (router.query.phase) {
              query.phase = router.query.phase;
            }
            router.replace(
              {
                query,
              },
              { shallow: true }
            );
            previewItem(item);
          }
        } else {
          setTimesClicked(0);
        }
      }, 500)
    );
  }, [isAdmin, dragging, timesClicked]);

  return (
    <div
      ref={itemRef}
      style={{
        ...computedPointStyles,
        ...(isAdmin && { cursor: "pointer" }),
        boxShadow: dragging && "rgb(0 0 0 / 40%) 0px 5px 50px 10px",
        display: !hasSetPointPositions && "none",
      }}
      className={`${styles["point"]}`}
      onClick={visitItemPreview}
    >
      {!isAdmin && !item.name.includes("public:") ? null : (
        <div className={styles["point-text"]}>
          {isAdmin ? item.name : item.name.replace("public:", "")}
        </div>
      )}
    </div>
  );
}
