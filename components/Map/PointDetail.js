import { useEffect, useState } from "react";
import Card from "../Card";
import WysiwygEditor from "../WysiwygEditor";
import MapBreadcrumbs from "./MapBreadcrumbs";
import styles from "./PointDetail.module.scss";
import ViewMapChildrenList from "./ViewMapChildrenList";
import GenerateContentModal from "./GenerateContentModal";
import hideCurrentModal from "../../utils/hideCurrentModal";
import { useRouter } from "next/dist/client/router";
import formatPointToSave from "../../utils/formatPointToSave";
import useMapRepository from "../../hooks/useMapRepository";
import MediaManager from "../MediaManager";

const initialNewChild = {
  name: "",
  image: "",
};

export default function PointDetail({ point }) {
  const {
    deletePoint,
    getMapChildren,
    storeManyUserMaps,
    storeUserMap,
    updatePoint,
  } = useMapRepository();
  const router = useRouter();
  const [saving, setSaving] = useState(false);
  const [computedPoint, setComputedPoint] = useState(null);
  const [children, setChildren] = useState([]);
  const [adding, setAdding] = useState(false);
  const [generatingItem, setGeneratingItem] = useState(false);
  const [newChild, setNewChild] = useState(initialNewChild);
  newChild.parent_map_id = point.id;

  useEffect(() => {
    setComputedPoint(point);
    loadItemChildren(point);
  }, [point, router]);

  useEffect(() => {
    if (computedPoint && computedPoint.id === point.id) {
      clearTimeout(window._updatingTimeout);
      window._updatingTimeout = setTimeout(() => {
        updatePoint(computedPoint);
      }, 500);
    }
  }, [computedPoint]);

  async function loadItemChildren(_point) {
    if (_point) {
      setChildren((await getMapChildren(_point)) || []);
    }
  }

  function onMapUpload(image) {
    setNewChild({
      ...newChild,
      image,
    });
  }

  function onNameInput(e) {
    setNewChild({
      ...newChild,
      name: e.target.value,
    });
  }

  async function onDeleteButtonClick() {
    const confirmed = confirm(
      "Are you sure you want to delete this content and all of it's children items?"
    );
    if (confirmed) {
      await deletePoint(point);
      router.replace(`/${point.parent_map_id || ""}`);
    }
  }

  async function onSaveButtonClick() {
    if (newChild.name) {
      await storeUserMap(newChild);
      setNewChild(initialNewChild);
      await loadItemChildren(point);
    }
  }

  function onModalClose() {
    hideCurrentModal().then(() => {
      setGeneratingItem(false);
    });
  }

  async function onContentGenerated(contentToSave) {
    const allChildren = [];
    function queueItemChildren(item) {
      item.children.forEach((child) => {
        allChildren.push(formatPointToSave(child, child.parent_map_id));
        if (child.children) {
          child.parent_map_id = item.id;
          queueItemChildren(child);
        }
      });
    }
    const formattedContentItems = contentToSave.map((item) => {
      if (item.children) {
        queueItemChildren(item);
      }
      return formatPointToSave(item, point.id);
    });

    await storeManyUserMaps([...allChildren, ...formattedContentItems]);
    loadItemChildren(point);
    setAdding(false);
    onModalClose();
  }

  function onMapNameInput(e) {
    setComputedPoint({
      ...computedPoint,
      name: e.target.value,
    });
  }

  function onComputedDescriptionChanged(event, editor) {
    const description = editor.getData();
    setComputedPoint({
      ...computedPoint,
      description,
    });
  }

  return (
    <div className={styles.container}>
      <MapBreadcrumbs map={point} />
      <div style={{ display: "flex" }}>
        <h1>
          {computedPoint && (
            <input onInput={onMapNameInput} value={computedPoint.name}></input>
          )}
        </h1>
        <button
          onClick={onDeleteButtonClick}
          className="button--error"
          style={{ display: "block", margin: "auto 0" }}
        >
          Delete
        </button>
      </div>
      <h2>Description:</h2>
      <WysiwygEditor
        onDescriptionChange={onComputedDescriptionChanged}
        data={(computedPoint || {}).description}
      />
      <h2>Children Items</h2>
      {adding ? (
        <Card>
          <fieldset>
            <label htmlFor="name">Name</label>
            <input value={newChild.name} onInput={onNameInput} type="name" />
            <label>Upload Map (optional)</label>
            <MediaManager.ModalButton
              onMediaSelected={(m) => onMapUpload(m.url)}
            />
          </fieldset>
          <button onClick={onSaveButtonClick}>Save</button>
          <button onClick={() => setGeneratingItem(true)}>
            Generate an item
          </button>
          <button className="button--error" onClick={() => setAdding(false)}>
            Cancel
          </button>
        </Card>
      ) : (
        <div>
          <button onClick={() => setAdding(true)}>Add Child</button>
          <button
            className="button--success"
            onClick={() => setGeneratingItem(true)}
          >
            Generate Content
          </button>
        </div>
      )}
      <ViewMapChildrenList mapChildren={children} />
      {generatingItem && (
        <GenerateContentModal
          onModalClose={onModalClose}
          onContentGenerated={onContentGenerated}
        />
      )}
    </div>
  );
}
