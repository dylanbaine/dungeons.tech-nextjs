import { useRef, useState, useEffect } from "react";
import Card from "../Card";
import Icon from "../Icon";
import ModalContainer from "../ModalContainer";

export default function MapIntroduction() {
  const [showing, setShowing] = useState(false);
  const videoRef = useRef();

  useEffect(() => {
    function globalShowMapEventListener() {
      setShowing(true);
    }
    if (document.cookie.includes("map-intro-shown=true")) {
      setShowing(false);
    } else {
      setShowing(true);
    }
    document.addEventListener(
      "showMapIntroduction",
      globalShowMapEventListener
    );
    return () => {
      document.removeEventListener(
        "showMapIntroduction",
        globalShowMapEventListener
      );
    };
  }, []);

  useEffect(() => {
    try {
      const { current } = videoRef;
      if (current) {
        try {
          current.playbackRate = 2;
          current.play();
        } catch (e) {}
      }
    } catch (e) {
      console.error(e);
    }
  }, [showing]);

  useEffect(() => {
    if (showing) {
      document.cookie = "map-intro-shown=true;path=/;max-age=63113904";
    }
  }, [showing]);

  if (!showing) {
    return null;
  }
  return (
    <ModalContainer onSelfHide={setShowing}>
      <Card style={cardStyle}>
        <div className="relative">
          <button
            onClick={() => {
              setShowing(false);
            }}
            className="margin-auto--left button--small"
          >
            <Icon name="close" />
          </button>
        </div>
        <div>
          <h1 className="text-center">Welcome to the map builder!</h1>
          {/* <video
            ref={videoRef}
            controls={true}
            autoPlay={true}
            style={videoElementStyle}
            src="https://res.cloudinary.com/drpocweiq/video/upload/v1638026790/Dungeons.Tech/Screen_Recording_2021-11-18_at_7.37.58_PM.mov"
          ></video> */}
          <ul>
            <li>Drag and drop your tiles to anywhere on the map.</li>
            <li>Click on a tile to see the tile details.</li>
            <li>Double click on tiles to edit them.</li>
            <li>Double click the map to add tiles.</li>
            <li>
              Click &quot;Publish to VTT&quot; in the top right menu to share
              the map with friends.
            </li>
          </ul>
          <div className="flex">
            <button
              onClick={() => setShowing(false)}
              className="button--success"
            >
              Got It!
            </button>
          </div>
        </div>
      </Card>
    </ModalContainer>
  );
}
const cardStyle = {
  width: "900px",
  maxWidth: "90vw",
  maxHeight: "90vh",
  overflow: "auto",
};
const videoElementStyle = {
  display: "block",
  margin: "auto",
  maxWidth: "90%",
};
