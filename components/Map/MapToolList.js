import Link from "next/link";
import { useRouter } from "next/router";
import { useCallback, useContext, useEffect, useState } from "react";
import MapContext from "../../context/MapContext";
import styles from "./MapToolList.module.scss";
import MapBreadcrumbs from "./MapBreadcrumbs";
import Card from "../Card";
import ModalContainer from "../ModalContainer";
import hideCurrentModal from "../../utils/hideCurrentModal";
import GenerateContentModal from "./GenerateContentModal";

import formatPointToSave from "../../utils/formatPointToSave";
import { editLink, guessLink } from "../../utils/pointLlinks";
import SearchUserContent from "../SearchUserContent";
import Icon from "../Icon";
import { useUser } from "../../context/UserContext.ts";
import useMapRepository from "../../hooks/useMapRepository";
import Spinner from "../Spinner";
import Loader from "../Loader";

let didClickOnPopup = false;
let preventVttPopup = false;
let vttPopupPrefReset;

function VttUpdatedNotification({ onHide = () => null }) {
  const user = useUser();
  const [alertMessage, setAlertMessage] = useState("");

  useEffect(() => {
    didClickOnPopup = false;
  }, []);

  const styles = {
    margin: "0 auto",
    position: "fixed",
    top: "4px",
    right: "4px",
    backgroundColor: "#fff",
    width: "250px",
  };

  function visitVtt() {
    clearTimeout(vttPopupPrefReset);
    didClickOnPopup = true;
    window.open(
      user.vttLink,
      user.vttLink,
      "location=yes,height=1000,width=1000,scrollbars=yes,status=yes"
    );
  }

  function copyVtt() {
    clearTimeout(vttPopupPrefReset);
    didClickOnPopup = true;
    const el = document.createElement("textarea");
    el.value = `${window.location.protocol}//${window.location.host}${user.vttLink}`;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setAlertMessage("Copied to clipboard!");
    setTimeout(() => {
      onHide();
    }, 1000);
  }

  if (preventVttPopup) {
    return null;
  }

  return (
    <div className="alert alert--success" style={styles}>
      <div className="flex justify-end">
        <Icon
          onClick={() => {
            onHide();
            preventVttPopup = true;
          }}
          style={{
            color: "black",
            marginLeft: "auto",
          }}
          className="pointer"
          name="close"
        />
      </div>
      <p>Your vtt has been updated!</p>
      <div className="flex">
        <button onClick={visitVtt} className="button--small button--success">
          Open VTT
        </button>

        <button
          onClick={copyVtt}
          className="margin-md--left button--small button--info "
        >
          Copy VTT URL
        </button>
      </div>
      {alertMessage}
    </div>
  );
}

export default function MapToolList() {
  const { map, deleteCurrentMap, reloadMaps, toggleGrid } =
    useContext(MapContext);
  const { storeManyUserMaps } = useMapRepository();
  const user = useUser();
  const router = useRouter();
  const [reading, setReading] = useState(false);
  const [generating, setGenerating] = useState(false);
  const [searching, setSearching] = useState(false);
  const [showSentNotification, setShowSentNotification] = useState(false);
  const [loading, setLoading] = useState(false);
  const [initiative, setInitiative] = useState({});

  useEffect(() => {
    function handlePublishInitiativeToVtt(e) {
      setInitiative({
        members: e.members || initiative.members,
        currentIndex:
          e.currentIndex === undefined
            ? initiative.currentIndex
            : e.currentIndex,
      });
    }
    document.addEventListener(
      "publishInitiativeToVtt",
      handlePublishInitiativeToVtt
    );
    return () => {
      document.removeEventListener(
        "publishInitiativeToVtt",
        handlePublishInitiativeToVtt
      );
    };
  }, [map, router.query.phase, initiative]);

  useEffect(() => {
    publishVtt();
  }, [initiative]);

  function onDeleteButtonClick() {
    deleteCurrentMap();
  }

  function onGenerateItemsClick() {
    setGenerating(true);
  }

  function onReadButtonClick() {
    setReading(true);
  }

  function onCloseModalButtonClick() {
    hideCurrentModal().then(() => {
      setReading(false);
      setGenerating(false);
    });
  }

  function onModalEditButtonClick() {
    hideCurrentModal().then(() => {
      setReading(false);
    });
    router.push(editLink(map));
  }

  async function onContentGenerated(contentToSave) {
    setLoading(true);
    const allChildren = [];
    function queueItemChildren(item) {
      item.children.forEach((child) => {
        allChildren.push(formatPointToSave(child, child.parent_map_id));
        if (child.children) {
          child.parent_map_id = item.id;
          queueItemChildren(child);
        }
      });
    }

    const items = [
      ...contentToSave.map((item) => {
        if (item.children) {
          queueItemChildren(item);
        }
        return formatPointToSave(item, map.id);
      }),
      ...allChildren,
    ];

    await storeManyUserMaps(items);
    setTimeout(() => {
      reloadMaps();
      onCloseModalButtonClick();
      setLoading(false);
    }, 700);
  }

  function publishVtt() {
    const initiativeJson = initiative ? JSON.stringify(initiative) : "";
    console.log(initiative);
    fetch(
      `/api/user-vtt/save?objectId=${map.id}&phase=${
        router.query.phase || ""
      }&initiative=${initiativeJson}`
    );
  }

  function sendMapToVtt() {
    if (!user) {
      if (confirm("Create an account to use the virtual table top.")) {
        window.location.href = `/register?backTo=${
          window.location.pathname || "/dashboard"
        }`;
      }
    } else {
      publishVtt();
      setShowSentNotification(true);
      setTimeout(() => {
        setShowSentNotification(false);
        if (!didClickOnPopup) {
          preventVttPopup = true;
        }
      }, 2500);
      vttPopupPrefReset = setTimeout(() => {
        preventVttPopup = false;
      }, 15000);
    }
  }

  if (router.asPath == "/") return null;

  return (
    <div className={styles.container}>
      <button
        onClick={() =>
          document.dispatchEvent(new CustomEvent("showMapIntroduction"))
        }
        className={`${styles["tool-item"]} fixed bottom left button--info`}
      >
        <Icon name="info" />
        Learn how to use the map
      </button>

      {generating &&
        (loading ? (
          <Loader />
        ) : (
          <GenerateContentModal
            onContentGenerated={onContentGenerated}
            onModalClose={onCloseModalButtonClick}
          />
        ))}
      {searching && (
        <ModalContainer onSelfHide={setSearching}>
          <Card style={{ minWidth: "400px" }}>
            <SearchUserContent
              onItemClick={(item) => router.push(guessLink(item))}
            />
            <div className="margin-md">
              <button
                onClick={() =>
                  hideCurrentModal().then(() => setSearching(false))
                }
                className="button--small button--error"
              >
                Cancel
              </button>
            </div>
          </Card>
        </ModalContainer>
      )}
      <MapBreadcrumbs
        map={map}
        className={`${styles["tool-item"]} ${styles["tool-item--breadcrumbs"]}`}
      />
      <button
        onClick={() => setSearching(true)}
        className={styles["tool-item"]}
      >
        Search
      </button>
      <button onClick={() => toggleGrid()} className={styles["tool-item"]}>
        Toggle Grid
      </button>
      <button onClick={onGenerateItemsClick} className={styles["tool-item"]}>
        Generate Items
      </button>
      <Link href={editLink(map)}>
        <button className={styles["tool-item"]}>Edit</button>
      </Link>
      <button
        onClick={onDeleteButtonClick}
        className={`${styles["tool-item"]} button--error`}
      >
        Delete
      </button>
      <button
        onClick={sendMapToVtt}
        className={`${styles["tool-item"]} button--info`}
      >
        Send To VTT
      </button>

      {showSentNotification && (
        <VttUpdatedNotification onHide={() => setShowSentNotification(false)} />
      )}
    </div>
  );
}
