import styles from "./Grid.module.scss";

export default function Fog({
  width = "1000px",
  height = "1000px",
  top = "0",
  left = "0",
  gridSize = "20px",
  style = {},
  ...props
}) {
  return (
    <div
      style={{
        width,
        height,
        top,
        left,
        backgroundSize: gridSize,
        ...style,
      }}
      className={styles.container}
      {...props}
    ></div>
  );
}
