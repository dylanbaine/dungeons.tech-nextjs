/* eslint-disable  @next/next/no-img-element */

import { useContext, useEffect, useRef, useState } from "react";
import MapContext from "../../context/MapContext";
import styles from "./MapImage.module.scss";
import Loader from "../Loader";
import Grid from "./Grid";
import normalizeMediaUrl from "../../utils/normalizeMediaUrl";

export default function MapImage() {
  const {
    map,
    setPendingPosition,
    setImageRef,
    setAddingChild,
    showGrid,
    gridSizePx,
    image,
  } = useContext(MapContext);
  const [computedImageStyles, setComputedImageStyles] = useState({});
  const [loading, setLoading] = useState(true);

  const imageRef = useRef();
  useEffect(() => {
    setImageRef(imageRef);
  }, []);

  function imageObject() {
    return new Promise((resolve, reject) => {
      const imageLoader = new Image();
      imageLoader.onload = function () {
        resolve(this);
      };
      imageLoader.src = map.image;
    });
  }

  async function imageWidth() {
    return (await imageObject()).width; // imageRef.current.width;
  }

  async function imageHeight() {
    return (await imageObject()).height; // imageRef.current.height;
  }

  useEffect(() => {
    async function calculateImageSize() {
      const _imageWidth = await imageWidth();
      const _imageHeight = await imageHeight();
      if (imageRef && imageRef.current) {
        const _computedImageStyles = {};
        if (_imageWidth > _imageHeight) {
          _computedImageStyles.height = "100vh";
        } else if (_imageHeight > _imageWidth) {
          _computedImageStyles.width = "100vw";
        }
        setComputedImageStyles(_computedImageStyles);
      }
    }
    imageRef.current.addEventListener("load", (e) => {
      calculateImageSize();
      setLoading(false);
    });
    setTimeout(() => {
      setLoading(false);
    }, 15000);

    window.addEventListener("resize", calculateImageSize);
    return () => {
      window.removeEventListener("resize", calculateImageSize);
    };
  }, []);

  async function onImageDoubleClick(e) {
    let { pageX, pageY } = e;
    const left = (pageX - e.target.offsetLeft) / imageRef.current.width;
    const top = (pageY - e.target.offsetTop) / imageRef.current.height;
    const pendingPosition = {
      left,
      top,
    };
    setPendingPosition(pendingPosition);
    setAddingChild(true);
  }

  function onGridDoubleClick(e) {
    alert("Hide grid to add tiles");
  }

  return (
    map && (
      <>
        {loading && <Loader />}
        <img
          style={{ ...computedImageStyles, opacity: !showGrid && 1 }}
          ref={imageRef}
          alt=""
          className={styles["image"]}
          src={normalizeMediaUrl(image)}
          onDoubleClick={onImageDoubleClick}
        ></img>
        <Grid
          onDoubleClick={onGridDoubleClick}
          width="100%"
          height="100%"
          gridSize={`${gridSizePx}px`}
          style={{ position: "absolute" }}
        />
      </>
    )
  );
}
