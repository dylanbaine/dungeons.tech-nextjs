import React, { useEffect, useState } from "react";
import Link from "next/link";
import styles from "./MapBreadcrumbs.module.scss";
import { guessLink } from "../../utils/pointLlinks";
import limitStringLength from "../../utils/limitStringLength";
import useMapRepository from "../../hooks/useMapRepository";

const dashboardItem = {
  id: "dashboard",
  href: `/dashboard`,
  text: "Dashboard",
};

export default function MapBreadcrumbs({ map, ...props }) {
  const [breadcrumbItems, setBreadcrumbItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const { getMapParent } = useMapRepository();

  async function buildBreadcrumbs(map) {
    setTimeout(() => setLoading(false), 300);
    setLoading(true);
    function makeBreadcrumb(source) {
      return {
        id: source.id,
        href: guessLink(source),
        text: source.name,
      };
    }
    const newBreadcrumbs = [dashboardItem];
    const parent = await getMapParent(map);
    if (parent) {
      const parentParent = await getMapParent(parent);
      if (parentParent) {
        newBreadcrumbs.push(makeBreadcrumb(parentParent));
      }
      newBreadcrumbs.push(makeBreadcrumb(parent));
    }
    setBreadcrumbItems(newBreadcrumbs);
    setLoading(false);
  }

  useEffect(() => {
    buildBreadcrumbs(map);
  }, [map]);

  return (
    <div {...props}>
      <div className={styles.container}>
        {breadcrumbItems.map((item) => (
          <Link passHref href={item.href} key={item.id}>
            <a className={styles["link"]}>
              <span>{limitStringLength(item.text)}</span>
              <span className={styles["item-divider"]}>/</span>
            </a>
          </Link>
        ))}
        {loading ? (
          "..."
        ) : (
          <strong className={styles["link"]} disabled>
            {map.name} {map.type && `(${map.type})`}
          </strong>
        )}
      </div>
    </div>
  );
}
