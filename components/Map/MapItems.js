import { useContext, useEffect } from "react";
import MapContext from "../../context/MapContext";
import AddChildMap from "./AddChildMap";
import PointOnMap from "./PointOnMap";
import Card from "../Card";

export default function MapItems(props) {
  const { map, childrenMaps, addingChild } = useContext(MapContext);

  const mapsWithPoints = childrenMaps.map((child) => {
    if (typeof child.top === "undefined") {
      child.top = "0.5";
      child.left = "0.5";
    }
    return child;
  });

  return (
    <>
      {addingChild && <AddChildMap parentMap={map} redirect={false} />}
      {mapsWithPoints &&
        mapsWithPoints.map((child) => (
          <PointOnMap key={child.id} item={child} />
        ))}
    </>
  );
}
