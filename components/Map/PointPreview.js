import hideCurrentModal from "../../utils/hideCurrentModal";
import widgetsMap, { renderWidget } from "../../utils/point/widgetsMap";
import { detailLink, mapLink } from "../../utils/pointLlinks";
import Card from "../Card";
import Icon from "../Icon";
import Link from "next/link";
import ModalContainer from "../ModalContainer";
import normalizeMediaUrl from "../../utils/normalizeMediaUrl";

function RenderPointWidgets({ point }) {
  return (
    <div>
      {point.widgets.map((widget) => {
        return renderWidget(widget.name, widget.data, () => null, false);
      })}
    </div>
  );
}

export default function PointPreview({ point, onClose = () => null }) {
  if (!point) return null;
  return (
    <ModalContainer onSelfHide={onClose}>
      <Card style={cardStyle(window.innerWidth < 700)}>
        <div className="relative">
          <Icon
            onClick={() => {
              hideCurrentModal().then(onClose);
            }}
            style={exitButtonStyle}
            name="close"
          />
          <div className="flex">
            <Link href={detailLink(point)}>
              <Icon className="margin-md--right pointer" name="edit" />
            </Link>
            <h2 className="margin-none">{point.name}</h2>
          </div>
          {point.image && (
            <div className="flex justify-center">
              <Link passHref={true} href={mapLink(point)}>
                <a>
                  <div
                    className="map-preview map-preview--small margin-auto"
                    style={{
                      background: `url(${normalizeMediaUrl(point.image)})`,
                    }}
                  ></div>
                </a>
              </Link>
            </div>
          )}
          {point.description && (
            <section
              style={contentStyle}
              dangerouslySetInnerHTML={{ __html: point.description }}
            ></section>
          )}
          {point.widgets && point.widgets.length > 0 && (
            <div
              style={widgetsStyle(
                point.description && point.description.length > 50
              )}
            >
              {point.widgets && <RenderPointWidgets point={point} />}
            </div>
          )}
        </div>
      </Card>
    </ModalContainer>
  );
}

const cardStyle = (mobile = false) => ({
  position: "absolute",
  right: "0px",
  top: "0px",
  width: mobile ? "70%" : "30%",
  height: "100%",
  borderRadius: "0",
  boxShadow: "-4px 0 10px rgb(0, 0, 0, 55%)",
});

const contentStyle = {
  maxHeight: "20vh",
  overflow: "auto",
};
const widgetsStyle = (hascontent) => ({
  maxHeight: hascontent ? "70vh" : "85vh",
  overflow: "auto",
  padding: "6px",
  boxShadow: "inset -1px -1px 6px 4px rgb(0, 0, 0, 0.05)",
  borderRadius: "4px",
});

const exitButtonStyle = {
  position: "absolute",
  right: "4px",
  top: "4px",
  cursor: "pointer",
};
