/* eslint-disable  @next/next/no-img-element */

import { useEffect, useState } from "react";
import WysiwygEditor from "../WysiwygEditor";
import Card from "../Card";
import router from "next/router";
import ModalContainer from "../ModalContainer";
import hideCurrentModal from "../../utils/hideCurrentModal";
import { generateRandomCityName } from "../../utils/generate";
import ViewMapChildrenList from "./ViewMapChildrenList";
import useMapRepository from "../../hooks/useMapRepository";
import MediaManager from "../MediaManager";

export default function EditMap({ map, onMapEdited }) {
  const [newMapData, setNewMapData] = useState({
    name: "",
    description: "",
    image: "",
  });

  const { getMapChildren, updatePoint } = useMapRepository();
  const [mapChildren, setMapChildren] = useState([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const { name = "", description = "", image = "" } = map;
    setNewMapData({ name, description, image });
    setLoaded(true);
    (async () => {
      setMapChildren(await getMapChildren(map));
    })();
  }, [map]);

  function generateRandomName() {
    setNewMapData({
      ...newMapData,
      name: generateRandomCityName(),
    });
  }

  function onNameInput(e) {
    newMapData.name = e.target.value;
    setNewMapData({ ...newMapData });
  }

  function onMapUpload(map) {
    newMapData.image = map;
    setNewMapData({ ...newMapData });
  }

  function onDescriptionChange(e, editor) {
    newMapData.description = editor.getData();
    setNewMapData({ ...newMapData });
  }

  async function onSaveButtonClick() {
    const computedMapData = { ...map, ...newMapData };
    updatePoint(computedMapData);
    document.dispatchEvent(new CustomEvent("loadMaps"));
    onMapEdited(computedMapData);
  }

  function onCancelButtonClick() {
    hideCurrentModal().then(() => {
      if (map.image) {
        router.push(`/${map.id}`);
      } else if (map.parent_map_id) {
        router.push(`/${map.parent_map_id}`);
      } else {
        // router.push(`/`);
      }
    });
  }

  return (
    <ModalContainer onSelfHide={onCancelButtonClick}>
      <Card style={{ maxWidth: "800px" }}>
        <fieldset style={{ display: "flex", alignItems: "center" }}>
          <label htmlFor="name" style={{ marginRight: "8px" }}>
            Name:
          </label>
          <input
            onInput={onNameInput}
            value={newMapData.name}
            type="text"
            id="name"
          />
          <button onClick={generateRandomName}>Generate</button>
        </fieldset>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: "8px",
          }}
        >
          <MediaManager.ModalButton
            onMediaSelected={(m) => onMapUpload(m.url)}
          ></MediaManager.ModalButton>
          <img
            style={{ marginLeft: "8px" }}
            src={newMapData.image}
            width={100}
          />
        </div>
        <fieldset>
          {loaded && (
            <WysiwygEditor
              data={newMapData.description}
              onDescriptionChange={onDescriptionChange}
            />
          )}
        </fieldset>
        <button onClick={onSaveButtonClick}>Save</button>
        <button onClick={onCancelButtonClick} className="button--error">
          Cancel
        </button>
        <ViewMapChildrenList mapChildren={mapChildren} />
      </Card>
    </ModalContainer>
  );
}
