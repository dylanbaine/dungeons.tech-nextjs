import MapContext from "../../context/MapContext";
import MapToolList from "./MapToolList";
import MapItems from "./MapItems";
import MapImage from "./MapImage";
import { useEffect, useState, useContext } from "react";
import styles from "./Map.module.scss";
import { useRouter } from "next/router";
import Head from "next/head";
import PointPreview from "./PointPreview";
import useMapRepository from "../../hooks/useMapRepository";
import MapIntroduction from "./MapIntroduction";
import Link from "next/link";
import { mapLink } from "../../utils/pointLlinks";

function MapPhaseSelector({ map }) {
  const { image } = useContext(MapContext);
  const activeStyles = "solid 6px var(--info)";

  return (
    <div className={styles["map-phase-selector"]}>
      {map.image && (
        <Link shallow href={mapLink(map)}>
          <a>
            <div
              className="map-preview map-preview--thumb"
              style={{
                background: `url(${map.image})`,
                borderLeft: map.image === image && activeStyles,
              }}
            ></div>
          </a>
        </Link>
      )}
      {map.phases.map((phase, i) => (
        <Link shallow key={phase} href={mapLink(map, i)}>
          <a>
            <div
              className="map-preview map-preview--thumb"
              style={{
                background: `url(${phase})`,
                borderLeft: phase === image && activeStyles,
              }}
            ></div>
          </a>
        </Link>
      ))}
    </div>
  );
}

function Map({
  map,
  childrenMaps,
  onUpdate = () => null,
  context = "admin",
  image,
}) {
  const [pendingPosition, setPendingPosition] = useState({});
  const [previewItem, setPreviewItem] = useState(null);
  const [imageRef, setImageRef] = useState();
  const [addingChild, setAddingChild] = useState(false);
  const [showGrid, setShowGrid] = useState(true);
  const { deletePoint } = useMapRepository();
  const router = useRouter();

  useEffect(() => {
    setShowGrid(
      localStorage.getItem("showGrid") === null ||
        localStorage.getItem("showGrid") === "true"
    );
  }, []);

  useEffect(() => {
    if (!router.query.item) {
      setPreviewItem(null);
    }
  }, [router.query]);

  const contextValue = {
    image,
    context,
    showGrid,
    toggleGrid() {
      setShowGrid(!showGrid);
      localStorage.setItem("showGrid", !showGrid ? "true" : "false");
    },
    gridSizePx: 30,
    map,
    childrenMaps,
    pendingPosition,
    setPendingPosition,
    imageRef,
    setImageRef,
    addingChild,
    setAddingChild,
    reloadMaps() {
      onUpdate();
      document.dispatchEvent(new CustomEvent("loadMaps"));
    },
    deleteCurrentMap() {
      const verified = confirm(
        "Are you sure you want to delete this map an all of it's children maps?"
      );
      if (verified) {
        deletePoint(map);
        setTimeout(() => {
          router.replace("/dashboard");
        }, 1000);
      }
    },
    previewItem(item) {
      setPreviewItem(item);
    },
  };
  return (
    <div className={styles.container}>
      {previewItem && (
        <PointPreview
          onClose={() => {
            setPreviewItem(null);
            router.push(
              {
                query: {
                  pointId: map.id,
                  phase: router.query.phase,
                },
              },
              { shallow: true }
            );
          }}
          point={previewItem}
        />
      )}
      <Head>
        <style>{`
                footer {
                    display: none !important;
                }
            `}</style>
      </Head>
      <MapContext.Provider value={contextValue}>
        {context === "admin" &&
          map.phases &&
          Array.isArray(map.phases) &&
          map.phases.length > 0 && <MapPhaseSelector map={map} />}
        {context === "admin" && !previewItem && <MapToolList />}
        {context === "admin" && <MapIntroduction />}
        <MapImage />
        <MapItems />
      </MapContext.Provider>
    </div>
  );
}

export default Map;
