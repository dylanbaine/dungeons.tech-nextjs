import ModalContainer from "../ModalContainer";
import Card from "../Card";
import { Children, useState } from "react";
import { generatorMap } from "../../utils/generate";

export default function GenerateContentModal({
  onModalClose = () => null,
  onContentGenerated = () => null,
}) {
  const [type, setType] = useState(null);
  const [count, setCount] = useState(null);
  const [generatedContent, setGeneratedContent] = useState([]);

  function generateItems() {
    const method = generatorMap[type];
    if (method) {
      const newGeneratedContent = [];
      while (newGeneratedContent.length < count) {
        const item = method();
        newGeneratedContent.push(item);
      }
      setGeneratedContent(newGeneratedContent);
    }
  }

  function onTypeSelect(e) {
    const newType = e.target.value;
    if (newType === "city") {
      setCount(1);
    }
    setType(newType);
  }
  function onTotalItemsInput(e) {
    setCount(e.target.value);
  }
  function onSaveButtonClick() {
    setTimeout(() => {
      onContentGenerated(generatedContent);
    }, 1000);
  }
  //   console.log(generatedContent);
  return (
    <ModalContainer onSelfHide={onModalClose}>
      <Card>
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <button
            onClick={onModalClose}
            className="button--error button--small"
          >
            Close
          </button>
        </div>
        <div
          style={{
            minWidth: "400px",
            maxWidth: "80vw",
            overflow: "auto",
            maxHeight: "80vh",
            padding: "4px",
          }}
        >
          <fieldset className="flex">
            <label style={{ marginRight: "8px" }} htmlFor="type-of-items">
              What Type?
            </label>
            <select onInput={onTypeSelect} id="type-of-items">
              <option value={null}>Select a type</option>
              <option value="npc">NPC</option>
              <option value="city">City</option>
              <option value="tavern">Tavern</option>
              <option value="magic-shop">Magic Shop</option>
              <option value="blacksmith">Blacksmith</option>
              <option value="random-shop">Random Shop</option>
            </select>
          </fieldset>
          {type && type !== "city" && (
            <fieldset className="flex">
              <label
                onInput={onTotalItemsInput}
                style={{ marginRight: "8px" }}
                htmlFor="total-items"
              >
                How Many?
              </label>
              <input
                autoFocus={true}
                id="total-items"
                onInput={onTotalItemsInput}
                type="number"
              />
            </fieldset>
          )}
          <div className="flex">
            {type && count && (
              <button onClick={generateItems}>
                {generatedContent.length > 0 && "Re-"}Generate
              </button>
            )}
            {generatedContent && generatedContent.length > 0 && (
              <button
                autoFocus={true}
                className="button--success"
                onClick={onSaveButtonClick}
              >
                SAVE
              </button>
            )}
          </div>
          <div style={{ maxHeight: "500px", overflow: "auto" }}>
            {generatedContent && generatedContent.length > 0 && (
              <div>
                {generatedContent.map((item) => (
                  <div key={item.id}>
                    <h3>{item.name}</h3>
                    <div
                      dangerouslySetInnerHTML={{ __html: item.description }}
                    ></div>
                    {item.preview && item.preview()}
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </Card>
    </ModalContainer>
  );
}
