import { useEffect, useMemo, useState } from "react";
import spells from "../../utils/data/spells";
import styles from "./SpellsWidget.module.scss";
import Card from "../Card";
import limitStringLength from "../../utils/limitStringLength";
import ModalContainer from "../ModalContainer";
import hideCurrentModal from "../../utils/hideCurrentModal";
import Icon from "../Icon";

function findSpell(name) {
  return spells.find(
    (spell) => spell.name.toLowerCase() === name.toLowerCase()
  );
}

const filterUniqueArray = (currentClass, index, classes) =>
  classes.indexOf(currentClass) === index;

const spellClassFilters = spells
  .map((s) => s.class.split(", "))
  .flat(1)
  .filter(filterUniqueArray);

const spellLevelFilters = spells.map((s) => s.level).filter(filterUniqueArray);

const spellSchoolFilters = spells
  .map((s) => s.school)
  .filter(filterUniqueArray);

const cantripToTop = (a, b) => (a === "Cantrip" ? -1 : b === "Cantrip" ? 1 : 0);

function RenderAttributeFilters({
  attribute,
  items,
  classFilterIsActive = () => null,
  spellClassFilterClicked = () => null,
}) {
  const [open, setOpen] = useState(true);
  const computedItems = [...items]
    .sort((a, b) => {
      return a.localeCompare(b);
    })
    .sort(cantripToTop);
  return (
    <div className={styles["filter-list-container"]}>
      <span onClick={() => setOpen(!open)}>{attribute}:</span>
      <hr></hr>
      {open && (
        <div className="flex flex--wrap">
          {computedItems.map((item) => (
            <button
              onClick={() => spellClassFilterClicked(item)}
              className={`button--small ${styles["filter-option"]} ${
                classFilterIsActive(item)
                  ? `${styles["filter-option--active"]}`
                  : ""
              }`.trim()}
              key={item}
            >
              {item}
            </button>
          ))}
        </div>
      )}
    </div>
  );
}

function BackendSpell({
  spell,
  spellClassFilterClicked = () => false,
  selectable = true,
  onSpellSelected = () => null,
  onSpellDelete = () => null,
  activeSpells = [],
}) {
  const [readingAll, setReadingAll] = useState(false);
  const isActive = activeSpells.includes(spell.name);
  return (
    <Card style={{ marginTop: "12px" }} key={spell.name}>
      <div className={styles["spell-title"]}>{spell.name}</div>
      {selectable ? (
        <button
          className="button--info"
          disabled={isActive}
          onClick={() => onSpellSelected(spell)}
          style={{ marginTop: "6px" }}
        >
          + Add
        </button>
      ) : (
        <button
          className="button--error"
          disabled={isActive}
          onClick={() => onSpellDelete(spell)}
          style={{ marginTop: "6px" }}
        >
          - Remove
        </button>
      )}
      <div>
        <div
          onClick={() => setReadingAll(!readingAll)}
          className="pointer"
          dangerouslySetInnerHTML={{
            __html:
              spell.desc &&
              limitStringLength(
                spell.desc,
                readingAll ? 5000 : 400,
                "...[click for more]"
              ),
          }}
        ></div>
      </div>
      {selectable && (
        <div className="flex">
          <button
            onClick={() => spellClassFilterClicked(spell.level)}
            className="button--small"
          >
            {spell.level}
          </button>
          <button
            onClick={() => spellClassFilterClicked(spell.school)}
            className="button--small"
          >
            {spell.school}
          </button>
          {spell.class.split(", ").map((spellClass) => (
            <button
              onClick={() => spellClassFilterClicked(spellClass)}
              className="button--small"
              key={spellClass}
            >
              {spellClass}
            </button>
          ))}
        </div>
      )}
    </Card>
  );
}

function SpellOptions({
  onSpellSelected = () => null,
  activeSpells = [],
  closeButtonClicked = () => null,
  onSpellDelete = () => null,
}) {
  const [search, setSearch] = useState("");
  const [attributeFilters, setAttributeFilters] = useState([]);

  const computedSpells = useMemo(() => {
    let _spells = [...spells];
    const currentClassFilters = attributeFilters.filter((f) =>
      spellClassFilters.includes(f)
    );
    const currentLevelFilters = attributeFilters.filter((f) =>
      spellLevelFilters.includes(f)
    );
    const currentSchoolFilters = attributeFilters.filter((f) =>
      spellSchoolFilters.includes(f)
    );
    if (currentClassFilters.length) {
      _spells = _spells.filter((s) =>
        currentClassFilters.find((c) =>
          s.class.toLowerCase().includes(c.toLowerCase())
        )
      );
    }
    if (currentLevelFilters.length) {
      _spells = _spells.filter((s) => currentLevelFilters.includes(s.level));
    }
    if (currentSchoolFilters.length) {
      _spells = _spells.filter((s) => currentSchoolFilters.includes(s.school));
    }
    if (search.length) {
      const searchLower = search.toLowerCase();
      _spells = _spells.filter(
        (s) =>
          s.name.toLowerCase().includes(searchLower) ||
          s.desc.toLowerCase().includes(searchLower)
      );
      _spells.sort((a, b) => {
        if (a.name.toLowerCase().includes(searchLower)) {
          return -1;
        }
        return 1;
      });
      _spells = _spells.map((s) => {
        return {
          ...s,
          desc: s.desc.replace(
            new RegExp(searchLower, "ig"),
            `<i style="background: yellow; color: black;">${searchLower}</i>`
          ),
        };
      });
    }
    return _spells;
  }, [search, attributeFilters]);

  function spellClassFilterClicked(spellClassClicked) {
    let newAttributeFilters = [...attributeFilters];
    if (classFilterIsActive(spellClassClicked)) {
      newAttributeFilters = attributeFilters.filter(
        (v) => v !== spellClassClicked
      );
    } else {
      newAttributeFilters.push(spellClassClicked);
    }
    setAttributeFilters(newAttributeFilters);
  }

  function classFilterIsActive(spell) {
    return attributeFilters.includes(spell);
  }

  return (
    <div className={styles["spells-list-modal"]}>
      <Card className={styles["all-filters"]}>
        <button
          onClick={closeButtonClicked}
          style={{ zIndex: "99" }}
          className="button--error button--small"
        >
          close
        </button>
        <input
          placeholder="Search Spells..."
          type="text"
          onInput={({ target: { value } }) => setSearch(value)}
        />
        {activeSpells.length > 0 && <b>Current Spells:</b>}
        {activeSpells.map((spellName) => {
          const spell = findSpell(spellName);
          return (
            <div key={spell.name}>
              <small>{spell.level}</small> {spell.name}{" "}
              <button
                onClick={() => onSpellDelete(spell)}
                className="button--error button--small"
              >
                remove
              </button>
            </div>
          );
        })}
        <RenderAttributeFilters
          spellClassFilterClicked={spellClassFilterClicked}
          classFilterIsActive={classFilterIsActive}
          attribute="Spell Level"
          items={spellLevelFilters}
        />
        <RenderAttributeFilters
          spellClassFilterClicked={spellClassFilterClicked}
          classFilterIsActive={classFilterIsActive}
          attribute="Class"
          items={spellClassFilters}
        />
        <RenderAttributeFilters
          spellClassFilterClicked={spellClassFilterClicked}
          classFilterIsActive={classFilterIsActive}
          attribute="School"
          items={spellSchoolFilters}
        />
      </Card>
      <div className={styles["spell-options-container"]}>
        <div>
          {computedSpells.map((spell) => {
            return (
              <BackendSpell
                activeSpells={activeSpells}
                onSpellSelected={() => onSpellSelected(spell)}
                key={spell.name}
                spell={spell}
                spellClassFilterClicked={spellClassFilterClicked}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

const slotsPerLevel = {
  "1st-level": 4,
  "2nd-level": 3,
  "3rd-level": 3,
  "4th-level": 3,
  "5th-level": 3,
  "6th-level": 2,
  "7th-level": 2,
  "8th-level": 1,
  "9th-level": 1,
};

function FrontEndSpell({ spell, showSeparator }) {
  const [showDesc, setShowDesc] = useState(false);
  return (
    <div
      onClick={() => setShowDesc(!showDesc)}
      key={spell.name}
      className="pointer"
    >
      {showSeparator && (
        <div
          style={{
            width: "40%",
            borderTop: "1px solid var(--border)",
            margin: "6px o",
          }}
        ></div>
      )}
      <b>{spell.name}</b>
      <div
        dangerouslySetInnerHTML={{
          __html: limitStringLength(
            spell.desc,
            showDesc ? 20000 : 100,
            "...[click for more]"
          ),
        }}
      ></div>
    </div>
  );
}

function SpellsFrontEnd({
  spells = [],
  usedSlots = {},
  onSpellSlotUsed = () => null,
  onSpellDelete = () => null,
}) {
  const computedSpells = useMemo(
    () => spells.map((s) => findSpell(s)),
    [spells]
  );
  if (spells.length === 0) {
    return "Looks like you need to add items to your spell list.";
  }
  return (
    <div>
      {spellLevelFilters
        .sort((a, b) => {
          return a.localeCompare(b);
        })
        .sort(cantripToTop)
        .map((level) => {
          const levelSpells = computedSpells.filter((s) => s.level === level);
          if (levelSpells.length === 0) return;
          const slotCount = slotsPerLevel[level];
          return (
            <div
              style={{
                borderLeft: "solid 2px var(--info)",
                paddingLeft: "12px",
                marginBottom: "20px",
              }}
              key={level}
            >
              <b>{level}</b>
              <br></br>
              {slotCount &&
                [...Array(slotCount).keys()].map((_, i) => {
                  const checked = usedSlots[level] > i;
                  return (
                    <input
                      checked={checked}
                      onChange={(e) => {
                        onSpellSlotUsed(level, checked ? "minus" : "plus");
                      }}
                      key={i}
                      type="checkbox"
                    ></input>
                  );
                })}
              <br></br>
              {levelSpells.map((spell, i) => {
                return (
                  <FrontEndSpell
                    showSeparator={i > 0}
                    key={`${spell}-${i}`}
                    spell={spell}
                  />
                );
              })}
              <hr></hr>
            </div>
          );
        })}
    </div>
  );
}

export default function SpellsWidget({
  initialData = {},
  onUpdate = () => null,
  allowEdit = true,
}) {
  const [loading, setLoading] = useState(true);
  const [activeSpells, setActiveSpells] = useState([]);
  const [showSpellOptions, setShowSpellOptions] = useState(false);
  const [usedSlots, setUsedSlots] = useState({});
  const [collapsed, setCollapsed] = useState(false);

  useEffect(() => {
    let activeSpells = initialData.activeSpells || [];
    if (activeSpells.length > 0 && typeof activeSpells[0] !== "string") {
      activeSpells = activeSpells.map((spell) => spell.name);
      onUpdate({
        ...initialData,
        activeSpells,
      });
    }
    setActiveSpells(activeSpells);
    setUsedSlots(initialData.usedSlots || {});
    if (initialData.collapsed) {
      setCollapsed(true);
    }
    setLoading(false);
  }, []);

  useEffect(() => {
    onUpdate({
      activeSpells,
      usedSlots,
      collapsed,
    });
  }, [collapsed, activeSpells, usedSlots]);

  function addSpelltoList(newSpell) {
    const spells = [...activeSpells];
    if (!activeSpells.includes(newSpell.name)) {
      spells.push(newSpell.name);
      setActiveSpells(spells);
      onUpdate({ activeSpells: spells, usedSlots, collapsed });
    }
  }

  function onSpellDelete(spell) {
    const newSpells = activeSpells.filter(
      (s) => s.toLowerCase() !== spell.name.toLowerCase()
    );
    setActiveSpells(newSpells);
    onUpdate({ activeSpells: newSpells, usedSlots, collapsed });
  }

  function onSpellSlotUsed(level, method = "plus") {
    const newUsedSlots = { ...usedSlots };
    let newValue = newUsedSlots[level] || 0;
    if (method === "plus") {
      newValue++;
    } else {
      newValue--;
    }
    newUsedSlots[level] = newValue;
    setUsedSlots(newUsedSlots);
    onUpdate({ activeSpells, usedSlots: newUsedSlots });
  }

  return (
    <div className={styles["spells-widget"]}>
      <div className="flex">
        {allowEdit && (
          <button onClick={() => setShowSpellOptions(!showSpellOptions)}>
            {showSpellOptions ? "View Your List" : "Manage Spells"}
          </button>
        )}
        <button
          className="margin-auto--left"
          onClick={() => setCollapsed(!collapsed)}
        >
          <Icon
            style={{
              transition: "transform 0.2s",
              transform: collapsed ? "rotate(90deg)" : "rotate(-90deg)",
            }}
            name="chevron_right"
          />
          {collapsed ? "Expand" : "Collapse"}
        </button>
      </div>

      {!collapsed && (
        <div>
          {showSpellOptions ? (
            <ModalContainer>
              <SpellOptions
                onSpellDelete={onSpellDelete}
                closeButtonClicked={() => {
                  hideCurrentModal().then(() => setShowSpellOptions(false));
                }}
                onSpellSelected={addSpelltoList}
                activeSpells={activeSpells}
              />
            </ModalContainer>
          ) : (
            <SpellsFrontEnd
              spells={activeSpells}
              usedSlots={usedSlots}
              onSpellSlotUsed={onSpellSlotUsed}
              onSpellDelete={onSpellDelete}
            />
          )}
        </div>
      )}
    </div>
  );
}
