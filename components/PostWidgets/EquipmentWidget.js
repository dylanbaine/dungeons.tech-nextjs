import { useEffect, useMemo, useState } from "react";
import styles from "./EquipmentWidget.module.scss";
import ModalContainer from "../ModalContainer";
import Card from "../Card";
import hideCurrentModal from "../../utils/hideCurrentModal";
import equipmentList from "../../utils/data/equipment";
import Icon from "../Icon";

function findItem(name) {
  return equipmentList.find(
    (item) => item.name.toLowerCase() === name.toLowerCase()
  );
}

function SearchEquipment({ onItemSelected }) {
  const [search, setSearch] = useState("");

  const computedEquipment = useMemo(() => {
    let filteredList = [...equipmentList];
    const searchFiltered = search.toLocaleLowerCase().replace(/\s/g, "");
    if (search) {
      filteredList = filteredList.filter((item) => {
        const filterableStrings = [
          item.name,
          item.equipment_category,
          item.name + item.equipment_category,
          item.equipment_category + item.name,
          item.weapon_range && item.weapon_range + item.name,
          item.weapon_range && item.name + item.weapon_range,
          item.weapon_category && item.weapon_category + item.name,
          item.weapon_category && item.name + item.weapon_category,
        ]
          .filter(Boolean)
          .map((s) => s.toLowerCase().replace(/\s/g, ""));
        return filterableStrings.find((s) => s.includes(searchFiltered));
      });
    }
    return filteredList;
  }, [search]);

  return (
    <div className={styles["equipment-list-container"]}>
      <div className={styles["equipment-list-filters"]}>
        <div>
          <input
            type="text"
            onInput={(e) => setSearch(e.target.value)}
            placeholder="Search..."
          />
        </div>
      </div>
      <div className={styles["equipment-list-items"]}>
        {computedEquipment.map((item) => {
          return (
            <div
              key={item.index}
              className={styles["equipment-list-single-item"]}
            >
              <div className={styles["inner"]}>
                <div className={styles["name"]}>{item.name}</div>
                <button
                  onClick={() => onItemSelected(item)}
                  className="button--small button--info"
                >
                  pin
                </button>
                <div className={styles["cost"]}>
                  {item.cost.quantity} {item.cost.unit}
                </div>
                <i>{item.category_range}</i>
                {item.damage && (
                  <div className={styles["damage"]}>
                    {item.damage.dice_count}d{item.damage.dice_value}{" "}
                    {item.damage.damage_type.name} Damage
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

function ShowPinnedEquipment({ pinnedItems = [], removeItem = () => null }) {
  const computedPinnedItems = useMemo(() => {
    if (typeof pinnedItems[0] !== "string") {
      return pinnedItems;
    }
    return pinnedItems.map((itemName) => findItem(itemName));
  }, [pinnedItems]);

  const styleOverride = {};
  if (!window.location.href.includes("/detail")) {
    styleOverride.width = "100%";
  }
  return (
    <div className={styles["pinned-items-list"]}>
      {(computedPinnedItems || []).map((item, i) => {
        return (
          <Card
            className={styles["pinned-item"]}
            style={styleOverride}
            key={item.name + i}
          >
            <div className={styles["name"]}>
              <h3>{item.name}</h3>
              <button
                onClick={() => removeItem(item, i)}
                className="button--error button--small"
              >
                <Icon className="size-sm" name="delete_forever" />
                Remove
              </button>
            </div>
            <table>
              <thead>
                <tr>
                  {item.armor_class && <td>AC</td>}
                  {item.damage && <td>Damage</td>}
                  <td>Weight</td>
                  <td>Cost</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  {item.armor_class && (
                    <td>
                      {item.armor_class.base}
                      {item.armor_class.dex_bonus && " + Dex"}
                      {item.armor_class.max_bonus && " + Max"}
                    </td>
                  )}
                  {item.damage && (
                    <td>
                      {item.damage.dice_count}d{item.damage.dice_value}
                    </td>
                  )}
                  <td> {item.weight}</td>
                  <td>
                    {" "}
                    {item.cost.quantity}
                    {item.cost.unit}
                  </td>
                </tr>
              </tbody>
            </table>
          </Card>
        );
      })}
    </div>
  );
}

export default function EquipmentWidget({
  initialData = {},
  onUpdate = () => null,
  allowEdit = true,
}) {
  const [pinnedItems, setPinnedItems] = useState([]);
  const [searching, setSearching] = useState(false);

  useEffect(() => {
    let pinnedItems = initialData.pinnedItems || [];
    if (pinnedItems.length > 0 && typeof pinnedItems[0] !== "string") {
      pinnedItems = pinnedItems.map((item) => item.name);
    }
    setPinnedItems(pinnedItems);
  }, []);

  useEffect(() => {
    onUpdate({ pinnedItems });
  }, [pinnedItems]);

  function removeItem(item, index) {
    if (confirm("Are you sure you want to remove this weapon?")) {
      const newPinedItems = pinnedItems.filter((_item, key) => {
        return key !== index;
      });
      setPinnedItems(newPinedItems);
    }
  }

  return (
    <div className={styles["container"]}>
      {allowEdit && (
        <button className="button--small" onClick={() => setSearching(true)}>
          <Icon name="add" className="size-sm" />
          Add Equipment
        </button>
      )}
      {searching && (
        <ModalContainer>
          <Card>
            <button
              style={{ marginLeft: "auto" }}
              className="button--error button--small"
              onClick={() => {
                hideCurrentModal().then(() => setSearching(false));
              }}
            >
              Close
            </button>
            <SearchEquipment
              onItemSelected={(newItem) => {
                setPinnedItems([...pinnedItems, newItem.name]);
              }}
            />
          </Card>
        </ModalContainer>
      )}
      <ShowPinnedEquipment pinnedItems={pinnedItems} removeItem={removeItem} />
    </div>
  );
}
