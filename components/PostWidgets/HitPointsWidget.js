import React, { useEffect, useMemo, useState } from "react";
import styles from "./HitPointsWidget.module.scss";

export default function HitPointsWidget({
  initialData = {},
  onUpdate = () => null,
  allowEdit = true,
}) {
  const [max, setMax] = useState("");
  const [current, setCurrent] = useState("");
  const [newCurrent, setNewCurrent] = useState("");
  const [ac, setAc] = useState(12);

  const computedPercentage = useMemo(() => {
    return Math.ceil((current / max) * 100);
  }, [max, current]);

  useEffect(() => {
    setMax(initialData.max || 20);
    setCurrent(initialData.current || 20);
    setAc(initialData.ac || 12);
  }, []);

  useEffect(() => {
    onUpdate({ max, current, ac });
  }, [max, current, ac]);

  return (
    <div className="margin-md">
      {allowEdit && (
        <React.Fragment>
          <div className="sm:flex">
            <span className="margin-md">Current</span>
            <input
              className="input--compact"
              style={{ width: "60px" }}
              value={current}
              type="number"
              onInput={(e) => {
                setCurrent(e.target.value);
              }}
            />
            <span className="margin-md">Max</span>
            <input
              className="input--compact"
              style={{ width: "60px" }}
              value={max}
              type="number"
              onInput={(e) => setMax(e.target.value)}
            />
          </div>
          <div className="sm:flex margin-md">
            <input
              className="input--compact"
              style={{ width: "60px", marginRight: "4px" }}
              value={newCurrent}
              onInput={(e) => setNewCurrent(e.target.value)}
              type="number"
            ></input>
            <button
              onClick={() => {
                if (newCurrent) {
                  setCurrent(parseInt(current) + parseInt(newCurrent));
                  setNewCurrent("");
                }
              }}
              className="button--small margin-none"
            >
              +
            </button>
            <button
              onClick={() => {
                if (newCurrent) {
                  setCurrent(parseInt(current) - parseInt(newCurrent));
                  setNewCurrent("");
                }
              }}
              className="button--small margin-none"
            >
              -
            </button>
          </div>
        </React.Fragment>
      )}
      <div>
        {current} / {max}
      </div>
      <div className={styles["indicator"]}>
        <div
          style={{ width: `${computedPercentage}%` }}
          className={styles["indicator__health"]}
        ></div>
      </div>
      <h4 className="sm:flex">
        <div>AC: </div>
        {allowEdit ? (
          <input
            value={ac}
            onInput={(e) => setAc(e.target.value)}
            className="margin-md--left"
          />
        ) : (
          ac
        )}
      </h4>
    </div>
  );
}
