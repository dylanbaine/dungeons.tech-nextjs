[vercel](https://vercel.com/dylanbaine) for next js app hosting
[upstash](https://console.upstash.com/) for managed redis
[forge](https://forge.laravel.com/) and [vultr](https://my.vultr.com/) for laravel api
[cloudinary](https://cloudinary.com/console/c-bc5c5d10b63cf079bed811e70da179/media_library/folders/00a9b9dc3eb1df38c47e22c0965809d789) for marketing media
[amazon s3](https://s3.console.aws.amazon.com/s3/buckets/elasticbeanstalk-us-east-2-350765264819?region=us-east-2&tab=objects) for uploaded user images
[stripe](https://dashboard.stripe.com/) for payment subscriptions
[pusher](https://dashboard.pusher.com/) for webseockets
[meilisearch](https://docs.meilisearch.com/) api as a service self hosted on forge
[Jira Board](https://dylanbaine.atlassian.net/jira/software/projects/DT/boards/1) for kanban board
[Laravel API](https://bitbucket.org/dylanbaine/dungeons.tech-laravel-api/src/master/)
