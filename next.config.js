const { API_BASE } = process.env;

const NextAppConfig = {
  rewrites() {
    return [
      {
        source: "/api/app/:path*",
        destination: `${API_BASE}/api/:path*`,
      },
      {
        source: "/uploaded-media/:path*",
        destination:
          "https://elasticbeanstalk-us-east-2-350765264819.s3.us-east-2.amazonaws.com/:path*",
      },
      {
        source: "/static-media/:path*",
        destination: "https://res.cloudinary.com/drpocweiq/image/upload/:path*",
      },
      {
        source: "/reddit-media/:path*",
        destination: "https://i.redd.it/:path*",
      },
    ];
  },
  headers() {
    return [
      {
        source: "/uploaded-media/:path*",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          },
        ],
      },
      {
        source: "/static-media/:path*",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          },
        ],
      },
      {
        source: "/reddit-media/:path*",
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=31536000, immutable",
          },
        ],
      },
    ];
  },
  reactStrictMode: true,
  env: {
    MEILISEARCH_HOST: process.env.MEILISEARCH_HOST,
    MEILISEARCH_KEY: process.env.MEILISEARCH_KEY,
    APP_URL: process.env.APP_URL,
    API_BASE: process.env.API_BASE,
    APP_ENV: process.env.APP_ENV,
  },
  images: {
    domains: ["res.cloudinary.com"],
  },
};
module.exports = NextAppConfig;
