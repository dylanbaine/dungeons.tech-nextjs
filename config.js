const api_base = process.env.API_BASE || "https://api.dungeons.tech";
const null_cookie_value = "__";

const config = { api_base, null_cookie_value };
export default config;
