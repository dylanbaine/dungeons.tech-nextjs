import GenerateContentModal from "../../../components/Map/GenerateContentModal";
import MapBreadcrumbs from "../../../components/Map/MapBreadcrumbs";
import ViewMapChildrenList from "../../../components/Map/ViewMapChildrenList";
import WysiwygEditor from "../../../components/WysiwygEditor";
import Card from "../../../components/Card";
import React, { createContext, useContext, useEffect, useState } from "react";
import { withRouter } from "next/router";
import styles from "./PointDetail.module.scss";
import Head from "next/head";
import Loader from "../../../components/Loader";
import { guessLink, mapLink } from "../../../utils/pointLlinks";
import Link from "next/link";
import formatPointToSave from "../../../utils/formatPointToSave";
import hideCurrentModal from "../../../utils/hideCurrentModal";
import useMapRepository from "../../../hooks/useMapRepository";
import SearchUserContent from "../../../components/SearchUserContent";
import ModalContainer from "../../../components/ModalContainer";
import defaultPostTypes from "../../../utils/defaultPostTypes";
import Icon from "../../../components/Icon";
import { renderWidget } from "../../../utils/point/widgetsMap";
import MediaManager from "../../../components/MediaManager";
import {
  useUser,
  useUserSubscriptionFeatures,
} from "../../../context/UserContext.ts";
import ExternalImageSourceSelector from "../../../components/ExternalImageSourceSelector";
import normalizeMediaUrl from "../../../utils/normalizeMediaUrl";

// const SpellsWidget = dynamic(() =>
//   import("")
// );

const PointDetailContext = createContext({});

function AddChildForm({ parent, onMapCreated = () => null }) {
  const { setGeneratingItem, setAdding } = useContext(PointDetailContext);
  const [newChild, setNewChild] = useState({});

  const { storeUserMap } = useMapRepository();

  newChild.parent_map_id = parent.id;

  function onMapUpload(image) {
    setNewChild({
      ...newChild,
      image,
    });
  }

  async function onSaveButtonClick() {
    if (newChild.name) {
      newChild.id = Date.now();
      await storeUserMap(newChild);
      onMapCreated();
      setAdding(false);
    }
  }

  return (
    <Card>
      <fieldset>
        <label htmlFor="name">Name</label>
        <input
          required
          value={newChild.name}
          onInput={(e) => setNewChild({ ...newChild, name: e.target.value })}
          type="name"
        />
        <label>Upload Map (optional)</label>
        <MediaManager.ModalButton
          key="child-form"
          onMediaSelected={(m) => onMapUpload(m.url)}
        />
      </fieldset>
      <div className="flex">
        <button onClick={onSaveButtonClick}>Save</button>
        <button onClick={() => setGeneratingItem(true)}>
          Generate an item
        </button>
        <button className="button--error" onClick={() => setAdding(false)}>
          Cancel
        </button>
      </div>
    </Card>
  );
}

function PointDetail({ router }) {
  const [point, setPoint] = useState({});
  const [children, setChildren] = useState([]);
  const [adding, setAdding] = useState(false);
  const [generatingItem, setGeneratingItem] = useState(false);
  const [loading, setLoading] = useState(false);
  const [nameInputValue, setNameInputValue] = useState("");
  const [descriptionInputValue, setDescriptionInputValue] = useState("");
  const [imageInputValue, setImageInputValue] = useState("");
  const [moving, setMoving] = useState(false);
  const [editDescription, setEditDescription] = useState(
    typeof localStorage !== "undefined" &&
      localStorage.getItem("editDescription") === "true"
  );
  const [userFeatures, loadingFeatures] = useUserSubscriptionFeatures();
  const [childrenTypeFilter, setChildrenTypeFilter] = useState(null);
  const [childrenTypes, setChildrenTypes] = useState([]);
  const user = useUser();
  const {
    deletePoint,
    getMapChildren,
    storeManyUserMaps,
    updatePoint,
    findPoint,
    getByType,
    getAllTypes,
  } = useMapRepository();

  useEffect(() => {
    localStorage.setItem("editDescription", editDescription ? "true" : "false");
  }, [editDescription]);

  useEffect(() => {
    (async () => {
      if (childrenTypeFilter) {
        const children = await getByType(childrenTypeFilter, point.id);
        console.log({ children });
        setChildren(children);
      } else {
        const children = await getMapChildren(point);
        setChildren(children);
      }
    })();
  }, [childrenTypeFilter]);

  useEffect(() => {
    if (editDescription) {
      localStorage.setItem("point.descriptionEdit", "1");
    } else {
      localStorage.setItem("point.descriptionEdit", "0");
    }
  }, [editDescription]);

  async function bootData() {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
    setLoading(true);
    if (router.query.pointId === "undefined") {
      router.replace("/");
    }
    const found = await findPoint(router.query.pointId);
    if (found) {
      const children = await getMapChildren(found);
      setPoint(found);
      setChildren(children);
      setNameInputValue(found.name);
      setDescriptionInputValue(found.description);
      setImageInputValue(found.image);
      const types = await getAllTypes(found.id);
      setChildrenTypes(types);
    }
    setTimeout(() => {
      setLoading(false);
    }, 400);
  }

  useEffect(() => {
    bootData();
    setEditDescription(localStorage.getItem("point.descriptionEdit") === "1");
    document.addEventListener("loadMaps", bootData);
    return () => {
      document.removeEventListener("loadMaps", bootData);
      router.events.off("routeChangeComplete", bootData);
    };
  }, [router]);

  useEffect(() => {
    clearTimeout(window._post_edit_timeout);
    window._post_edit_timeout = setTimeout(() => {
      updatePoint(point);
    }, 500);
  }, [point]);

  function duplicate() {
    const duplicate = { ...point };
    duplicate.id = Date.now();
    duplicate.name = `${duplicate.name} (copy)`;
    updatePoint(duplicate);
    router.push(guessLink(duplicate));
  }

  async function onContentGenerated(contentToSave) {
    setLoading(true);
    setTimeout(() => setLoading(false), 3000);
    const allChildren = [];
    function queueItemChildren(item) {
      if (item.children && Array.isArray(item.children)) {
        item.children.forEach((child) => {
          child.parent_map_id = item.id;
          allChildren.push(formatPointToSave(child, child.parent_map_id));
          if (child.children) {
            queueItemChildren(child);
          }
        });
      }
    }

    const items = [
      ...contentToSave.map((item) => {
        if (item.children) {
          queueItemChildren(item);
        }
        return formatPointToSave(item, point.id);
      }),
      ...allChildren,
    ];
    await storeManyUserMaps(items);
    setTimeout(() => {
      bootData();
    }, 1000);
    closeGeneratingModal();
  }

  function closeGeneratingModal() {
    hideCurrentModal().then(() => {
      setGeneratingItem(false);
    });
  }

  function moveCurrentItemToNewItem(newParent) {
    setPoint({
      ...point,
      parent_map_id: newParent.id,
    });
    hideCurrentModal().then(() => setMoving(false));
  }

  useEffect(() => {
    setPoint({
      ...point,
      name: nameInputValue,
    });
  }, [nameInputValue]);

  useEffect(() => {
    setPoint({
      ...point,
      description: descriptionInputValue,
    });
  }, [descriptionInputValue]);

  useEffect(() => {
    setPoint({
      ...point,
      image: imageInputValue,
    });
  }, [imageInputValue]);

  const mapLabel = point.type === "NPC" ? "Picture" : "Map";

  function removeWidget(index) {
    const newPoint = { ...point };
    newPoint.widgets.splice(index, 1);
    setPoint(newPoint);
  }

  function onWidgetUpdate(widgetId, newData) {
    const newPoint = { ...point };
    newPoint.widgets = newPoint.widgets.map((w) => {
      if (w.id == widgetId) {
        w.data = newData;
      }
      return w;
    });
    setPoint(newPoint);
  }

  function addSpellWidget() {
    const newPoint = { ...point };
    const widgets = newPoint.widgets || [];
    widgets.push({
      id: Date.now(),
      name: "spells",
      data: {},
    });
    newPoint.widgets = widgets;
    setPoint(newPoint);
  }

  function addHitPointsWidget() {
    const newPoint = { ...point };
    const widgets = newPoint.widgets || [];
    widgets.push({
      id: Date.now(),
      name: "hitPoints",
      data: {},
    });
    newPoint.widgets = widgets;
    setPoint(newPoint);
  }

  function addEquipmentWidget() {
    const newPoint = { ...point };
    const widgets = newPoint.widgets || [];
    widgets.push({
      id: Date.now(),
      name: "equipment",
      data: {},
    });
    newPoint.widgets = widgets;
    setPoint(newPoint);
  }

  function selectMapToAddAsPhase(map) {
    const phases = point.phases || [];
    phases.push(map.url);
    point.phases = phases;
    setPoint({
      ...point,
      phases,
    });
  }

  return (
    point && (
      <PointDetailContext.Provider
        value={{ adding, setAdding, setGeneratingItem, generatingItem }}
      >
        {loading && <Loader />}
        <Head>
          <title>{point.name}</title>
        </Head>
        <div className={styles.container}>
          <MapBreadcrumbs map={point} />
          <div>
            <SearchUserContent
              onItemClick={(item) => {
                router.push(guessLink(item));
              }}
            />
          </div>
          <div className={styles["heading"]}>
            <h1>
              <input
                className="input--full-width"
                onInput={(e) => setNameInputValue(e.target.value)}
                value={nameInputValue}
              ></input>
            </h1>
          </div>
          <div className="sm:flex margin-md--bottom overflow-auto hide-scrollbar">
            <button
              className="button--error button--small margin-xs"
              onClick={async () => {
                if (confirm("Are you sure you want to delete this?")) {
                  await deletePoint(point);
                  setTimeout(() => {
                    router.push(
                      !point.parent_map_id || point.parent_map_id !== "00000"
                        ? `/app/detail/${point.parent_map_id}`
                        : "/dashboard"
                    );
                  }, 1000);
                }
              }}
            >
              <Icon name="delete" className="size-sm" />
              Delete
            </button>
            <button
              className="button--info button--small margin-xs"
              onClick={() => {
                setMoving(true);
              }}
            >
              <Icon className="size-sm" name="trending_up" />
              Move
            </button>
            <button className="button--small margin-xs" onClick={duplicate}>
              <Icon className="size-sm" name="content_copy" />
              Duplicate
            </button>
          </div>
          <div>
            <label htmlFor="type">Change Post Type</label>
            <select
              id="type"
              onChange={(e) => {
                setPoint({
                  ...point,
                  type: e.target.value,
                });
              }}
            >
              {defaultPostTypes.map((type) => {
                return (
                  <option
                    selected={
                      !point.type ? type == "Thing" : type == point.type
                    }
                    value={type}
                    key={type}
                  >
                    {type}
                  </option>
                );
              })}
            </select>
          </div>
          <h2>{mapLabel}</h2>
          {point.image && (
            <Link href={mapLink(point)}>
              <a>View {mapLabel}</a>
            </Link>
          )}
          <div className="flex flex--wrap">
            <div className="text-center">
              {user && (
                <MediaManager.ModalButton
                  id="main-media-manager"
                  key="main"
                  onMediaSelected={(m) => setImageInputValue(m.url)}
                >
                  Select Map
                </MediaManager.ModalButton>
              )}
              {!loadingFeatures && (
                <div>
                  {userFeatures.mapPhases ? (
                    <MediaManager.ModalButton
                      id="phases-media-manager"
                      key="phases"
                      onMediaSelected={selectMapToAddAsPhase}
                    >
                      Add a map phase
                    </MediaManager.ModalButton>
                  ) : (
                    <div>
                      <Link passHref href="/pricing">
                        <a>
                          <button>Upgrade to add map phases.</button>
                        </a>
                      </Link>
                    </div>
                  )}
                </div>
              )}
            </div>
            {imageInputValue && (
              <Link passHref={true} href={mapLink(point)}>
                <a>
                  <div
                    className="map-preview"
                    style={{
                      background: `url(${normalizeMediaUrl(imageInputValue)})`,
                    }}
                  ></div>
                </a>
              </Link>
            )}
            {point.phases &&
              point.phases.length > 0 &&
              point.phases.map((phase, i) => (
                <Link key={phase} passHref={true} href={mapLink(point, i)}>
                  <a>
                    <div
                      className="map-preview relative"
                      style={{ background: `url(${normalizeMediaUrl(phase)})` }}
                    >
                      <Icon
                        className="absolute top--small right--small md-sm"
                        style={{ color: "var(--error)" }}
                        name="delete_forever"
                        onClick={(e) => {
                          e.preventDefault();
                          e.stopPropagation();
                          const phases = point.phases.filter(
                            (p) => p !== phase
                          );
                          setPoint({
                            ...point,
                            phases,
                          });
                        }}
                      ></Icon>
                    </div>
                  </a>
                </Link>
              ))}
          </div>
          <ExternalImageSourceSelector onImageSelect={setImageInputValue} />
          <div className="margin-lg--top">
            <button className="button--small" onClick={addSpellWidget}>
              Add Spell Widget
            </button>
            <button className="button--small" onClick={addHitPointsWidget}>
              Add Hit Points Widget
            </button>
            <button className="button--small" onClick={addEquipmentWidget}>
              Add Equpment Widget
            </button>
          </div>
          {point.widgets &&
            point.widgets.map((widget, index) => (
              <Card
                style={{ marginBottom: "12px" }}
                key={widget.id || widget.name}
              >
                {renderWidget(widget.name, widget.data, (data) =>
                  onWidgetUpdate(widget.id, data)
                )}
                <button
                  onClick={() => {
                    const confirmed = confirm(
                      "Are you sure you want to remove this widget an all of its data?"
                    );
                    if (confirmed) {
                      removeWidget(index);
                    }
                  }}
                  className="button--error button--small"
                >
                  <Icon className="size-sm" name="delete_forever" /> Remove
                  Widget
                </button>
              </Card>
            ))}
          <h2>Description:</h2>
          <label>
            Edit Mode:
            <input
              type="checkbox"
              onChange={(event) =>
                setEditDescription(event.currentTarget.checked)
              }
              checked={editDescription}
            />
          </label>
          {editDescription || editDescription === null ? (
            <WysiwygEditor
              onDescriptionChange={(e, editor) => {
                const description = editor.getData();
                if (!loading) {
                  setDescriptionInputValue(description);
                }
              }}
              data={descriptionInputValue}
            />
          ) : (
            <Card
              dangerouslySetInnerHTML={{ __html: descriptionInputValue }}
            ></Card>
          )}
          <h2>Children Items</h2>
          {adding ? (
            <AddChildForm
              parent={point}
              onMapCreated={() => {
                setTimeout(() => {
                  bootData();
                }, 500);
                setAdding(false);
              }}
            />
          ) : (
            <div>
              <div className="flex">
                <button className="margin-md" onClick={() => setAdding(true)}>
                  Add Child
                </button>
                <button
                  className="button--success margin-md"
                  onClick={() => setGeneratingItem(true)}
                >
                  Generate Content
                </button>
              </div>
            </div>
          )}
          <div className="sm:flex flex--wrap">
            <b>Filter:</b>{" "}
            {(childrenTypes || []).map((type) => (
              <button
                key={type}
                className={`margin-sm button--small ${
                  childrenTypeFilter === type ? "button--info" : ""
                }`.trim()}
                onClick={() => setChildrenTypeFilter(type)}
              >
                {type}
              </button>
            ))}
            <button
              className={`margin-sm button--small ${
                childrenTypeFilter === null ? "button--info" : ""
              }`.trim()}
              onClick={() => setChildrenTypeFilter(null)}
            >
              All
            </button>
          </div>
          <ViewMapChildrenList
            mapChildren={children}
            preview={typeof window !== "undefined" && window.innerWidth > 700}
          />
          {moving && (
            <ModalContainer onSelfHide={setMoving}>
              <Card>
                <h1>Select where you want to move it to.</h1>
                <SearchUserContent onItemClick={moveCurrentItemToNewItem} />
                <div
                  style={{
                    marginTop: "8px",
                  }}
                >
                  <button
                    onClick={() =>
                      hideCurrentModal().then(() => setMoving(false))
                    }
                    className="button--error button--small"
                  >
                    Cancel
                  </button>
                </div>
              </Card>
            </ModalContainer>
          )}
          {generatingItem && (
            <GenerateContentModal
              onModalClose={closeGeneratingModal}
              onContentGenerated={onContentGenerated}
            />
          )}
        </div>
      </PointDetailContext.Provider>
    )
  );
}

export default withRouter(PointDetail);
