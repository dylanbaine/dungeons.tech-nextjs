import { withRouter } from "next/router";
import { useEffect, useState } from "react";
import Map from "../../../components/Map/Map";
import useMapRepository from "../../../hooks/useMapRepository";
import { detailLink, mapLink } from "../../../utils/pointLlinks";

function PointMap(props) {
  const { findPoint, getMapChildren } = useMapRepository();
  const { router } = props;
  const [point, setPoint] = useState({});
  const [children, setChildren] = useState([]);
  const [image, setImage] = useState("");

  async function bootData() {
    const foundPoint = await findPoint(router.query.pointId);
    if (foundPoint) {
      if (!foundPoint.image && !router.query.phase) {
        router.replace(detailLink(foundPoint));
      } else {
        setPoint(foundPoint);
        setChildren(await getMapChildren(foundPoint));
      }
    }
  }

  useEffect(() => {
    bootData();
    gtag("event", "conversion", {
      send_to: "AW-626684793/Kr9WCJCIrIMDEPnm6aoC",
    });
    document.addEventListener("loadMaps", bootData);
    return () => {
      document.removeEventListener("loadMaps", bootData);
    };
  }, [router]);

  useEffect(() => {
    if (router.query.phase !== undefined && point.id) {
      const selectedImage = point.phases && point.phases[router.query.phase];
      if (selectedImage) {
        setImage(selectedImage);
      } else {
        router.push(mapLink(point));
      }
    } else {
      setImage(point.image);
    }
  }, [router, point]);

  return (
    <div>
      <Map
        onUpdate={bootData}
        map={point}
        childrenMaps={children}
        image={image}
      />
    </div>
  );
}

export default withRouter(PointMap);
