import { useEffect } from "react";
import useMapRepository from "../../hooks/useMapRepository";

export default function Dashboard(props) {
  const { findPoint } = useMapRepository();
  useEffect(() => {
    findPoint("1634496690092").then(console.log);
  }, []);
  return "dashboard";
}
