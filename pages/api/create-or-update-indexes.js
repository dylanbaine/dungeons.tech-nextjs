import getCurrentUser from "../../utils/getCurrentUser";
import createUserIndexes from "../../utils/server/createUserIndexes";

export default async function handler(req, res) {
  const user = getCurrentUser(req);
  const userIndexes = await createUserIndexes(user);
  console.log({ userIndexes });
  res.json(userIndexes ? userIndexes.map((i) => i && i.message) : {});
}
