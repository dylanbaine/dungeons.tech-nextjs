import Cookies from "cookies";
import config from "../../config";
import setAuthToken from "../../utils/client/setAuthToken";
import setCurrentUser from "../../utils/client/setCurrentUser";

export default async function handler(req, res) {
  const cookies = new Cookies(req, res);
  setAuthToken(config.null_cookie_value, cookies);
  setCurrentUser(config.null_cookie_value, cookies);
  res.json({ loggedOut: true });
}
