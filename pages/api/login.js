import axios from "axios";
import Cookies from "cookies";
import config from "../../config";
import setAuthToken from "../../utils/client/setAuthToken";
import setCurrentUser from "../../utils/client/setCurrentUser";
import createUserIndexes from "../../utils/server/createUserIndexes";

export default async function handler(req, res) {
  setTimeout(() => {
    try {
      res.json({
        error: "timed out",
      });
    } catch (e) {}
  }, 6000);

  const cookies = new Cookies(req, res);
  const { email, password } = req.body;
  const loginCreds = {
    email,
    password,
  };
  try {
    axios
      .post(`${config.api_base}/api/login`, loginCreds)
      .then(async (apiResponse) => {
        const { data } = apiResponse || {};
        if (data.token) {
          setAuthToken(data.token, cookies);
          setCurrentUser(data.user, cookies);
          await createUserIndexes(data.user);
          return res.json(data);
        } else {
          return res.status(422).json({ e: "no api token" });
        }
      })
      .catch((e) => {
        if (e.response) {
          return res.status(422).json(e.response.data);
        } else {
          return res.status(500).json({
            error: "Unknown Error",
          });
        }
      });
  } catch (e) {
    // console.log({ e });
    return res.json(e);
  }
}
