import getCurrentUserToken from "../../utils/server/getCurrentUserToken";

export default function handler(req, res) {
  const token = getCurrentUserToken(req);
  res.json({ token });
}
