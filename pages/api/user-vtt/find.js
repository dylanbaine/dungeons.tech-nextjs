import findPublishedMap from "../../../utils/published-maps/findPublishedMap";

export default async function handler(req, res) {
  const { id } = req.query;
  if (!id) {
    return res.status(400).send({
      error: "id is required",
    });
  }
  const object = await findPublishedMap(id);
  if (!object) {
    return res.status(404).json({ message: "Object not found" });
  }
  return res.json(object);
}
