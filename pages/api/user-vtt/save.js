import getCurrentUser from "../../../utils/getCurrentUser";
import savePublishedMap from "../../../utils/published-maps/savePublishedMap";

export default async function handler(req, res) {
  const user = getCurrentUser(req);
  if (!user) {
    return res.status(401).json({ message: "Unauthorized" });
  }
  const { objectId, phase = null, initiative = null } = req.query || req.body;
  const object = await savePublishedMap({
    objectId,
    user,
    phase,
    initiative: initiative && JSON.parse(initiative),
  });
  if (!object) {
    return res.status(404).json({ message: "Not found" });
  }
  const updatedObject = await user.meili().getDocument(objectId);
  return res.json({ message: "published object", updatedObject });
}
