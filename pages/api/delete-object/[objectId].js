import getCurrentUser from "../../../utils/getCurrentUser";
import loggedInMapRepository from "../../../map-repository/client-side/loggedInMapRepository";
import getAllObjectChildren from "../../../utils/getAllObjectChildren";

async function deleteObjectAndChildren(user, objectId) {
  const now = Date.now();
  console.log("starting object delete");
  try {
    const repo = loggedInMapRepository(user);
    const found = await repo.findPoint(objectId);
    if (found) {
      const children = await getAllObjectChildren(found, repo);
      await user.meili().deleteDocuments(children.map((c) => c.id));
    }
    console.log(
      `finished deleting ${children.length} objects in ${
        Date.now() - now
      } milliseconds`
    );
  } catch (error) {
    console.log("Error deleting object");
    console.log(error);
  }
}

export default async function handler(req, res) {
  const { objectId } = req.query;
  const user = getCurrentUser(req);
  if (user) {
    await deleteObjectAndChildren(user, objectId);
  }
  res.send();
}
