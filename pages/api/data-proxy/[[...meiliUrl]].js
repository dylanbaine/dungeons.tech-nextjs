import getCurrentUser from "../../../utils/getCurrentUser";

function getIndexFromUrl(url) {
  return (
    url.split("/")[1] ||
    url.match(/indexes\/(.+)\//)[0].replace(/\/|indexes/g, "")
  );
}

export default async function hander(req, res) {
  let queryParams = "";
  if (req.query) {
    Object.keys(req.query).forEach((key, i) => {
      if (key !== "meiliUrl") {
        queryParams += `${queryParams === "" ? "?" : "&"}${key}=${
          req.query[key]
        }`;
      }
    });
  }
  const targetUrl = req.query.meiliUrl.join("/") + queryParams;
  const currentUser = await getCurrentUser(req);
  if (!currentUser) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }
  const requestedIndex = getIndexFromUrl(targetUrl);
  if (!currentUser.searchIndexes.includes(requestedIndex)) {
    return res.status(401).json({
      message: "You don't have permission to manage that index.",
    });
  }

  const reqConfig = {
    method: req.method,
    headers: {
      "X-Meili-Api-Key": process.env.MEILISEARCH_KEY,
      "content-type": "application/json",
    },
  };
  if (reqConfig.method === "POST") {
    reqConfig.body = JSON.stringify(req.body);
  }

  const response = await fetch(
    `${process.env.MEILISEARCH_BACKEND_HOST}/${targetUrl}`,
    reqConfig
  )
    .then(async (_res) => {
      try {
        return await _res.json();
      } catch (message) {
        return { message };
      }
    })
    .catch((error) => {
      console.error({ error });
      res.statusCode = 500;
      return { error };
    });
  return res.json(response);
}
