import getAllOneshotEscepadesEpisodes from "../../../utils/server/getAllOneshotEscepadesEpisodes";
export default async function handler(req, res) {
  res.json(await getAllOneshotEscepadesEpisodes());
}
