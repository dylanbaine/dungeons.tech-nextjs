import axios from "axios";
import appConfig from "../../../config";
import authHeaders from "../../../utils/server/authHeaders";

export const config = {
  api: {
    bodyParser: {
      sizeLimit: "20mb",
    },
  },
};

export default async function handler(req, res) {
  try {
    const response = await axios.post(
      `${appConfig.api_base}/api/maps/sync`,
      req.body,
      {
        headers: {
          ...authHeaders(req),
        },
      }
    );
    res.json(response.data);
  } catch (e) {
    res.json({ e });
  }
}
