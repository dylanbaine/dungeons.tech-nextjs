import getTodaysAggregatedNews from "../../../utils/server/getTodaysAggregatedNews";

export default async function handler(req, res) {
  const news = await getTodaysAggregatedNews();
  res.json(news);
}
