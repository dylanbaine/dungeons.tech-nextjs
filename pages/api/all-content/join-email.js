import saveEmailLead from "../../../utils/server/saveEmailLead";
export default async function handlere(req, res) {
  if (req.body && req.body.email) {
    await saveEmailLead(req.body.email, "all-content");
  }
  res.end();
}
