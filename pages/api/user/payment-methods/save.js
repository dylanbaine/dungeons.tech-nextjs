import stripe from "../../../../stripe/stripe";
import User from "../../../../objects/User.ts";

export default async function handler(req, res) {
  const errors = {};
  const { body } = req;
  const number = body.number || (errors.number = "Missing card number");
  const exp_month =
    body.exp_month || (errors.exp_month = "Missing expiration month");
  const exp_year =
    body.exp_year || (errors.exp_year = "Missing expiration year");
  const cvc = body.cvc || (errors.cvc = "Missing CVC");
  if (Object.keys(errors).length > 0) {
    return res.status(422).json({ errors });
  }
  const user = await User.fromRequest(req);
  if (!user || !user.stripe_id) {
    return res.status(401).json({ error: "Unauthorized" });
  }
  const currentPaymentMethod = (await user.getStripeData()).cards[0];
  if (currentPaymentMethod) {
    await stripe.paymentMethods.detach(currentPaymentMethod.id);
  }
  const paymentMethod = await stripe.paymentMethods.create({
    type: "card",
    card: {
      number,
      exp_month,
      exp_year,
      cvc,
    },
  });
  await stripe.paymentMethods.attach(paymentMethod.id, {
    customer: user.stripe_id,
  });
  res.json(paymentMethod);
}
