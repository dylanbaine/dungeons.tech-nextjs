import User from "../../../objects/User.ts";
import stripe from "../../../stripe/stripe";

export default async function handler(req, res) {
  const user = await User.fromRequest(req);
  const subscription = req.body.subscription;
  if (!subscription) {
    return res
      .status(422)
      .json({ errors: { subscription: "subscription is required" } });
  }
  try {
    await stripe.subscriptions.del(subscription.subscriptionId);
    return res.json({ user, subscription });
  } catch (e) {
    return res.status(500).json({
      error: e.message,
    });
  }
}
