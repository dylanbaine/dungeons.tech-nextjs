import User from "../../../objects/User.ts";
import stripe from "../../../stripe/stripe";

export default async function handler(req, res) {
  const user = await User.fromRequest(req);
  const plan = req.body.plan;
  const paymentMethod = req.body.paymentMethod;
  if (!plan) {
    return res.status(422).json({ errors: { plan: "Plan is required" } });
  } else if (!paymentMethod) {
    return res
      .status(422)
      .json({ errors: { paymentMethod: "Payment method is required" } });
  }
  if (!user || !user.stripe_id) {
    return res.status(401).json({
      error: "Unauthorized",
    });
  }
  try {
    const subscription = await stripe.subscriptions.create({
      customer: user.stripe_id,
      items: [{ price: plan }],
      default_payment_method: paymentMethod,
    });
    res.json(subscription);
  } catch (e) {
    return res.status(500).json({
      error: e.message,
    });
  }
}
