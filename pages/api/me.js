import User from "../../objects/User.ts";

export default async function handler(req, res) {
  const user = await User.fromRequest(req);
  if (user) {
    const paymentProfile = await user.getStripeData();
    const storage = await user.getSubscriptionFeatures();
    return res.json({
      ...user,
      paymentProfile,
      storage,
    });
  }
  res.json({
    message: "User not found",
  });
}
