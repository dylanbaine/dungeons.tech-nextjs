import Cookies from "cookies";
import axios from "axios";
import config from "../../config";
import createUserIndexes from "../../utils/server/createUserIndexes";
import setCurrentUser from "../../utils/client/setCurrentUser";

export default async function handler(req, res) {
  const cookies = new Cookies(req, res);
  try {
    axios
      .post(`${config.api_base}/api/register`, req.body || {})
      .catch((e) => {
        if (e.response) {
          const { data } = e.response || {};
          return res.status(422).json(data);
        }
        return res.status(422).json({ e });
      })
      .then(async (apiResponse) => {
        if (!apiResponse) {
          console.error(apiResponse);
          return res.status(422).json({
            message: "An unexpected error occured. Please try again later.",
          });
        }
        const { data } = apiResponse;
        if (data.token) {
          cookies.set("api-token", data.token, {
            maxAge: Math.max(),
          });
          setCurrentUser(data.user, cookies);
        }
        await createUserIndexes(data.user);
        return res.json(data);
      });
  } catch (e) {
    return res.status(422).json({ e, error: true });
  }
}
