import router from "next/router";
import { useEffect } from "react";
import { useContentGenerator } from "../../context/ContentGeneratorContext";
import { guessLink } from "../../utils/pointLlinks";

export default function GenerateCity() {
  const contentGenerator = useContentGenerator();
  useEffect(() => {
    if (typeof window !== "undefined") {
      window.history.pushState(null, null, "/");
      contentGenerator.generateACity();
    }
  });
  return null;
}
