import Card from "../components/Card";
import FrontEndLayout from "../components/FrontEndLayout";
import Icon from "../components/Icon";
import Link from "next/link";
import plansList from "../stripe/plansList";
import { useState } from "react";
import { useUser } from "../context/UserContext.ts";
import FeaturesList from "../components/FeaturesList";

function Pricing({ paymentOptions, priceMode: priceModeProp }) {
  const [priceMode, setPriceMode] = useState(priceModeProp);
  const user = useUser();

  return (
    <FrontEndLayout>
      <main className="main--thin">
        <h1>Pricing</h1>
        <div className="button-group">
          <button
            className={priceMode === "yearly" ? "button--info" : ""}
            onClick={() => setPriceMode("yearly")}
          >
            Yearly
          </button>
          <button
            className={priceMode === "monthly" ? "button--info" : ""}
            onClick={() => setPriceMode("monthly")}
          >
            Monthly
          </button>
        </div>
        <div className="flex flex--wrap">
          {paymentOptions.map((option) => (
            <div
              className="third-width mobile:full-width margin-none border-box padded-md"
              key={option.name}
            >
              <Card>
                <h2 className="text-center">{option.name}</h2>
                {option.features.map((feature) => (
                  <div
                    className="sm:flex margin-auto items-center"
                    style={{
                      maxWidth: "250px",
                      borderBottom: "solid 1px #363636",
                      padding: "8px 0",
                    }}
                    key={feature}
                  >
                    <div className="margin-md--right">
                      <Icon className="text-success" name="task_alt" />
                    </div>
                    <div>{feature}</div>
                  </div>
                ))}
                {option.isFree ? (
                  <div className="text-center">
                    <h3>Free forever</h3>
                    <br></br>
                  </div>
                ) : (
                  <div className="text-center">
                    <h3>
                      $
                      {priceMode === "yearly"
                        ? option.yearly.monthlyPrice
                        : option.monthly.monthlyPrice}{" "}
                      a month when billed {priceMode}.
                    </h3>
                  </div>
                )}
                {option.isFree ? (
                  <Link passHref={true} href="/register">
                    <a>
                      <button
                        disabled={user}
                        className="button--block button--success"
                      >
                        Create an account
                      </button>
                    </a>
                  </Link>
                ) : (
                  <Link
                    href={`/dashboard/subscribe?plan=${
                      priceMode === "yearly"
                        ? option.yearly.id
                        : option.monthly.id
                    }`}
                  >
                    <a>
                      <button className="button--block button--success">
                        Sign Up
                      </button>
                    </a>
                  </Link>
                )}
              </Card>
            </div>
          ))}
        </div>
        <h2 className="text-center">Features</h2>
        <hr className="margin-lg--top"></hr>
        <FeaturesList cta={false} />
      </main>
    </FrontEndLayout>
  );
}

export async function getServerSideProps(app) {
  const props = {};
  props.paymentOptions = await plansList();
  props.priceMode = app.query.mode || "yearly";
  return { props };
}
export default Pricing;
