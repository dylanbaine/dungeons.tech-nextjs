/* eslint-disable  @next/next/no-img-element */
import { useEffect, useState } from "react";
import router from "next/router";
import Link from "next/link";
import Head from "next/head";
import { guessLink } from "../utils/pointLlinks";
import FrontEndLayout from "../components/FrontEndLayout";
import generateACity from "../utils/generateCity";
import useMapRepository from "../hooks/useMapRepository";
import generatorImages from "../utils/generatorImages";
import { useContentGenerator } from "../context/ContentGeneratorContext";
import { useUser } from "../context/UserContext.ts";
import FeaturesList from "../components/FeaturesList";

function GenerateCityButton({ generateACity, mainMaps }) {
  const generator = useContentGenerator();
  return (
    (mainMaps || []).length < 2 && (
      <button
        className="button--primary"
        onClick={() => generator.generateACity()}
      >
        GENERATE A CITY
      </button>
    )
  );
}

function Page(props) {
  const { getTopLevelMaps } = useMapRepository();
  const [mainMaps, setMainMaps] = useState([]);
  const [loading, setLoading] = useState(true);
  const user = useUser();

  useEffect(() => {
    (async () => {
      await loadMainMaps();
      setLoading(false);
    })();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  async function loadMainMaps() {
    setMainMaps(await getTopLevelMaps());
  }

  return (
    <FrontEndLayout>
      <main className="main--thin home-page">
        <Head>
          <title>
            Generate content for your Dungeons and Dragons campaign. |
            Dungeons.Tech
          </title>
          <meta
            name="description"
            content="A Content Generator for your Dungeons and Dragons Campaigns.
            Generate NPC's, Shops, and Cities with the click of a button.
            Focus on your story and plot, let Dungeons.Tech do the rest."
          />
          <link rel="canonical" href="https://dungeons.tech" />
        </Head>
        <h1>A Content Generator for your Dungeons and Dragons Campaigns</h1>
        <p>Focus on your story and plot, let Dungeons.Tech do the rest.</p>
        {mainMaps.length > 0 || user ? (
          <Link passHref href="/dashboard">
            <a>
              <button className="button--primary">Go to dashboard</button>
            </a>
          </Link>
        ) : (
          <GenerateCityButton
            mainMaps={mainMaps}
            generateACity={generateACity}
          />
        )}
        <h2>Unique random content on the fly.</h2>
        <p>
          No matter the situation, Dungeons.Tech generates content for your
          D&amp;D campaign quickly.
        </p>
        <FeaturesList />
      </main>
    </FrontEndLayout>
  );
}

Page.getInitialProps = async function (ctx) {
  const props = {};

  return {
    props,
  };
};
export default Page;
