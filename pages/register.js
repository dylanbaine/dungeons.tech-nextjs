import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Card from "../components/Card";
import { useUser } from "../context/UserContext.ts";
import FrontEndLayout from "../components/FrontEndLayout";
import setCurrentUser from "../utils/client/setCurrentUser";

import Spinner from "../components/Spinner";
import FeaturesList from "../components/FeaturesList";
import LogInWithGoogle from "../components/svg/LogInWithGoogle";

export default function Register(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const user = useUser();
  const router = useRouter();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (user) {
      router.replace("/dashboard");
    }
  }, []);

  async function register() {
    setError(null);
    setLoading(true);
    if (email && email.length && password && password.length) {
      axios
        .post("/api/register", {
          email,
          password,
          name,
        })
        .then((response) => {
          gtag("event", "conversion", {
            send_to: "AW-626684793/iyokCNb4g4MDEPnm6aoC",
          });
          const backTo = router.query.backTo || "/dashboard";
          setCurrentUser(response.data.user);
          setTimeout(async () => {
            window.location.replace(`/login/sync-data?backTo=${backTo}`);
          }, 1000);
        })
        .catch((e) => {
          if (e.response && e.response.data && e.response.data.message) {
            setError(e.response.data);
          } else {
            setError({
              message: "An unexpected error occureed. Please try again later.",
            });
          }
          setLoading(false);
        });
    }
  }

  if (user) {
    return null;
  }

  return (
    <FrontEndLayout>
      <main className="main--thin">
        <Card>
          <div className="flex flex--items-center">
            <h1>Register</h1>
            <a href="/api/app/auth/google" className="margin-auto--left">
              <button type="">
                <LogInWithGoogle />{" "}
                <span className="margin-md--left">Log In with google</span>
              </button>
            </a>
          </div>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              register();
            }}
          >
            {error && error.message && (
              <div className="error-state padded-md rounded">
                {error.message}
              </div>
            )}
            <fieldset>
              <label htmlFor="email">Email</label>
              <input
                required
                className="input--full-width"
                type="email"
                id="email"
                onInput={(e) => setEmail(e.target.value)}
              />
              {error && error.errors && error.errors.email && (
                <div className="error-state padded-md rounded">
                  {error.errors.email.join(", ")}
                </div>
              )}
            </fieldset>
            <fieldset>
              <label htmlFor="name">Username</label>
              <input
                required
                className="input--full-width"
                type="text"
                id="name"
                onInput={(e) => setName(e.target.value)}
              />
            </fieldset>
            <fieldset>
              <label htmlFor="password">Password</label>
              <input
                required
                className="input--full-width"
                type="password"
                onInput={(e) => setPassword(e.target.value)}
              />
            </fieldset>
            <div className="flex">
              <button disabled={loading} type="submit">
                {loading ? <Spinner size="small" /> : "Sign up"}
              </button>
              <Link href="/login">
                <a>Log In</a>
              </Link>
            </div>
          </form>
        </Card>
        <h2 className="text-center">Features</h2>
        <FeaturesList />
      </main>
    </FrontEndLayout>
  );
}
