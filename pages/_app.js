import "../styles/water.css";
import "../styles/global.scss";
import "../styles/icons.scss";
import InitiativeTool from "../components/InitiativeTool";
import Head from "next/head";
import { default as NextApp } from "next/app";
import UserContext from "../context/UserContext.ts";
import { useEffect, useMemo, useRef, useState } from "react";
import StateContext from "../context/StateContext";
import LeadGenEmailForm from "../components/LeadGenEmailForm";
import { ContentGeneratorProvider } from "../context/ContentGeneratorContext";
import loggedOutmapRepository from "../map-repository/client-side/loggedOutMapRepository";
import loggedInMapRepository from "../map-repository/client-side/loggedInMapRepository";
import getUserFromRequest from "../utils/getUserFromRequest.ts";
import User from "../objects/User.ts";
import mediaRepositoryFactory from "../media-repository.ts";
import axios from "axios";
import { useRouter } from "next/router";
import Loader from "../components/Loader";

const defaultBodyStyle = {
  transition: "all .3s",
};
export default function App({ Component, pageProps, cookieUser, _env }) {
  const [bodyStyle, setBodyStyle] = useState(defaultBodyStyle);
  const [routerLoading, setRouterLoading] = useState(false);
  const router = useRouter();

  const footerRef = useRef();
  if (typeof window !== "undefined") {
    if (!window._env) {
      window._env = _env;
      document.dispatchEvent(new Event("_env"));
    }
  }

  useEffect(() => {
    if (routerLoading) {
      setBodyStyle({
        transition: "all .3s",
        opacity: 0,
      });
    } else {
      setBodyStyle({
        transition: "all .3s",
        opacity: 1,
      });
    }
  }, [routerLoading]);

  useEffect(() => {
    function handleRouteChangeStart() {
      setRouterLoading(true);
    }
    function handleRouteChangeComplete() {
      setRouterLoading(false);
    }
    router.events.on("routeChangeStart", handleRouteChangeStart);
    router.events.on("routeChangeComplete", handleRouteChangeComplete);
    return () => {
      router.events.off("routeChangeStart", handleRouteChangeStart);
      router.events.off("routeChangeComplete", handleRouteChangeComplete);
    };
  }, [router]);

  const user = useMemo(() => {
    return cookieUser && User.fromCookie(cookieUser);
  }, [cookieUser]);
  const mapRepository = useMemo(() => {
    return user ? loggedInMapRepository(user) : loggedOutmapRepository();
  }, [user]);
  const mediaRepository = useMemo(() => {
    return user && mediaRepositoryFactory(user);
  }, [user]);

  console.log(`Map Repository State: ${mapRepository.type}`);

  useEffect(() => {
    const syncEventName = "_syncWithRemote";
    function syncRemoteEventCallback(e) {
      mapRepository.syncWithRemote();
    }
    user &&
      Object.keys(user).length &&
      document &&
      document.addEventListener(syncEventName, syncRemoteEventCallback);
    return () => {
      clearInterval(window.syncInterval);
      document.removeEventListener(syncEventName, syncRemoteEventCallback);
    };
  }, [user, mapRepository]);

  useEffect(() => {
    if (user) {
      typeof window !== "undefined" &&
        axios.get("/api/create-or-update-indexes");

      typeof FS !== "undefined" && FS.identify(user.id, user);
    }
  }, [user]);

  return (
    <>
      <UserContext.Provider value={user}>
        <StateContext.Provider
          value={{
            mapRepository,
            mediaRepository,
          }}
        >
          <ContentGeneratorProvider>
            <Head>
              <link
                href="https://fonts.googleapis.com/icon?family=Material+Icons"
                rel="stylesheet"
              />
              <link
                rel="icon"
                type="image/ico"
                href="https://res.cloudinary.com/drpocweiq/image/upload/v1627472216/Dungeons.Tech/favicon.ico"
              />
              {user && <title>{user.name} | Dungeons.Tech</title>}
              <script
                async
                src="https://www.googletagmanager.com/gtag/js?id=AW-626684793"
              ></script>
              <script
                async
                src="https://www.googletagmanager.com/gtag/js?id=UA-159409283-1"
              ></script>
              {/* eslint-disable-next-line */}
              <script src="/3rd-party.js"></script>
            </Head>
            {routerLoading && <Loader />}
            <div style={bodyStyle}>
              <Component {...pageProps} />
            </div>
            <InitiativeTool />
            <div
              style={{
                marginTop: "6px",
                height: `${
                  footerRef && footerRef.current
                    ? footerRef.current.offsetHeight || 20
                    : 20
                }px`,
              }}
            ></div>
            <footer
              ref={footerRef}
              style={{
                position: "fixed",
                left: "0",
                right: "0",
                bottom: "0",
                backgroundColor: "#fff",
                display: "flex",
                padding: "8px",
              }}
            >
              <LeadGenEmailForm />
              &copy; {new Date().getFullYear()} – Dungeons.Tech
            </footer>
          </ContentGeneratorProvider>
        </StateContext.Provider>
      </UserContext.Provider>
    </>
  );
}

App.getInitialProps = async (context) => {
  const props = await NextApp.getInitialProps(context);
  if (typeof window !== "undefined") {
    props.cookieUser = __NEXT_DATA__.props.cookieUser;
  } else {
    const cookieUser = await getUserFromRequest(
      context.ctx.req,
      context.ctx.res
    );
    if (cookieUser) {
      props.cookieUser = cookieUser;
    }
  }
  props._env = process.env.APP_ENV;
  return props;
};
