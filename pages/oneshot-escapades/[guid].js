import Head from "next/head";
import styles from "./OneshotEscapades.module.scss";
import getAllOneshotEscepadesEpisodes from "../../utils/server/getAllOneshotEscepadesEpisodes";
import Link from "next/link";
import FrontEndLayout from "../../components/FrontEndLayout";
import EmailLeadForm from "../../components/EmailLeadForm";

export default function OneshotEscepadesEpisode(props) {
  const { episode } = props;

  if (!episode) {
    return null;
  }

  return (
    <FrontEndLayout>
      <Head>
        <title>{episode.title} | The Oneshot Escapades Podcast</title>
        <meta name="description" content={episode.contentSnippet} />
        <link
          rel="canonical"
          href={`https://dungeons.tech/oneshot-escapses/${episode.guid}`}
        />
      </Head>
      <main
        className={`${styles["episode-card"]} main--thin`}
        style={{ minHeight: "50vh" }}
      >
        <Link href="/oneshot-escapades">
          <a>Oneshot Escapades</a>
        </Link>
        <span style={{ margin: "0 4px", display: "inline-block" }}>/</span>
        <b>{episode.title}</b>

        <h1>{episode.title}</h1>
        <p>{episode.pubDate}</p>
        <div
          style={{ marginBottom: "20px" }}
          dangerouslySetInnerHTML={{ __html: episode["content"] }}
        ></div>
        <audio
          style={{ width: "100%", marginBottom: "20px" }}
          controls={true}
          src={episode.enclosure.url}
        ></audio>
        <EmailLeadForm
          text="Be the first to know when new episodes come out."
          formUrl="/api/oneshot-escapses/join-email"
        />
      </main>
    </FrontEndLayout>
  );
}

export async function getStaticProps({ params }) {
  const feed = await getAllOneshotEscepadesEpisodes();
  const episode =
    feed.items.find((e) => {
      return e.guid == params.guid;
    }) || null;
  const props = { episode };
  return { props, revalidate: 60 };
}

export async function getStaticPaths() {
  const feed = await getAllOneshotEscepadesEpisodes();
  return {
    paths: feed.items.map((item) => `/oneshot-escapades/${item.guid}`),
    fallback: true,
  };
}
