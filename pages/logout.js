import setCurrentUser from "../utils/client/setCurrentUser";
import setAuthToken from "../utils/client/setAuthToken";
import Loader from "../components/Loader";
import config from "../config";
import Cookies from "cookies";
import { useEffect } from "react";

export default function Logout() {
  useEffect(() => {
    window.location.href = "/dashboard";
  }, []);
  return <Loader message="Logging you out..." />;
}

export function getServerSideProps({ req, res }) {
  const cookies = new Cookies(req, res);
  setAuthToken(config.null_cookie_value, cookies);
  setCurrentUser(config.null_cookie_value, cookies);
  return {
    props: {},
  };
}
