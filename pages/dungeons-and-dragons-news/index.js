import getTodaysAggregatedNews from "../../utils/server/getTodaysAggregatedNews";
import Head from "next/head";
import { useState } from "react";
import styles from "./DungeonsAndDragonsNews.module.scss";
import FrontEndLayout from "../../components/FrontEndLayout";
import EmailLeadForm from "../../components/EmailLeadForm";

export default function DungeonsAndDragonsNews(props) {
  const [hasJoinedEmail, setHasJoinedEmail] = useState(false);

  function getItemLink(item) {
    return new URL(item.link).searchParams.get("url");
  }

  function getItemSource(item) {
    return getItemLink(item)
      .replace(/(http|https):\/\/|/g, "")
      .split("/")[0];
  }

  const { items } = props;
  return (
    <FrontEndLayout>
      <main className="main--thin">
        <Head>
          <title>Dungeons and Dragons Content | Dungeons.Tech</title>
          <meta name="description" content={props.pageDescription} />
          <link
            rel="canonical"
            href="https://dungeons.tech/dungeons-and-dragons-news"
          />
        </Head>
        <h1 className={styles["page-title"]}>
          Dungeons and Dragons News and Content from Around the Internet!
        </h1>
        <EmailLeadForm
          text="
            Sign up for our news letter to stay up to date on the latest Dungeons and Dragons content.
        "
          formUrl="/api/all-content/join-email"
        />
        <section className={styles["item-list"]}>
          {items.length === 0 && (
            <div className="margin-md text-center width-full">
              Sorry, no news yet. Check back later.
            </div>
          )}
          {items.map((item) => (
            <article className={styles["item-card"]} key={item.link}>
              <div className={styles["item-card__content"]}>
                <a target="_blank" rel="noreferrer" href={getItemLink(item)}>
                  <h2 dangerouslySetInnerHTML={{ __html: item.title }}></h2>
                </a>
                <p>{item.contentSnippet}</p>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://${getItemSource(item)}`}
                  className={styles["news-source"]}
                >
                  {getItemSource(item)}
                </a>
              </div>
            </article>
          ))}
        </section>
      </main>
    </FrontEndLayout>
  );
}

export async function getStaticProps(ctx) {
  const items = await getTodaysAggregatedNews();
  const props = {
    items,
    pageDescription: `Meet Edna. Electronic Dungeon News Aggregator. Stay up to date with
    all the latest news in Dungeons & Dragons and nerd culture.`,
  };
  return { props, revalidate: 60 * 60 };
}
