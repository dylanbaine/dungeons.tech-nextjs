import Link from "next/link";
import Card from "../../components/Card";
import EmailLeadForm from "../../components/EmailLeadForm";
import FrontEndLayout from "../../components/FrontEndLayout";
import Head from "next/head";

export default function ExploreContent() {
  const items = [
    {
      text: "Oneshot Escapades",
      href: "/oneshot-escapades",
      description: `Each week, Jackson and Dylan–along with a guest–play a oneshot from the
      Dungeons & Dragons community. They review the game and give Dungeon
      Masters tips on what may help them run the module.`,
    },
    {
      text: "Dungeons and Dragons News",
      href: "/dungeons-and-dragons-news",
      description: `Meet Edna. Electronic Dungeon News Aggregator. Stay up to date with
      all the latest news in Dungeons & Dragons and nerd culture.`,
    },
  ];
  return (
    <FrontEndLayout>
      <Head>
        <title>Explore Dungeons and Dragons Content | Dungeons.Tech</title>
        <link rel="canonical" href="https://dungeons.tech/explore-content" />
      </Head>
      <main className="main--thin">
        <EmailLeadForm
          text="
            Sign up for our news letter to stay up to date on the latest Dungeons and Dragons content.
        "
          formUrl="/api/all-content/join-email"
        />
        <div
          className="flex"
          style={{ alignItems: "start", minHeight: "60vh" }}
        >
          {items.map((item) => (
            <Link key={item.href} passHref={true} href={item.href}>
              <a className="padded-lg" style={{ width: "50%" }}>
                <Card>
                  <h2>{item.text}</h2>
                  <p>{item.description}</p>
                </Card>
              </a>
            </Link>
          ))}
        </div>
      </main>
    </FrontEndLayout>
  );
}
