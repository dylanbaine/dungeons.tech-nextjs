import FeaturesList from "../components/FeaturesList";
import FrontEndLayout from "../components/FrontEndLayout";
export default function Dev() {
  return (
    <FrontEndLayout>
      <div className="main--thin">
        <FeaturesList />
      </div>
    </FrontEndLayout>
  );
}
