import axios from "axios";
import Loader from "../../../components/Loader";
import qs from "qs";
import setAuthToken from "../../../utils/client/setAuthToken";
import setCurrentUser from "../../../utils/client/setCurrentUser";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Cookies from "cookies";

export default function LoginWithGoogleCallback(props) {
  const router = useRouter();
  useEffect(() => {
    if (props.success) {
      window.location.href = "/login/sync-data";
    } else {
      window.location.href = "/register";
    }
  }, [props.success]);
  return <Loader message="Logging you in..." />;
}

export async function getServerSideProps({ query, req, res }) {
  const props = {};
  const oauthResponse = await axios
    .get(
      `${process.env.API_BASE}/api/auth/google/callback?${qs.stringify(query)}`
    )
    .then((res) => res.data);
  console.log({ oauthResponse });
  const { user, auth, message } = oauthResponse;
  if (auth) {
    const cookies = new Cookies(req, res);
    setAuthToken(user.token, cookies);
    setCurrentUser(user.user, cookies);
    props.success = true;
  } else {
    props.success = false;
    console.console.error("google auth error", message);
  }
  return { props };
}
