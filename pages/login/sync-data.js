import { useEffect } from "react";
import Loader from "../../components/Loader";
import { useRouter } from "next/router";
import useMapRepository from "../../hooks/useMapRepository";

export default function SyncData() {
  const router = useRouter();
  const mapRepository = useMapRepository();

  useEffect(() => {
    mapRepository.syncWithRemote().finally(() => {
      const { backTo = "/dashboard" } = router.query || {};
      setTimeout(() => {
        window.location.replace(backTo);
      }, 2000);
    });
  }, []);

  return <Loader message="Syncing your data..." />;
}
