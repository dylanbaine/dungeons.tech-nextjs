import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Card from "../components/Card";
import Link from "next/link";
import FrontEndLayout from "../components/FrontEndLayout";
import setCurrentUser from "../utils/client/setCurrentUser";
import Spinner from "../components/Spinner";
import { useUser } from "../context/UserContext.ts";
import LogInWithGoogle from "../components/svg/LogInWithGoogle";

export default function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const user = useUser();
  const router = useRouter();

  useEffect(() => {
    if (user && Object.keys(user).length > 1) {
      router.replace("/dashboard");
    }
  }, []);

  useEffect(() => {
    router.query.emai && setEmail(router.query.email);
  }, [router.query]);

  async function attemptLogin() {
    setError(null);
    setLoading(true);
    if (email && email.length && password && password.length) {
      axios
        .post("/api/login", {
          email,
          password,
        })
        .then(async (res) => {
          setCurrentUser(res.data.user);
          window.location.replace("/login/sync-data");
        })
        .catch((e) => {
          if (e.response.data && e.response.data.message) {
            setError(e.response.data.message);
          } else {
            setError("An error occurred. Please try again later.");
          }
          setLoading(false);
        });
    }
  }

  if (user) {
    return null;
  }

  return (
    <FrontEndLayout>
      <main className="main--thin">
        <Card>
          <div className="flex flex--items-center">
            <h1>Log In</h1>
            <a href="/api/app/auth/google" className="margin-auto--left">
              <button type="">
                <LogInWithGoogle />{" "}
                <span className="margin-md--left">Log In with google</span>
              </button>
            </a>
          </div>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              attemptLogin();
            }}
          >
            {error && (
              <div className="error-state padded-md rounded">{error}</div>
            )}
            <fieldset>
              <label htmlFor="email">Email</label>
              <input
                required
                className="input--full-width"
                type="email"
                id="email"
                onInput={(e) => setEmail(e.target.value)}
              />
            </fieldset>
            <fieldset>
              <label htmlFor="password">Password</label>
              <input
                required
                className="input--full-width"
                type="password"
                onInput={(e) => setPassword(e.target.value)}
              />
            </fieldset>
            <div className="flex">
              <button disabled={loading} type="submit">
                {loading ? <Spinner size="small" /> : "Log In"}
              </button>
              <Link href="/register">
                <a>Register</a>
              </Link>
              <Link href={`/passwords/reset?email=${email}`}>
                <a style={{ marginLeft: "12px" }}>Reset Password</a>
              </Link>
            </div>
          </form>
        </Card>
      </main>
    </FrontEndLayout>
  );
}
