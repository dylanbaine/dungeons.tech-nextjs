import { useRouter } from "next/dist/client/router";
import { useEffect, useState } from "react";
import Head from "next/head";
import Map from "../../components/Map/Map";
import Loader from "../../components/Loader";
import EditMap from "../../components/Map/EditMap";
import hideCurrentModal from "../../utils/hideCurrentModal";
import PointDetail from "../../components/Map/PointDetail";
import useMapRepository from "../../hooks/useMapRepository";

export default function ShowPoint(props) {
  const { findPoint, getMapChildren } = useMapRepository();
  const router = useRouter();
  const [point, setPoint] = useState({});
  const [children, setChildren] = useState([]);
  const editing =
    router.query.method && router.query.method[0] === "edit" ? true : false;

  useEffect(() => {
    setTimeout(async () => {
      loadPoint();
    }, 10);
    function loadMapsListener() {
      setTimeout(() => {
        loadPoint();
      }, 300);
    }
    document.addEventListener("loadMaps", loadMapsListener);
    return () => {
      document.removeEventListener("loadMaps", loadMapsListener);
    };
  }, [router]);

  async function loadPoint() {
    const point = await findPoint(router.query.pointId);
    if (point) {
      setPoint(point);
      //   if (!point.image && !editing) {
      //     // router.replace(`/${point.id}/edit`);
      //   }
      setChildren(await getMapChildren(point));
    }
  }

  async function onMapEdited(map) {
    if (map.image) {
      await hideCurrentModal();
      router.push(`/${map.id}`);
    }
  }

  function PointHead() {
    return (
      <Head>
        <title>{point.name} - Dungeons.Tech</title>
      </Head>
    );
  }
  if (!point) return <Loader />;
  return (
    <>
      <PointHead />
      {editing && <EditMap onMapEdited={onMapEdited} map={point} />}
      {point.image ? (
        <Map map={point} childrenMaps={children} />
      ) : (
        <PointDetail point={point} />
      )}
    </>
  );
}
