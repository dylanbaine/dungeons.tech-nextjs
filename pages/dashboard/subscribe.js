import router, { useRouter } from "next/router";
import { useEffect, useState } from "react";
import FrontEndLayout from "../../components/FrontEndLayout";
import plansListGetter from "../../stripe/plansList";
import User from "../../objects/User.ts";
import Card from "../../components/Card";
import Spinner from "../../components/Spinner";
import axios from "axios";
import Link from "next/link";
import { useUser } from "../../context/UserContext.ts";
import FeaturesList from "../../components/FeaturesList";

function AddPaymentMethod({ onPaymentMethodAdded, existingPaymentMethod }) {
  const [cardNumber, setCardNumber] = useState(null);
  const [cardExpiryMonth, setCardExpiryMonth] = useState(null);
  const [cardExpiryYear, setCardExpiryYear] = useState(null);
  const [cardCvc, setCardCvc] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    console.log({ existingPaymentMethod });
  }, [existingPaymentMethod]);

  async function savePaymentMethod(e) {
    e.preventDefault();
    setLoading(true);
    axios
      .post("/api/user/payment-methods/save", {
        number: cardNumber,
        exp_month: cardExpiryMonth,
        exp_year: cardExpiryYear,
        cvc: cardCvc,
      })
      .then(({ data }) => {
        onPaymentMethodAdded(data);
      })
      .catch(({ response }) => {
        alert("An error with your payment info occured.");
        console.log(response.data);
      });
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }

  return (
    <div>
      <Card>
        <h2>Add a payment method.</h2>
        <form onSubmit={savePaymentMethod}>
          <fieldset>
            <label htmlFor="card-number">Card Number</label>
            <input
              required
              onInput={(e) => setCardNumber(e.target.value)}
              id="card-number"
              type="text"
              className="input--full-width"
              placeholder="4242 4242 4242 4242"
              name="card_number"
            />
          </fieldset>
          <fieldset className="sm:flex">
            <div className="padded-md margin-md--right">
              <label htmlFor="exp-month">Expiration Date</label>
              <div className="sm:flex">
                <input
                  required
                  onInput={(e) => setCardExpiryMonth(e.target.value)}
                  id="exp-month"
                  className="input--small"
                  name="exp_month"
                  placeholder="MM"
                ></input>
                <input
                  required
                  onInput={(e) => setCardExpiryYear(e.target.value)}
                  className="input--small"
                  name="exp_year"
                  placeholder="YYYY"
                ></input>
              </div>
            </div>
            <div>
              <label htmlFor="cvc">CVC</label>
              <input
                onInput={(e) => setCardCvc(e.target.value)}
                id="cvc"
                type="text"
                className="input--small"
                placeholder="123"
                name="cvc"
              />
            </div>
          </fieldset>
          <div className="flex">
            <button disabled={loading} className="button--success">
              {loading ? <Spinner size="small" /> : "Save"}
            </button>
            {existingPaymentMethod && (
              <button
                onClick={() => onPaymentMethodAdded(existingPaymentMethod)}
                className="button--error"
              >
                Cancel Edit
              </button>
            )}
          </div>
        </form>
      </Card>
      <h2 className="text-center">Features</h2>
      <hr className="margin-lg--top"></hr>
      <FeaturesList cta={false} />
    </div>
  );
}

function SubscribeFlow({
  selectedPlanId,
  subscriptionState,
  setSubscriptionState,
  paymentProfile,
}) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const user = useUser();

  function confirmSubscription() {
    setLoading(true);
    axios
      .post("/api/user/subscribe", {
        plan: selectedPlanId,
        paymentMethod: subscriptionState.paymentMethod.id,
      })
      .then(() => router.push("/dashboard"))
      .catch(() => setLoading(false));
  }

  useEffect(() => {
    if (!user) {
      router.push({
        pathname: "/register",
        query: {
          backTo: router.asPath,
        },
      });
    }
  }, [user]);

  if (!user) {
    return "You must be logged in to subscribe.";
  }

  if (paymentProfile.subscriptions.length > 0) {
    return "Already subscribed";
  }

  if (!subscriptionState.paymentMethod) {
    return (
      <AddPaymentMethod
        existingPaymentMethod={paymentProfile.cards && paymentProfile.cards[0]}
        onPaymentMethodAdded={(method) =>
          setSubscriptionState({
            ...setSubscriptionState,
            paymentMethod: method,
          })
        }
      />
    );
  }

  return (
    <div className="flex">
      <button
        disabled={loading}
        onClick={confirmSubscription}
        className="button--success"
      >
        {loading ? <Spinner size="small" /> : "CONFIRM SUBSCRIPTION"}
      </button>
    </div>
  );
}

export default function Subscribe({
  redirect,
  plan,
  amountToCharge,
  frequency,
  paymentProfile,
  selectedPlanId,
}) {
  const [subscriptionState, setSubscriptionState] = useState({});
  useEffect(() => {
    if (redirect) {
      router.replace(redirect);
    }
  }, [redirect]);

  useEffect(() => {
    if (paymentProfile && paymentProfile.cards.length > 0) {
      setSubscriptionState({
        ...subscriptionState,
        paymentMethod: paymentProfile.cards[0],
      });
    }
  }, [paymentProfile]);

  return (
    !redirect && (
      <FrontEndLayout>
        <main className="main--thin">
          <h1>
            Sign up for {plan.name} <small>(billed {frequency})</small>
          </h1>
          <h2>
            ${amountToCharge} {frequency === "yearly" ? "a year" : "a month"}.
          </h2>
          {subscriptionState.paymentMethod && (
            <p>
              Paying with card ending in{" "}
              {subscriptionState.paymentMethod.card.last4}.
              <button
                onClick={() =>
                  setSubscriptionState({
                    paymentMethod: null,
                  })
                }
                className="button--small"
              >
                Change Payment Method
              </button>
            </p>
          )}
          <SubscribeFlow
            selectedPlanId={selectedPlanId}
            subscriptionState={subscriptionState}
            setSubscriptionState={setSubscriptionState}
            paymentProfile={paymentProfile}
          />
        </main>
      </FrontEndLayout>
    )
  );
}

export async function getServerSideProps(app) {
  const props = {};
  const plansList = await plansListGetter();
  let frequency = null;
  const selectedPlan = app.query.plan;
  props.selectedPlanId = selectedPlan;
  props.plan = plansList.find((plan) => {
    if (plan.isFree) return false;
    if (selectedPlan === plan.yearly.id) {
      frequency = "yearly";
      return true;
    }
    if (selectedPlan === plan.monthly.id) {
      frequency = "monthly";
      return true;
    }
  });

  if (!props.plan) {
    return {
      props: {
        redirect: "/pricing",
      },
    };
  }
  const user = await User.fromRequest(app.req);
  props.paymentProfile = user && (await user.getStripeData());
  props.amountToCharge =
    frequency === "monthly" ? props.plan.monthly.pay : props.plan.yearly.pay;
  props.frequency = frequency;
  return { props };
}
