import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import FrontEndLayout from "../../components/FrontEndLayout";
import User from "../../objects/User.ts";
import Link from "next/link";
import axios from "axios";
import Spinner from "../../components/Spinner";

export default function Subscriptions({ subscription }) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!subscription) {
      router.push("/dashboard");
    }
  }, [subscription, router]);

  async function cancelSubscription() {
    if (confirm("Are you sure you want to cancel your subscription?")) {
      setLoading(true);
      try {
        await axios.post("/api/user/cancel-subscription", { subscription });
      } catch (error) {
        console.log(error);
      }
      setLoading(false);
      router.push("/dashboard");
    }
  }

  if (!subscription) return null;
  return (
    <FrontEndLayout>
      <main className="main--thin">
        <h1>{subscription.name}</h1>
        <div className="flex">
          <Link passHref href="/dashboard">
            <a>
              <button className="button--info">Back to dashboard</button>
            </a>
          </Link>
          <button onClick={cancelSubscription} className="button--error">
            {loading ? <Spinner size="small" /> : "Cancel subscription"}
          </button>
        </div>
      </main>
    </FrontEndLayout>
  );
}

export async function getServerSideProps({ query, req }) {
  const user = await User.fromRequest(req);
  const subscription = (await user.getSubscriptions()).find(
    (subscription) => subscription.id === query.id
  );
  return {
    props: {
      subscription: subscription ? subscription.toObject() : null,
    },
  };
}
