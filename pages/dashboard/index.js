import React, { useEffect, useMemo, useState } from "react";
import FrontEndLayout from "../../components/FrontEndLayout";
import useMapRepository from "../../hooks/useMapRepository";
import Link from "next/link";
import { guessLink } from "../../utils/pointLlinks";
import Icon from "../../components/Icon";
import Spinner from "../../components/Spinner";
import { useContentGenerator } from "../../context/ContentGeneratorContext";
import Card from "../../components/Card";
import Image from "next/image";
import { useUser } from "../../context/UserContext.ts";
import MediaManager from "../../components/MediaManager";
import SearchUserContent from "../../components/SearchUserContent";
import { useRouter } from "next/router";

function WelcomeLoggedInUser({ user }) {
  const [currentSubscriptions, setCurrentSubscriptions] = useState([]);
  const [loading, setLoading] = useState(true);

  async function loadCurrentSubscriptions() {
    if (user) {
      setLoading(true);
      const subs = await user.getSubscriptions();
      setCurrentSubscriptions(subs);
      setLoading(false);
    }
  }

  useEffect(() => {
    loadCurrentSubscriptions();
  }, []);

  return (
    <div>
      <p>
        Logged in as {user.name}. <Link href="/logout">Log Out</Link>
      </p>
      <div>
        <h3 className="margin-none margin-md--right">Subscriptions</h3>
      </div>
      {loading ? (
        <Spinner size="small" />
      ) : (
        <React.Fragment>
          <div className="flex flex--align-center">
            {currentSubscriptions.length === 0 && (
              <div>
                <p>You aren&apos;t subscribed to a plan.</p>
                <Link passHref href="/pricing">
                  <a>
                    <button>Browse Subscriptions</button>
                  </a>
                </Link>
              </div>
            )}
            {currentSubscriptions.map((subscription) => {
              return (
                <Card key={subscription.name}>
                  <div>
                    <b>{subscription.name}</b>
                  </div>
                  <div>
                    ${subscription.intervalCost} a {subscription.intervalName}{" "}
                    payed {subscription.intervalPaid.toLowerCase()}.
                  </div>
                  <div className="margin-md--top">
                    <Link
                      passHref
                      href={`/dashboard/my-subscriptions?id=${subscription.id}`}
                    >
                      <a>
                        <button className="button--small">Manage</button>
                      </a>
                    </Link>
                  </div>
                </Card>
              );
            })}
          </div>
        </React.Fragment>
      )}
    </div>
  );
}

function Welcome({ user }) {
  if (user) return <WelcomeLoggedInUser user={user} />;

  return (
    <div className="alert alert--info">
      <p>
        You are not logged in. By creating an account, you can sync your content
        across all of your devices.
      </p>
      <Link href="/register">
        <button className="button--info">REGISTER NOW</button>
      </Link>
    </div>
  );
}

export default function Dashboard(props) {
  const user = useUser();
  const mapRepository = useMapRepository();
  const { getTopLevelMaps, count: countMaps, getAllTypes } = mapRepository;
  const [mainMaps, setMainMaps] = useState([]);
  const [totalMapsUsed, setTotalMapsUsed] = useState(0);
  const [loading, setLoading] = useState(true);
  const generator = useContentGenerator();
  const [allObjectTypes, setAllObjectTypes] = useState([]);
  const [objectTypeFilter, setObjectTypeFilter] = useState(null);
  const router = useRouter();

  const canGenerate = useMemo(() => {
    if (user) {
      return true;
    }
    return mainMaps && mainMaps.length < 1;
  }, [user, mainMaps]);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await loadMainMaps();
      await loadAllObjectTypes();
      setTimeout(() => {
        setLoading(false);
      }, 500);
    })();
    gtag("event", "conversion", {
      send_to: "AW-626684793/j8BHCMOehIMDEPnm6aoC",
    });
  }, []);

  useEffect(() => {
    (async () => {
      if (objectTypeFilter) {
        setMainMaps(await mapRepository.getByType(objectTypeFilter));
      } else {
        loadMainMaps();
      }
    })();
  }, [objectTypeFilter]);

  async function loadAllObjectTypes() {
    setAllObjectTypes(await getAllTypes());
  }

  async function loadMainMaps() {
    setMainMaps(await getTopLevelMaps());
    setTotalMapsUsed(await countMaps());
  }

  return (
    <FrontEndLayout>
      <main className="main--thin">
        <Welcome user={user} />
        <h1>Dashboard</h1>
        <div className="flex">
          {user && user.vttLink && (
            <Link href={user.vttLink}>
              <a>
                <button>Your VTT</button>
              </a>
            </Link>
          )}
          <MediaManager.ModalButton
            onMediaSelected={(media) => {
              window.open(media.url);
            }}
          />
        </div>
        {loading ? (
          <Spinner />
        ) : (
          <section>
            <p>
              {Number.isInteger(totalMapsUsed) &&
                `You have ${totalMapsUsed} objects saved.`}
            </p>
            <div className="sm:flex flex--wrap">
              <b>Filter:</b>{" "}
              {(allObjectTypes || []).map((type) => (
                <button
                  key={type}
                  className={`margin-sm button--small ${
                    objectTypeFilter === type ? "button--info" : ""
                  }`.trim()}
                  onClick={() => setObjectTypeFilter(type)}
                >
                  {type}
                </button>
              ))}
              <button
                className={`margin-sm button--small ${
                  objectTypeFilter === null ? "button--info" : ""
                }`.trim()}
                onClick={() => setObjectTypeFilter(null)}
              >
                Main Maps
              </button>
            </div>
            <SearchUserContent
              onItemClick={(item) => router.push(guessLink(item))}
              showRegister={false}
            />
            <ul className="flex flex--wrap padded-none">
              {mainMaps &&
                mainMaps.map((item) => (
                  <li
                    className="list-none margin-none padded-sm"
                    style={{
                      width:
                        typeof window !== "undefined" && window.innerWidth > 600
                          ? "33.33%"
                          : "100%",
                      boxSizing: "border-box",
                    }}
                    key={item.id}
                  >
                    <Link passHref href={guessLink(item)}>
                      <a>
                        <Card
                          className="relative padded-none"
                          style={{
                            overflow: "hidden",
                            maxWidth: "100%",
                            padding: "0",
                          }}
                        >
                          {item.image && (
                            <Image
                              loader={({ src }) => {
                                return src;
                              }}
                              alt=""
                              objectFit="cover"
                              width={600}
                              height={600}
                              src={item.image}
                            />
                          )}
                          <div className="padded-sm">
                            <button className="flex button--info full-width justify-center">
                              <Icon name="chevron_right" /> {item.name}
                            </button>
                          </div>
                        </Card>
                      </a>
                    </Link>
                  </li>
                ))}
            </ul>
            {canGenerate && (
              <button
                className="button--primary"
                onClick={() => generator.generateACity()}
              >
                GENERATE A CITY
              </button>
            )}
          </section>
        )}
      </main>
    </FrontEndLayout>
  );
}
