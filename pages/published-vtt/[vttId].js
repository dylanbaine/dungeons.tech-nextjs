import findPublishedMap from "../../utils/published-maps/findPublishedMap";
import Map from "../../components/Map/Map";
import React, { useEffect, useMemo, useState } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Script from "next/script";
import { vttKey } from "../../utils/vttKey";
import Card from "../../components/Card";
import Icon from "../../components/Icon";

const initiativeStyle = {
  zIndex: 999,
  width: "200px",
};
function VttInitiative({ initiative }) {
  const [hidden, setHidden] = useState(false);
  console.log({ initiative });
  if (hidden)
    return (
      <button
        style={{ zIndex: 999 }}
        className="fixed bottom right"
        onClick={() => setHidden(false)}
      >
        Initiative
      </button>
    );
  return (
    <Card className="fixed bottom right" style={initiativeStyle}>
      <div className="sm:flex flex--align-center">
        <h4>Initiative</h4>
        <Icon
          className="block margin-auto--left pointer"
          onClick={() => setHidden(true)}
          name="close"
        />
      </div>
      {initiative.members.map((member, i) => {
        return (
          <div
            style={{
              border: initiative.currentIndex === i && `solid 1px var(--info)`,
            }}
            className="padded-md margin-md rounded"
            key={member.name}
          >
            {member.name}
          </div>
        );
      })}
    </Card>
  );
}
export default function PublishedMap(props) {
  const router = useRouter();
  const [map, setMap] = useState({});
  const [updateNotify, setUpdateNotify] = useState(false);

  async function fetchMap() {
    const { query } = router;
    const newMap = await findPublishedMap(query.mapId, query.userId);
    setMap(newMap);
  }

  useEffect(() => {
    setMap(props.map);
  }, [props.map]);

  function bootPusher() {
    Pusher.logToConsole = true;
    const pusher = new Pusher("27f7669159149ab0646b", {
      cluster: "us2",
    });
    const channel = pusher.subscribe("Dungeons.Tech-production");
    const eventName = vttKey(props.asPath);
    console.log({ eventName });
    channel.bind(eventName, ({ message }) => {
      const incomingMap = JSON.parse(message);
      console.log({ incomingMap });
      setMap(incomingMap);
      setUpdateNotify(true);
      setTimeout(() => {
        setUpdateNotify(false);
      }, 4000);
    });
  }

  const pageTitle = useMemo(() => {
    if (map) {
      return map.name;
    }
    if (props.map) {
      return props.map.name;
    }
    return "Not found";
  }, [props.map, map]);

  if (!props.map) {
    return <div className="text-center">Virtual table top not found :(</div>;
  }

  return (
    <React.Fragment>
      <Head>
        <title>
          {updateNotify ? "🟢" : ""} {pageTitle}
        </title>
        {/* eslint-disable-next-line */}
      </Head>
      {map ? (
        <Map
          context="public"
          map={map}
          image={map.currentImage}
          childrenMaps={map.childrenMaps || []}
        />
      ) : (
        "VTT not published"
      )}
      {map.initiative && map.initiative.members && (
        <VttInitiative initiative={map.initiative} />
      )}

      <Script
        src="https://js.pusher.com/7.0/pusher.min.js"
        onLoad={bootPusher}
      ></Script>
    </React.Fragment>
  );
}

PublishedMap.getInitialProps = async function ({ req, asPath }) {
  const props = {};
  const map = await findPublishedMap(asPath);
  console.log({ map });
  props.map = map;
  props.asPath = asPath;
  return props;
};
