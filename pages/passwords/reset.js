import FrontEndLayout from "../../components/FrontEndLayout";
import Card from "../../components/Card";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Spinner from "../../components/Spinner";

export default function ResetPassword(props) {
  const [error, setError] = useState(null);
  const [email, setEmail] = useState(null);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const router = useRouter();

  function requestResetEmail() {
    setLoading(true);
    setError(false);
    setSuccess(false);
    axios
      .post(`/api/app/reset-password`, { email })
      .then(() => {
        setSuccess(true);
      })
      .catch((e) => {
        setError("An unknown error occured");
      })
      .finally(() => setLoading(false));
  }

  useEffect(() => {
    router.query.email && setEmail(router.query.email);
  }, [router.query]);

  return (
    <FrontEndLayout>
      <main className="main--thin">
        {success && (
          <Card className="success-state margin-md--bottom">
            <h1>Reset email sent!</h1>
            <Link href="/login">
              <a>Back to login</a>
            </Link>
          </Card>
        )}
        <Card>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              requestResetEmail();
            }}
          >
            <h1>Log In</h1>
            {error && (
              <div className="error-state padded-md rounded">{error}</div>
            )}
            <fieldset>
              <label htmlFor="email">Email</label>
              <input
                className="input--full-width"
                defaultValue={router.query.email}
                type="email"
                id="email"
                onInput={(e) => setEmail(e.target.value)}
              />
            </fieldset>
            <div className="flex">
              <button disabled={loading} type="submit">
                {loading ? <Spinner size="small" /> : "Send Reset Link"}
              </button>
              <Link passHref={false} href="/login">
                <button className="button--error">Cancel</button>
              </Link>
            </div>
          </form>
        </Card>
      </main>
    </FrontEndLayout>
  );
}
