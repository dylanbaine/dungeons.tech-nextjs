import FrontEndLayout from "../../components/FrontEndLayout";
import Card from "../../components/Card";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Spinner from "../../components/Spinner";

function NoTokenView() {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => {
      router.replace("/login");
    }, 1200);
  }, []);
  return <Card>This page has expired...</Card>;
}

export default function UpdatePassword(props) {
  const [error, setError] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  function resetEmail() {
    setLoading(true);
    setError(false);
    axios
      .post(`/api/app/update-password`, { email, token, password })
      .then(() => {
        router.replace(`/login?email=${email}`);
      })
      .catch((e) => {
        setError("An unknown error occured");
      })
      .finally(() => setLoading(false));
  }

  useEffect(() => {
    router.query.email && setEmail(router.query.email);
    router.query.token && setToken(router.query.token);
  }, [router.query]);

  return (
    <FrontEndLayout>
      <main className="main--thin">
        {!router.query.token ? (
          <NoTokenView />
        ) : (
          <Card>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                resetEmail();
              }}
            >
              <h1>Set your new password</h1>
              {error && (
                <div className="error-state padded-md rounded">{error}</div>
              )}
              <fieldset>
                <label htmlFor="password">New Password</label>
                <input
                  className="input--full-width"
                  type="password"
                  id="password"
                  onInput={(e) => setPassword(e.target.value)}
                />
              </fieldset>
              <button disabled={loading} type="submit">
                {loading ? <Spinner size="small" /> : "Update Password"}
              </button>
            </form>
          </Card>
        )}
      </main>
    </FrontEndLayout>
  );
}
