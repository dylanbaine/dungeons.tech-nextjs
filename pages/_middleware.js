import { NextRequest, NextResponse } from "next/server";
/**
 *
 * @param {NextRequest} req
 */
export default function Middleware(req, res) {
  const { geo } = req;
  const response = NextResponse.next();
  const expires = new Date();
  expires.setFullYear(expires.getFullYear() + 5);
  response.cookie("geo", JSON.stringify(geo), {
    expires,
  });
  return response;
}
