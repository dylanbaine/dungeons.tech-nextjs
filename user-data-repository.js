const initiativeKey = "initiative";

export async function getPersistedInitiative() {
  const stored = localStorage.getItem(initiativeKey);
  if (stored) {
    return JSON.parse(stored);
  }
  return [];
}

export async function addItemToPersistedInitiative(item) {
  const stored = await getPersistedInitiative();
  const newStored = [...stored, item];
  localStorage.setItem(initiativeKey, JSON.stringify(newStored));
}

export async function clearPersistedInitiative() {
  localStorage.setItem(initiativeKey, "[]");
}

export function setPersistedInitiative(members) {
  localStorage.setItem(initiativeKey, JSON.stringify(members));
}
