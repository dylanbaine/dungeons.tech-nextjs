export default function throwIfMissing(field) {
  throw new Error(`Missing field: ${field}`);
}
