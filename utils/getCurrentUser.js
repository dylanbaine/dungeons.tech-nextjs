import Cookies from "cookies";
import User from "../objects/User.ts";
import throwIfMissing from "./throwIfMissing";

export default function getCurrentUser(httpReq = throwIfMissing("httpReq")) {
  const cookies = new Cookies(httpReq, {});
  const userCookie = cookies.get("user-data");
  if (userCookie) {
    return User.fromCookie(userCookie);
  }
  return null;
}
