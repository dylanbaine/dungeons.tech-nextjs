import { MeiliSearch } from "meilisearch";

export default function meiliClient(user = null, index = "posts") {
  function serverSide() {
    const client = new MeiliSearch({
      host: process.env.MEILISEARCH_BACKEND_HOST,
      apiKey: process.env.MEILISEARCH_KEY,
    });
    if (user) {
      return client.index(`${user.meili_search_index_namespace}_${index}`);
    }
    return client;
  }

  function clientSide() {
    const client = new MeiliSearch({
      host: `${window.location.origin}/api/data-proxy`,
      apiKey: "_",
    });
    return client.index(`${user.meili_search_index_namespace}_${index}`);
  }

  function admin(params) {}

  if (typeof window === "undefined") {
    return serverSide();
  }
  return clientSide();
}
