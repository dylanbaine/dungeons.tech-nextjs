// https://www.npmjs.com/package/browser-image-compression
import imageCompression from "browser-image-compression";

export default async function compressImage(file) {
  const options = {
    maxSizeMB: 0.3, // (default: Number.POSITIVE_INFINITY)
    maxWidthOrHeight: 1000, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
    // but, automatically reduce the size to smaller than the maximum Canvas size supported by each browser.
    // Please check the Caveat part for details.
    onProgress: () => null, // optional, a function takes one progress argument (percentage from 0 to 100)
    useWebWorker: false, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)

    //   // following options are for advanced users
    //   maxIteration: number, // optional, max number of iteration to compress the image (default: 10)
    //   exifOrientation: number, // optional, see https://stackoverflow.com/a/32490603/10395024
    //   fileType: string, // optional, fileType override
    //   initialQuality: number, // optional, initial quality value between 0 and 1 (default: 1)
  };
  const result = await imageCompression(file, options);
  return result;
}
