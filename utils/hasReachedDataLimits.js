import envExecute from "./envExecute";

export default function hasReachedDataLimits() {
    return envExecute({
        client() {
            return localStorage.getItem('data-limit-reached');
        }
    })
}