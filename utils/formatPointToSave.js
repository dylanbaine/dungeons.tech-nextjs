export default function formatPointToSave(item, parentId = null) {
  const formatted = {
    id: item.id,
    name: item.name,
    description: item.description,
    type: item.type,
    left: item.left,
    top: item.top,
  };
  if (parentId) {
    formatted.parent_map_id = parentId;
  }
  return formatted;
}
