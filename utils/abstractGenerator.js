import ViewMapChildrenList from "../components/Map/ViewMapChildrenList";
import { randomInt } from "./generate";

function generateCoords() {
  return randomInt(1, 9) * 0.1;
}

export default function abstractGenerator({
  generateChildren = () => [],
  name,
  description = "",
  top,
  left,
  type,
  image,
}) {
  const id = Date.now() + randomInt(0, 99999);
  const item = {
    id,
    name,
    image,
    description,
    left: left || generateCoords(),
    top: top || generateCoords(),
    type,
    children: generateChildren({ id }),
  };
  if (item.children && item.children.length > 0) {
    item.preview = function Preview() {
      return <ViewMapChildrenList preview mapChildren={item.children} />;
    };
  }
  return item;
}
