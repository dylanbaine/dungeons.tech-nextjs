const subRaces = {
  name: "Sub Races",
  data: [
    {
      index: 1,
      name: "Hill Dwarf",
      race: {
        index: "1",
        name: "Dwarf",
      },
      desc: "As a hill dwarf, you have keen senses, deep intuition, and remarkable resilience.",
      ability_bonuses: [0, 0, 0, 0, 1, 0],
      "starting_proficiencies:": [],
      languages: [],
      racial_traits: [
        {
          name: "Dwarven Toughness",
          index: "4",
        },
      ],
    },
    {
      index: 2,
      name: "High Elf",
      race: {
        index: "2",
        name: "Elf",
      },
      desc: "As a high elf, you have a keen mind and a mastery of at least the basics of magic. In many fantasy gaming worlds, there are two kinds of high elves. One type is haughty and reclusive, believing themselves to be superior to non-elves and even other elves. The other type is more common and more friendly, and often encountered among humans and other races.",
      ability_bonuses: [0, 0, 0, 1, 0, 0],
      "starting_proficiencies:": [
        {
          name: "Longswords",
          url: "http://www.dnd5eapi.co/api/proficiencies/42",
        },
        {
          name: "Shortswords",
          url: "http://www.dnd5eapi.co/api/proficiencies/48",
        },
        {
          name: "Shortbows",
          url: "http://www.dnd5eapi.co/api/proficiencies/33",
        },
        {
          name: "Longbows",
          url: "http://www.dnd5eapi.co/api/proficiencies/56",
        },
      ],
      languages: [],
      language_options: {
        choose: 1,
        from: [
          {
            name: "Dwarvish",
            url: "http://www.dnd5eapi.co/api/languages/2",
          },
          {
            name: "Giant",
            url: "http://www.dnd5eapi.co/api/languages/4",
          },
          {
            name: "Gnomish",
            url: "http://www.dnd5eapi.co/api/languages/5",
          },
          {
            name: "Goblin",
            url: "http://www.dnd5eapi.co/api/languages/6",
          },
          {
            name: "Halfling",
            url: "http://www.dnd5eapi.co/api/languages/7",
          },
          {
            name: "Orc",
            url: "http://www.dnd5eapi.co/api/languages/8",
          },
          {
            name: "Abyssal",
            url: "http://www.dnd5eapi.co/api/languages/9",
          },
          {
            name: "Celestial",
            url: "http://www.dnd5eapi.co/api/languages/10",
          },
          {
            name: "Draconic",
            url: "http://www.dnd5eapi.co/api/languages/11",
          },
          {
            name: "Deep Speech",
            url: "http://www.dnd5eapi.co/api/languages/12",
          },
          {
            name: "Infernal",
            url: "http://www.dnd5eapi.co/api/languages/13",
          },
          {
            name: "Primordial",
            url: "http://www.dnd5eapi.co/api/languages/14",
          },
          {
            name: "Sylvan",
            url: "http://www.dnd5eapi.co/api/languages/15",
          },
          {
            name: "Undercommon",
            url: "http://www.dnd5eapi.co/api/languages/16",
          },
        ],
        type: "language",
      },
      racial_traits: [],
      racial_trait_options: {
        choose: 1,
        from: [
          {
            name: "High Elf Cantrip: Light",
            index: "9",
          },
          {
            name: "High Elf Cantrip: Mage Hand",
            index: "10",
          },
          {
            name: "High Elf Cantrip: Mending",
            index: "11",
          },
          {
            name: "High Elf Cantrip: Message",
            index: "12",
          },
          {
            name: "High Elf Cantrip: Minor Illusion",
            index: "13",
          },
          {
            name: "High Elf Cantrip: Acid Splash",
            index: "14",
          },
          {
            name: "High Elf Cantrip: Prestidigitation",
            index: "15",
          },
          {
            name: "High Elf Cantrip: Ray of Frost",
            index: "16",
          },
          {
            name: "High Elf Cantrip: Shocking Grasp",
            index: "17",
          },
          {
            name: "High Elf Cantrip: True Strike",
            index: "18",
          },
          {
            name: "High Elf Cantrip: Chill Touch",
            index: "19",
          },
          {
            name: "High Elf Cantrip: Dancing Lights",
            index: "20",
          },
        ],
        type: "trait",
      },
    },
    {
      index: 3,
      name: "Lightfoot Halfling",
      race: {
        index: "3",
        name: "Halfling",
      },
      desc: "As a lightfoot halfling, you can easily hide from notice, even using other people as cover. You’re inclined to be affable and get along well with others. Lightfoots are more prone to wanderlust than other halflings, and often dwell alongside other races or take up a nomadic life.",
      ability_bonuses: [0, 0, 0, 0, 0, 1],
      "starting_proficiencies:": [],
      languages: [],
      racial_traits: [
        {
          name: "Naturally Stealthy",
          index: "23",
        },
      ],
    },
    {
      index: 4,
      name: "Mountain Dwarf",
      race: {
        index: "1",
        name: "Dwarf",
      },
      desc: "As a mountain dwarf, you're strong and hardy, accustomed to a difficult life in rugged terrain. You're probably on the tall side (for a dwarf), and tend toward lighter coloration.",
      ability_bonuses: [2, 0, 0, 0, 0, 0],
      "starting_proficiencies:": [
        {
          name: "Medium armor",
          url: "http://www.dnd5eapi.co/api/proficiencies/2",
        },
        {
          name: "Heavy armor",
          url: "http://www.dnd5eapi.co/api/proficiencies/3",
        },
      ],
      languages: [],
      racial_traits: [],
    },
    {
      index: 5,
      name: "Wood Elf",
      race: {
        index: "2",
        name: "Elf",
      },
      desc: "As a wood elf, you have keen senses and intuition, and your fleet feet carry you quickly through your native forests.",
      ability_bonuses: [0, 0, 0, 0, 1, 0],
      "starting_proficiencies:": [
        {
          name: "Longswords",
          url: "http://www.dnd5eapi.co/api/proficiencies/42",
        },
        {
          name: "Shortswords",
          url: "http://www.dnd5eapi.co/api/proficiencies/48",
        },
        {
          name: "Shortbows",
          url: "http://www.dnd5eapi.co/api/proficiencies/33",
        },
        {
          name: "Longbows",
          url: "http://www.dnd5eapi.co/api/proficiencies/56",
        },
      ],
      languages: [],
      racial_traits: [
        {
          name: "Fleet of Foot",
          index: "37",
        },
        {
          name: "Mask of the Wild",
          index: "38",
        },
      ],
    },
    {
      index: 6,
      name: "Dark Elf (Drow)",
      race: {
        index: "2",
        name: "Elf",
      },
      desc: "As a wood elf, you have keen senses and intuition, and your fleet feet carry you quickly through your native forrests.",
      ability_bonuses: [0, 0, 0, 0, 0, 1],
      "starting_proficiencies:": [
        {
          name: "Rapiers",
          url: "http://www.dnd5eapi.co/api/proficiencies/46",
        },
        {
          name: "Shortswords",
          url: "http://www.dnd5eapi.co/api/proficiencies/48",
        },
        {
          name: "Crossbows, hand",
          url: "http://www.dnd5eapi.co/api/proficiencies/54",
        },
      ],
      languages: [],
      racial_traits: [
        {
          name: "Superior Darkvision",
          index: "39",
        },
        {
          name: "Sunlight Sensitivity",
          index: "40",
        },
        {
          name: "Drow Magic",
          index: "41",
        },
      ],
    },
  ],
};
export default subRaces;
