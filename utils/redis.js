let redis = {};
if (typeof window === "undefined") {
  const RedisConnector = require("ioredis");
  redis = new RedisConnector(
    "rediss://:91b2b818140843a9bc2b269892f10f29@gusc1-humane-kid-30758.upstash.io:30758"
  );
}
export default redis;
