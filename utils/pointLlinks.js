function isNumberic(point) {
  return (
    Number.isInteger(point) ||
    (typeof point === "string" &&
      parseInt(point) > 0 &&
      parseInt(point) !== NaN)
  );
}
function getId(point) {
  if (!point) return null;
  if (isNumberic(point)) {
    return parseInt(point);
  }
  return point.id;
}

export function guessLink(point) {
  if (isNumberic(point)) {
    return detailLink(point);
  }
  if (typeof window !== "undefined" && window.innerWidth < 800) {
    return detailLink(point);
  }
  if (point.image) {
    return mapLink(point);
  }
  return detailLink(point);
}

export function detailLink(point) {
  return `/app/detail/${getId(point)}`;
}

export function mapLink(point, phase = null) {
  let phaseParam = "";
  if (phase !== null) {
    phaseParam = `?phase=${phase}`;
  }
  return `/app/map/${getId(point)}${phaseParam}`;
}

export function editLink(point) {
  return detailLink(point);
  //   return `/app/edit/${getId(point)}`;
}
