const generatorImages = {
  npc: "https://res.cloudinary.com/drpocweiq/image/upload/v1636818193/Dungeons.Tech/alexander-andrews-wBPJA1ZxMo8-unsplash_ocgfdj.jpg",
  city: "https://res.cloudinary.com/drpocweiq/image/upload/v1627471104/Dungeons.Tech/city-1675290_640.jpg",
  tavern:
    "https://res.cloudinary.com/drpocweiq/image/upload/v1627470194/Dungeons.Tech/still-life-379858_640.jpg",
  magicShop:
    "https://res.cloudinary.com/drpocweiq/image/upload/v1627470201/Dungeons.Tech/witchcraft-4893559_640.jpg",
  blacksmith:
    "https://res.cloudinary.com/drpocweiq/image/upload/v1627470198/Dungeons.Tech/horseshoe-1516269_640.jpg",
};
export default generatorImages;
