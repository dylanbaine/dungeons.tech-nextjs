import faker, { name } from "faker";
import abstractGenerator from "./abstractGenerator";

const vowels = "aeiou";

function splitWordOnRandomVowel(word) {
  let crawledLetters = "";
  const results = [];
  word.split("").forEach((letter) => {
    crawledLetters += letter;
    if (vowels.includes(letter)) {
      results.push(crawledLetters.replace(/\s|`/, "").toLowerCase());
      crawledLetters = "";
    }
  });
  return results;
}

function mergeWords(...words) {
  let allParts = [];
  words.forEach((word) => {
    allParts = [...splitWordOnRandomVowel(word), allParts];
  });
  return `${allParts[0]}${allParts[randomInt(1, allParts.length - 1)]}`.replace(
    /,/g,
    ""
  );
}

function capitalizeWord(word) {
  return `${word.charAt(0).toUpperCase()}${word.split("").splice(1).join("")}`;
}

export function generateRandomCityName() {
  const city1 = faker.address.city().split(" ");
  const city2 = faker.address.city().split(" ");
  const word = mergeWords(
    city1[randomInt(0, city1.length - 1)],
    city2[randomInt(0, city2.length - 1)]
  ).replaceAll(",", "");
  return capitalizeWord(
    word
      .split("")
      .splice(randomInt(5, word.length - 1) * -1)
      .join("")
  );
}

function randomNameFromPartsA() {
  const nm1 = [
    "Aell",
    "Aer",
    "Air",
    "Ar",
    "Av",
    "Bel",
    "Ber",
    "Caell",
    "Cal",
    "Cec",
    "Cel",
    "Crel",
    "Cyl",
    "Der",
    "Des",
    "Dhon",
    "Dhyl",
    "Dor",
    "Dys",
    "Faen",
    "Fean",
    "Fer",
    "Flor",
    "Glor",
    "God",
    "Gwyn",
    "Gyl",
    "Hell",
    "Hem",
    "Heph",
    "Hyn",
    "Hyst",
    "Ial",
    "Ier",
    "Ill",
    "Ion",
    "Laer",
    "Lin",
    "Lor",
    "Lyn",
    "Lyph",
    "Lys",
    "Lyv",
    "Maer",
    "Men",
    "Mes",
    "Mor",
    "Myn",
    "Nel",
    "Nell",
    "Neph",
    "Ner",
    "Nor",
    "Nyph",
    "Nyr",
    "Nys",
    "Oc",
    "Ocyp",
    "Oll",
    "Or",
    "Orin",
    "Pel",
    "Per",
    "Phel",
    "Phlor",
    "Phyn",
    "Pod",
    "Sav",
    "Seph",
    "Sol",
    "Syl",
    "Syr",
    "Taer",
    "Ten",
    "Thel",
    "Ther",
    "Thyl",
    "Tyl",
    "Typh",
    "Tyr",
    "Tyv",
    "Uem",
    "Ur",
    "Uv",
    "Vael",
    "Vel",
    "Ver",
    "Vol",
    "Vyn",
    "Xel",
    "Xir",
    "Xyl",
    "Xyn",
    "Yen",
    "Yes",
    "Yor",
    "Zean",
    "Zel",
    "Zeph",
    "Zer",
  ];
  const nm2 = [
    "a",
    "aene",
    "aeno",
    "alin",
    "alis",
    "alle",
    "ane",
    "aphe",
    "aphine",
    "ara",
    "arge",
    "aria",
    "ase",
    "asha",
    "asis",
    "ea",
    "eano",
    "eanor",
    "efis",
    "elle",
    "ena",
    "enne",
    "eo",
    "ephise",
    "era",
    "erin",
    "eris",
    "ete",
    "ethe",
    "evis",
    "ia",
    "ial",
    "ialle",
    "iana",
    "iane",
    "iara",
    "ie",
    "ielle",
    "iene",
    "inis",
    "inore",
    "iphis",
    "iris",
    "is",
    "ise",
    "o",
    "oah",
    "oe",
    "oelle",
    "oene",
    "oinne",
    "ola",
    "one",
    "onia",
    "ophine",
    "ophis",
    "ora",
    "orena",
    "oris",
    "oya",
    "ya",
    "yana",
    "ylia",
    "ylis",
    "yne",
    "ynea",
    "ynne",
    "ynore",
    "yore",
    "yphe",
    "yre",
    "yrea",
    "yris",
    "ys",
    "yth",
  ];
  return (
    nm1[randomInt(0, nm1.length - 1)] +
    nm2[randomInt(0, nm2.length - 1)] +
    (randomInt(1, 20) === 1 ? "'" + nm2[randomInt(0, nm2.length)] : "")
  );
}

function randomFromArray(array) {
  return array[randomInt(0, array.length - 1)];
}

function randomNameFromPartsB() {
  const nm1 = [
    "Abban",
    "Abel",
    "Adam",
    "Adaue",
    "Aedan",
    "Aland",
    "Albyn",
    "Aldyn",
    "Aleyn",
    "Alister",
    "Allow",
    "Allowe",
    "Altar",
    "Andreays",
    "Andrew",
    "Anghus",
    "Art",
    "Arteure",
    "Asketil",
    "Aslac",
    "Asmund",
    "Aspallan",
    "Austeyn",
    "Baran",
    "Bertram",
    "Bertrem",
    "Biorn",
    "Brendan",
    "Bretan",
    "Bretnach",
    "Brice",
    "Cairbe",
    "Cane",
    "Carbry",
    "Carmac",
    "Carmick",
    "Caroly",
    "Cashin",
    "Christopher",
    "Cian",
    "Colby",
    "Colla",
    "Colum",
    "Colyn",
    "Conal",
    "Connaghyn",
    "Conylt",
    "Coobragh",
    "Cooil",
    "Cormac",
    "Cristal",
    "Cristen",
    "Crosse",
    "Custal",
    "David",
    "Denis",
    "Dermot",
    "Diarmid",
    "Dilno",
    "Dofnald",
    "Dogan",
    "Dolen",
    "Dollin",
    "Dolyn",
    "Donachan",
    "Donal",
    "Donald",
    "Doncan",
    "Donn",
    "Dooil",
    "Doolish",
    "Douglas",
    "Dufgal",
    "Dugal",
    "Eaghan",
    "Eamon",
    "Ean",
    "Ector",
    "Edard",
    "Edmund",
    "Edward",
    "Elisha",
    "Eoin",
    "Erling",
    "Ewan",
    "Faragher",
    "Fayrhare",
    "Felys",
    "Fergus",
    "Fiac",
    "Filip",
    "Finbar",
    "Finlo",
    "Fintan",
    "Flan",
    "Flaxney",
    "Fogal",
    "Franc",
    "Fynlo",
    "Fynn",
    "Galfrid",
    "Gavan",
    "Gawain",
    "Geffry",
    "Geoffrey",
    "George",
    "German",
    "Germot",
    "Gibbon",
    "Gilander",
    "Gilandrew",
    "Gilaspic",
    "Gilbert",
    "Gilbrid",
    "Gilcalm",
    "Gilchrist ",
    "Gilcolm",
    "Gilmartin",
    "Gilmere",
    "Gilmurry",
    "Gilno",
    "Gilrea",
    "Godred",
    "Gorman",
    "Gorry",
    "Hamish",
    "Hane",
    "Harold",
    "Henry",
    "Heremon",
    "Herman",
    "Huan",
    "Huchon",
    "Hugen",
    "Hugh",
    "Hugo",
    "Illiam",
    "Inry",
    "Ivanhoe",
    "Ivar",
    "James",
    "Jamys",
    "Jenkyn",
    "Jocelin",
    "John",
    "Jole",
    "Juan",
    "Keird",
    "Kerron",
    "Kieran",
    "Laurence",
    "Laurys",
    "Lear",
    "Liag",
    "Lochlin",
    "Loghlan",
    "Lolan",
    "Lonan",
    "Lucas",
    "Magnus",
    "Mannanan",
    "Manus",
    "Marcus",
    "Mark",
    "Markys",
    "Martyn",
    "Maurice",
    "Mayl",
    "Miall",
    "Mian",
    "Michael",
    "Michel",
    "Mold",
    "Moleyn",
    "Monier",
    "Moris",
    "Morris",
    "Murchad",
    "Murdagh",
    "Murghad",
    "Nel",
    "Nele",
    "Nellyn",
    "Nevyn",
    "Niall",
    "Nichol",
    "Nicholas",
    "Nigel",
    "Odairr",
    "Okerfair",
    "Olaf",
    "Olave",
    "Olyn",
    "Orry",
    "Oshin",
    "Ossian",
    "Oter",
    "Otes",
    "Otnel",
    "Ottar",
    "Padeen",
    "Paric",
    "Parick",
    "Parlance",
    "Paton",
    "Patrick",
    "Paul",
    "Payl",
    "Peddyr",
    "Peric",
    "Perick",
    "Peter",
    "Quistaghyn",
    "Quisten",
    "Ramsey",
    "Ranlyn",
    "Reginald",
    "Reynold",
    "Richard",
    "Rigard",
    "Robart",
    "Robert",
    "Robyn",
    "Roddy",
    "Rody",
    "Roger",
    "Ronan",
    "Rory",
    "Rumund",
    "Sandulf",
    "Sharry",
    "Sigurd",
    "Sigvald",
    "Silvester",
    "Stephen",
    "Stiurt",
    "Stoill",
    "Teige",
    "Thomas",
    "Thomase",
    "Thomlyn",
    "Thorfin",
    "Thorgil",
    "Thorkell",
    "Thormot",
    "Thorryn",
    "Thorulf",
    "Urmen",
    "Waltar",
    "William",
    "Ysaig",
    "Yuan",
    "Yvar",
    "Yveno",
    "Yvon",
    "Yvor",
  ];
  const nm2 = [
    "Aalid",
    "Aalin",
    "Aaue",
    "Aedyt",
    "Aelid",
    "Agnes",
    "Aileen",
    "Ailstreena",
    "Aimel",
    "Ainle",
    "Alice",
    "Andreca",
    "Aufrica",
    "Aurick",
    "Averick",
    "Bahee",
    "Bahey",
    "Bahie",
    "Blaa",
    "Blae",
    "Blaunch",
    "Breata",
    "Breda",
    "Brede",
    "Breesha",
    "Breeshey",
    "Brenta",
    "Bridget",
    "Calibrid",
    "Caly",
    "Calybride",
    "Calycrist",
    "Calycrista",
    "Calyhony",
    "Calypatric",
    "Calypatrick",
    "Calyree",
    "Calyvorra",
    "Calyvorri",
    "Cara",
    "Carola",
    "Carree",
    "Catreena",
    "Catreeney",
    "Cecilia",
    "Cissolt",
    "Conla",
    "Cossot",
    "Creena",
    "Crera",
    "Cristeena",
    "Cristen",
    "Cristory",
    "Diorval",
    "Doona",
    "Drema",
    "Ealee",
    "Ealisaid",
    "Ealish",
    "Edith",
    "Effrica",
    "Eileen",
    "Eilleen",
    "Elena",
    "Emell",
    "Essa",
    "Eunys",
    "Feena",
    "Fenella",
    "Fingola",
    "Finola",
    "Francaig",
    "Fritha",
    "Grayse",
    "Greeba",
    "Ibot",
    "Ibott",
    "Ina",
    "Iney",
    "Inot",
    "Isabell",
    "Isbal",
    "Isot",
    "Isott",
    "Jinn",
    "Joan",
    "Joannia",
    "Johna",
    "Johnet",
    "Jonee",
    "Joney",
    "Jony",
    "Julia",
    "Juliana",
    "Katerina",
    "Kateryn",
    "Kathleen",
    "Katryna",
    "Lilee",
    "Lora",
    "Lucy",
    "Lula",
    "Lulach",
    "Malane",
    "Manana",
    "Margaid",
    "Margaret",
    "Mariod",
    "Marion",
    "Mariot",
    "Matilda",
    "Mawde",
    "Meave",
    "Moira",
    "Moirrey",
    "Mona",
    "More",
    "Moreen",
    "Mureal",
    "Nan",
    "Nancy",
    "Nessa",
    "Nessy",
    "Nora",
    "Onnee",
    "Onnor",
    "Onora",
    "Paaie",
    "Paie",
    "Peggy",
    "Ranhilda",
    "Ratyn",
    "Rein",
    "Reina",
    "Renny",
    "Roseen",
    "Sessott",
    "Sheela",
    "Sissott",
    "Sorcha",
    "Tissot",
    "Tosha",
    "Una",
    "Ursula",
    "Voirrey",
    "Vorana",
    "Vorgel",
    "Vorgell",
    "Ysbal",
  ];
  const word = mergeWords(randomFromArray(nm1), randomFromArray(nm2));
  return word.charAt(0).toUpperCase() + word.slice(1);
}

function randomNameFromParts() {
  const generators = [randomNameFromPartsA, randomNameFromPartsB];
  return randomFromArray(generators)();
}

export function generateRandomPersonName(gender = null) {
  if (randomInt(1, 3) >= 2) {
    return randomNameFromParts();
  }
  const name = faker.name.firstName(gender);
  return name;
}

export function randomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getCanabisName() {
  const names = require("./canabisNames").default;
  return names[randomInt(0, names.length - 1)];
}

export function generateRandomTavernName() {
  const names = require("./restrauntNames").default;
  return names[randomInt(0, names.length - 1)];
}

export function generateRandomMagicShopName() {
  let name = faker.company.companyName();
  const postfixMap = [
    "'s",
    "",
    " Tricks",
    " Scroll House",
    " Wisp",
    " Warehouse",
    " Cloak",
    " Cloak",
    " Supply",
    " Familiars",
    " and More",
    " Solutions",
    " and",
  ];
  let postfix = postfixMap[randomInt(0, postfixMap.length - 1)];

  if (postfix === "and") {
    postfix = `${postfix} ${generateRandomMagicShopName()
      .split("and")[0]
      .trim()}`;
  }

  const prefixMap = [
    "The",
    "The",
    `${generateRandomPersonName()}'s`.replace("s's", "s'"),
    "",
    "",
  ];
  let prefix = prefixMap[randomInt(0, prefixMap.length - 1)];
  return `${prefix} ${name.split(",")[0].split(" ")[0]}${postfix}`.trim();
}

export function generateRandomBlacksmithName() {
  const name = faker.company.companyName().replace("LLC") || "";
  const postFixList = [
    "'s Works",
    ": Things for Wielding",
    " – Metal Artisan",
    "",
    " Family Forging",
    " Brothers Limited",
    ": Things for Slaying",
  ];
  const postFix = postFixList[randomInt(0, postFixList.length - 1)] || "";
  return `${name}${postFix}`.trim();
}

export function generateRandomNpcDescription() {
  const personalityList = [
    "Imaginitive and strategic with a plan for everything",
    "Innovative inventor with an unquenchable thirst for knowledge.",
    "Bold, imaginitive, and strong-willed leader. Always finds a way, or makes one.",
    "Smart and curious who cannot resist an inellectual challenge.",
    "Quiet and mystical, yet very inspiring and tirelessly idealistic.",
    "Poetic, kind, and altruistic. Always eager to help a good cause.",
    "Charasmatic and inspiring leader. Able to mesmerize listeners.",
    "Enthusiastic, creative, and sociable free spirit who can always find a reason to smile.",
    "Practical and fact-minded who's reliability cannot be doubted.",
    "Very dedicated and warm protector always ready to defend loved ones.",
    "Exellent administrator, upsurpassed at managing things, or people.",
    "Extraordinary caring, social, and populare. Always eager to help.",
    "Bold and practical experimentor. Master of all kinds of tools.",
    "Flexible, charming, and artistic. Always ready to explore and experience something new.",
    "Smart, energetic, and perceptive. Truly enjoys living on the edge.",
    "Spontaneous, energetic, and enthusiastic. Life is never boring to them.",
  ];
  const personality = personalityList[randomInt(0, personalityList.length - 1)];
  const classList = require("./data/classes").default;
  const villager = {
    name: "Villager",
  };
  const generatedClass =
    randomInt(0, 5) > 2
      ? villager
      : classList[randomInt(0, classList.length - 1)];
  const raceList = require("./data/races").default;
  const generatedRace = raceList[randomInt(0, raceList.length - 1)];
  const alignmentList = require("./data/alignments").default;
  const generatedAlignment =
    alignmentList[randomInt(0, alignmentList.length - 1)];
  return `
    <p>
        <strong>Personality:</strong>
        ${personality}
    </p>
    <table>
        <tr>
            <td><strong>Gold</strong></td>
            <td><strong>Class</strong></td>
            <td><strong>Race</strong></td>
            <td><strong>Alignment</strong></td>
        </tr>
        <tr>
            <td>${randomInt(2, 200)}</td>
            <td>${(generatedClass || {}).name}</td>
            <td>${(generatedRace || {}).name}</td>
            <td>${(generatedAlignment || {}).name}</td>
        </tr>
    </table>
    `;
}
export function generateRandomTavernDescription() {
  const beers = require("./beerNames").default;
  let drinkList = `
    <li>${beers[randomInt(0, beers.length - 1)]}</li>
    <li>${beers[randomInt(0, beers.length - 1)]}</li>
  `;
  let foodList = "";
  const description = `
    <table>
        <tr>
            <td>
                <strong>Drinks</strong>
            </td>
            <td>
                <strong>Food</strong>
            </td>
        </tr>
            <td>
                <ul>
                    ${drinkList}
                </ul>
            </td>
            <td>
                <ul>
                    ${foodList}
                </ul>
            </td>
        <tr>
    </table>
    `;
  return description;
}
export function generateRandomMagicShopDescription() {}
export function generateRandomBlacksmithDescription() {}

export const generatorMap = {
  npc: () => {
    return abstractGenerator({
      name: `${generateRandomPersonName()} ${generateRandomPersonName()}`,
      type: "NPC",
      left: null,
      top: null,
      description: generateRandomNpcDescription(),
    });
  },
  city: () => {
    const images = [
      "https://i.imgur.com/7UbqwPe.jpeg",
      "https://elasticbeanstalk-us-east-2-350765264819.s3.us-east-2.amazonaws.com/web-uploads/1635880297-p83WAvo4TjvtxjHy-blob",
      "https://elasticbeanstalk-us-east-2-350765264819.s3.us-east-2.amazonaws.com/web-uploads/1639108048-kxUXdHevrPaTMLve-blob",
      "https://i.redd.it/ydjenggt3al71.jpg",
    ];
    return abstractGenerator({
      type: "City",
      image: randomFromArray(images),
      name: generateRandomCityName(),
      generateChildren({ id }) {
        const shops = [generatorMap.tavern()];
        const randomShopsToGenerate = randomInt(8, 12);
        while (shops.length < randomShopsToGenerate) {
          const shopGenerators = [
            generatorMap.tavern,
            generatorMap.tavern,
            generatorMap.tavern,
            generatorMap.tavern,
            generatorMap.tavern,
            generatorMap.blacksmith,
            generatorMap.blacksmith,
            generatorMap["magic-shop"],
            generatorMap["magic-shop"],
            generatorMap["magic-shop"],
          ];
          const randomGenerator = randomFromArray(shopGenerators);
          shops.push({
            parent_map_id: id,
            ...randomGenerator(),
          });
        }
        return shops;
      },
    });
  },
  tavern: () => {
    return abstractGenerator({
      generateChildren({ id }) {
        const children = [];
        const npcsToGenerate = randomInt(2, 7);
        while (children.length < npcsToGenerate) {
          children.push({ parent_map_id: id, ...generatorMap.npc() });
        }
        return children;
      },
      name: generateRandomTavernName(),
      type: "Tavern",
      description: generateRandomTavernDescription(),
    });
  },
  "magic-shop": () => {
    return abstractGenerator({
      generateChildren({ id }) {
        const children = [];
        const npcsToGenerate = randomInt(1, 3);
        while (children.length < npcsToGenerate) {
          children.push({ parent_map_id: id, ...generatorMap.npc() });
        }
        return children;
      },
      name: generateRandomMagicShopName(),
      type: "Magic Shop",
      description: generateRandomMagicShopDescription(),
    });
  },
  blacksmith: () => {
    return abstractGenerator({
      type: "Blacksmith",
      name: generateRandomBlacksmithName(),
      generateChildren({ id }) {
        const children = [];
        const npcsToGenerate = randomInt(1, 3);
        while (children.length < npcsToGenerate) {
          children.push({ parent_map_id: id, ...generatorMap.npc() });
        }
        return children;
      },
      description: generateRandomBlacksmithDescription(),
    });
  },
  "random-shop": () => {
    const generators = ["tavern", "magic-shop", "blacksmith"];
    const generator = generators[randomInt(0, generators.length - 1)];
    return generatorMap[generator]();
  },
};
