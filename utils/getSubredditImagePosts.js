export default function getSubredditImagePosts(
  subreddit,
  page = "",
  goBack = false,
  search = null
) {
  const url = `https://www.reddit.com/r/${subreddit}${
    search ? "/search" : ""
  }.json?${goBack ? "before" : "after"}=${page}${
    search ? `&q=${search}&restrict_sr=1` : ""
  }`;

  return fetch(url).then((res) => res.json());
}
