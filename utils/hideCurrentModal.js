export default function hideCurrentModal() {
  return new Promise((resolve) => {
    document.dispatchEvent(new CustomEvent("hideModal"));
    setTimeout(resolve, 200);
  });
}
