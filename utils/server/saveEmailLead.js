import axios from "axios";
import config from "../../config";

export default async function saveEmailLead(email, source) {
  return await axios.post(`${config.api_base}/api/email-lead`, {
    email,
    source,
  });
}
