export default async function getTodaysAggregatedNews() {
  try {
    let Parser = require("rss-parser");
    let parser = new Parser();
    const feed = await parser.parseURL(
      "https://www.google.com/alerts/feeds/07209286744142978580/12543681142892509334"
    );
    return feed.items;
  } catch (e) {
    console.error(e);
    return [];
  }
}
