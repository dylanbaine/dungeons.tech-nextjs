import getCurrentUserToken from "./getCurrentUserToken";

export default function authHeaders(req) {
  return {
    Authorization: `Bearer ${getCurrentUserToken(req)}`,
  };
}
