import axios from "axios";
import Cookies from "cookies";
import config from "../../config";

export default function getCurrentUserToken(req = null) {
  if (!req) {
    return axios.get("/api/my-token").then((res) => res.data);
  }
  const cookies = new Cookies(req, {});
  const token = cookies.get("api-token");
  if (token === config.null_cookie_value) return null;
  return token;
}
