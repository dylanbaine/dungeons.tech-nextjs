import meiliClient from "../meiliClient";

export default async function createUserIndexes(user) {
  if (!user) return null;
  const meili = meiliClient();
  const userIndexes = [
    {
      indexName: `${user.meili_search_index_namespace}_posts`,
      filterableAttributes: ["parent_map_id", "image", "image_list", "type"],
    },
    {
      indexName: `${user.meili_search_index_namespace}_media`,
      filterableAttributes: ["url"],
    },
  ];
  const results = await Promise.all(
    userIndexes.map(
      ({ indexName, options = {}, filterableAttributes = [] }) => {
        return new Promise(async (resolve, reject) => {
          try {
            await meili.getOrCreateIndex(indexName, options);
            await meili
              .index(indexName)
              .updateFilterableAttributes(filterableAttributes);
            resolve({
              message: `Index ${indexName} set up.`,
              index: await meili.index(indexName),
            });
          } catch (error) {
            reject(error);
          }
        });
      }
    )
  );
  return results;
}
