import Cookies from "cookies";
import redis from './redis'
import config from '../config'

export default async function getUserFromRequest(req, res): Promise<string | null> {
    const cookies = new Cookies(req, res);
    const token = cookies.get("api-token");
    if (!token) {
        return null;
    }
    const legacyCookie = cookies.get("user-data");
    if (legacyCookie) {
        await redis.set(token, legacyCookie);
    }
    const value = await redis.get(token);
    if (value.includes(config.null_cookie_value)) return null;
    return value || null


}
