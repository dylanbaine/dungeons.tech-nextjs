export default function envExecute({ server = () => null, client = () => null }) {
    if (typeof window === 'undefined') {
        return server();
    }
    return client();
}