import randomInt from "./randomInt";

export default function generateRandomWord() {
  function odd(num) {
    return num % 2 === 1;
  }

  const vowels = ["a", "e", "i", "o", "u", "y"];
  const consonants = [
    "b",
    "c",
    "d",
    "f",
    "g",
    "h",
    "j",
    "k",
    "l",
    "m",
    "n",
    "p",
    "r",
    "s",
    "t",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];

  let word = "";
  let number = randomInt(1, 2);
  while (number < randomInt(4, 9)) {
    if (odd(number)) {
      word += vowels[randomInt(0, vowels.length - 1)];
    } else {
      word += consonants[randomInt(0, vowels.length - 1)];
    }
    number++;
  }
  return word;
}
