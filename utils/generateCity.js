import { generatorMap } from "./generate";

export default async function generateACity(
  mainItemDateOverride = {},
  mapRepository
) {
  const { storeManyUserMaps } = mapRepository;
  const generated = generatorMap["city"]();
  const allChildren = [];
  function queueItemChildren(item) {
    item.children.forEach((child) => {
      if (child.children) {
        queueItemChildren(child);
      }
      allChildren.push({
        id: child.id,
        parent_map_id: item.id,
        name: child.name,
        type: child.type,
        description: child.description,
        type: child.type,
        left: child.left,
        top: child.top,
      });
    });
  }

  queueItemChildren(generated);
  const mainItem = {
    id: generated.id,
    name: generated.name,
    image: generated.image,
    description: generated.description,
    type: generated.type,
    left: generated.left,
    top: generated.top,
    type: generated.type,
    ...mainItemDateOverride,
  };
  await storeManyUserMaps([mainItem, ...allChildren]);
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(generated);
    }, 700);
  });
}
