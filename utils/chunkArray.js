import paginateArray from "./paginateArray";

export default function chunkArray(array = [], chunkSize = 20) {
  const chunks = [];
  let currentPage = 1;
  let lastPage = 2;
  while (currentPage <= lastPage) {
    const pagination = paginateArray(array, chunkSize, currentPage);
    currentPage++;
    chunks.push(pagination.items);
    lastPage = pagination.totalPages;
  }
  return chunks;
}
