export default function listenForKeyInput(e, key, callback) {
  if (e.key === key) {
    callback(e);
  }
}
