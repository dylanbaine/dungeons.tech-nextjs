export default function limitStringLength(string, length = 20, fill = "...") {
  if (string.length <= length) {
    return string;
  }
  return string.split("").slice(0, length).join("") + fill;
}
