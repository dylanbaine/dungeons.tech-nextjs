import dynamic from "next/dynamic";
import React from "react";

const widgetsMap = {
  spells: dynamic(() => import("../../components/PostWidgets/SpellsWidget")),
  hitPoints: dynamic(() =>
    import("../../components/PostWidgets/HitPointsWidget")
  ),
  equipment: dynamic(() =>
    import("../../components/PostWidgets/EquipmentWidget")
  ),
};
export function renderWidget(
  widgetName,
  initialData,
  onUpdate,
  allowEdit = true
) {
  const WidgetComponent = widgetsMap[widgetName];
  if (WidgetComponent) {
    return (
      <React.Fragment>
        <h3 className="capitalize margin-none margin-sm--bottom">
          {widgetName}
        </h3>
        <WidgetComponent
          onUpdate={onUpdate}
          initialData={initialData}
          allowEdit={allowEdit}
        />
      </React.Fragment>
    );
  }
  return <div>Widget not found</div>;
}
export default widgetsMap;
