export default async function getAllObjectChildren(parent, repo) {
  const toDelete = [];
  return new Promise(async (resolve, reject) => {
    try {
      toDelete.push(parent);
      const children = await repo.getMapChildren(parent);
      (
        await await Promise.all(
          children.map((child) => {
            return getAllObjectChildren(child, repo);
          })
        )
      ).forEach((child) => toDelete.push(child));
      resolve(toDelete.flat());
    } catch (error) {
      reject(error);
    }
  });
}
