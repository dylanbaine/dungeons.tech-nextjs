export default function fileDataToFileObject(dataurl, filename) {
  if (typeof dataurl !== "string") {
    return null;
  }
  const arr = dataurl.split(",");
  if (arr.length === 1) {
    return dataurl;
  }
  let mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, { type: mime });
}
