export default function paginateArray(array = [], size = 20, groupNumber = 1) {
  const items = array.slice((groupNumber - 1) * size, groupNumber * size);
  const totalPages = Math.ceil(array.length / size);
  return {
    totalPages,
    items,
  };
}
