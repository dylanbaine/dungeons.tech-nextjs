const defaultPostTypes = [
  "NPC",
  "City",
  "Tavern",
  "Blacksmith",
  "Magic Shop",
  "Place",
  "Folder",
  "Thing",
];
export default defaultPostTypes;
