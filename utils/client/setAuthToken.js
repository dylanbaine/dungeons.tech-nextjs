export default function setAuthToken(token, cookies) {
  if (!token || !cookies) return;
  const expires = new Date();
  expires.setFullYear(expires.getFullYear() + 5);
  cookies.set("api-token", token, {
    expires,
  });
}
