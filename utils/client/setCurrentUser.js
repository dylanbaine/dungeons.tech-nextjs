export default function setCurrentUser(user, cookies = null) {
  if (!user) return;
  if (cookies) {
    const expires = new Date();
    expires.setFullYear(expires.getFullYear() + 5);
    cookies.set("user-data", JSON.stringify(user), {
      expires,
    });
  } else if (typeof localStorage !== "undefined") {
    localStorage.setItem("user", JSON.stringify(user));
  } else {
    console.error("setCurrentUser did not run");
  }
}
