export function vttKey(key) {
  return `${process.env.APP_ENV || "prod"}:published-vtt:${key}`;
}
