export default function normalizeMediaUrl(url) {
  if (!url) return null;
  if (
    url.includes(
      "https://elasticbeanstalk-us-east-2-350765264819.s3.us-east-2.amazonaws.com/"
    )
  ) {
    return url.replace(
      "https://elasticbeanstalk-us-east-2-350765264819.s3.us-east-2.amazonaws.com/",
      "/uploaded-media/"
    );
  } else if (
    url.includes("https://res.cloudinary.com/drpocweiq/image/upload/")
  ) {
    return url.replace(
      "https://res.cloudinary.com/drpocweiq/image/upload/",
      "/static-media/"
    );
  } else if (url.includes("https://i.redd.it/")) {
    return url.replace("https://i.redd.it/", "/reddit-media/");
  }
  return url;
}
