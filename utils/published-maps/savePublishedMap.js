import loggedInMapRepository from "../../map-repository/client-side/loggedInMapRepository";
import redis from "../redis";
import { vttKey } from "../vttKey";

function publishToPusher(object, user) {
  const Pusher = require("pusher");
  const pusher = new Pusher({
    appId: "994462",
    key: "27f7669159149ab0646b",
    secret: "74b1e67f9f73438fa244",
    cluster: "us2",
    useTLS: true,
  });
  pusher.trigger("Dungeons.Tech-production", vttKey(user.vttLink), {
    message: JSON.stringify(object),
  });
}

export default async function savePublishedMap({
  objectId,
  user,
  phase = null,
  initiative = null,
}) {
  const repository = loggedInMapRepository(user);
  const object = await repository.findPoint(objectId);
  if (!object) {
    return null;
  }
  object.childrenMaps = await repository.getMapChildren(object);
  const selectedPhase = phase && object.phases && object.phases[phase];
  if (selectedPhase) {
    object.currentImage = selectedPhase;
  } else {
    object.currentImage = object.image;
  }
  if (initiative) {
    object.initiative = initiative;
  }
  console.log("published", object.currentImage);
  publishToPusher(object, user);
  const key = vttKey(user.vttLink);
  await redis.setex(key, 60 * 60 * 24, JSON.stringify(object));
  return Promise.resolve(object);
}
