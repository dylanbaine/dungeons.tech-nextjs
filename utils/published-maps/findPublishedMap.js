import { vttKey } from "../vttKey";

export default async function findPublishedMap(vttLink) {
  if (typeof window === "undefined") {
    const key = vttKey(vttLink);
    const redis = require("../redis").default;
    const object = await redis.get(key);
    return JSON.parse(object);
  }
  return await fetch(`/api/user-vtt/find?id=${vttLink}`).then((res) =>
    res.json()
  );
}
