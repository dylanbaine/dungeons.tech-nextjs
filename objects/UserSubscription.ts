export default class UserSubscription {
    public id: String;
    public subscriptionId: String;
    public intervalPaid: String;
    public intervalCost: Number;
    public intervalName: String;
    public name: String;

    public static fromStripe(stripeData): UserSubscription {
        const subscription = new UserSubscription();
        subscription.id = stripeData.plan.id;
        subscription.subscriptionId = stripeData.id;
        subscription.intervalPaid = stripeData.plan.interval === 'year' ? "Yearly" : "Monthly";
        subscription.intervalName = stripeData.plan.interval;
        subscription.intervalCost = stripeData.plan.amount / 100;
        subscription.name = stripeData.plan.nickname;
        return subscription;
    }

    toString(): string {
        return JSON.stringify(this);
    }

    toObject(): Object {
        return JSON.parse(this.toString());
    }
}