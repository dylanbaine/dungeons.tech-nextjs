import { MeiliSearch, Index } from "meilisearch";
import stripe from "../stripe/stripe";
import meiliClient from '../utils/meiliClient';
import getUserFromRequest from '../utils/getUserFromRequest.ts';
import axios from "axios";
import UserSubscription from "./UserSubscription.ts";

export default class User {
    public id: number;
    public name: string;
    public email: string;
    public meili_search_index_namespace: string;
    public stripe_id: string;
    #_customerData: Object | null = null;

    public static fromContext(context) {
        const user = new User();
        user.id = context.id;
        user.name = context.name;
        user.email = context.email;
        user.meili_search_index_namespace = context.meili_search_index_namespace;
        return user;
    }

    public static fromCookie(cookieValue: string): User | null {
        if (!cookieValue) {
            return null;
        }
        const user = new User();
        const cookie = JSON.parse(cookieValue);
        user.id = cookie.id;
        user.name = cookie.name;
        user.email = cookie.email;
        user.meili_search_index_namespace = cookie.meili_search_index_namespace;
        user.stripe_id = cookie.stripe_id;
        return user;
    }

    public static async fromRequest(request): Promise<User | null> {
        const data = await getUserFromRequest(request, {});
        if (!data) {
            return null;
        }
        return User.fromCookie(data);
    }

    async getStripeData() {
        if (typeof window !== "undefined") {
            return axios.get('/api/me').then(res => res.data.paymentProfile);
        }
        if (!this.#_customerData) {
            console.log('get stripe data');
            const customer = await stripe.customers.retrieve(this.stripe_id);
            const cards = await stripe.paymentMethods.list({
                customer: customer.id,
                type: "card",
            });
            this.#_customerData = {
                subscriptions: customer.subscriptions.data,
                cards: cards.data
            }
        }
        return this.#_customerData;
    }

    async getSubscriptions(): Promise<UserSubscription[] | null> {
        const { subscriptions } = await this.getStripeData();
        return subscriptions.map(UserSubscription.fromStripe);
    }

    async getSubscriptionFeatures(): Promise<Object> {
        let media = 3;
        let objects = 300;
        let mapPhases = false;
        const subscriptions = await this.getSubscriptions();
        if (subscriptions.find(sub => sub.name.toLowerCase().includes('lite'))) {
            objects = 1000;
            media = 10;
        } else if (subscriptions.find(sub => sub.name.toLowerCase().includes('premium'))) {
            objects = 4000000;
            media = 50;
            mapPhases = true;
        }
        return {
            media,
            objects,
            mapPhases,
        };
    }

    public get vttLink(): string | null {
        const vttUid =
            this.id &&
            new Buffer(this.id.toString()).toString("base64").replace(/=/g, "");
        return (vttUid && `/published-vtt/${vttUid}`) || null;
    }

    public meili(indexName): Index | MeiliSearch {
        return meiliClient(this, indexName);
    }

    public get searchIndexes(): string[] {
        return this.meili_search_index_namespace
            ? [
                `${this.meili_search_index_namespace}_posts`,
                `${this.meili_search_index_namespace}_media`,
            ]
            : [];
    }
}
