const nullMapsRepository = {
  type: "waiting...",
  async syncWithRemote() {},
  async getByType(type, parentId) {},
  async getAllTypes(parentId) {},
  async clearMaps() {},
  async getCurrentUserMaps() {},
  async saveMaps(maps, shouldSync = false) {},
  async getTopLevelMaps() {},
  async count() {},
  setCurrentMainMap(map) {},
  async getMainMap() {},
  async getAllChildrenMaps() {},
  async getMapChildren(parent) {},
  async getMapParent(child) {},
  async deletePoint(mapDeleting) {},
  async findPoint(id) {},
  async updatePoint(point) {},
  async storeUserMap(newMap) {},
  async storeManyUserMaps(newMaps) {},
};

export default nullMapsRepository;
