import axios from "axios";

const nullPlaceholder = "00000";

function normalizeItem(item) {
  if (!item.parent_map_id) {
    item.parent_map_id = nullPlaceholder;
  }
  return item;
}

export default function loggedInMapRepository(user) {
  async function getCurrentUserMaps(config = { limit: 1000 }) {
    const docs = await user.meili().getDocuments(config);
    return docs;
  }

  async function saveMaps(maps) {
    return user.meili().addDocuments(maps);
  }

  async function getMapChildren(parent) {
    try {
      return (
        await user.meili().search("", {
          filter: `parent_map_id = ${parent.id}`,
          limit: 100,
        })
      ).hits;
    } catch (e) {
      return [];
    }
  }

  async function findPoint(id) {
    try {
      const found = await user.meili().getDocument(id);
      if (found.id) {
        return found;
      }
    } catch (e) {}
    return null;
  }

  async function count() {
    const res = await user.meili().search("", {
      limit: 0,
    });
    return res.nbHits;
  }

  async function canCreateMaps() {
    const countMaps = await count();
    const { objects } = await user.getSubscriptionFeatures();
    console.log({ objects, countMaps });
    if (countMaps >= objects) {
      if (
        confirm(
          "You have reached your storage limit. Update your subscription to create more."
        )
      ) {
        window.location.href = "/pricing";
      }
      return false;
    }
    return true;
  }

  async function getByType(type, parentId = null) {
    let parentFilter = "";
    if (parentId) {
      parentFilter = `AND parent_map_id = ${parentId}`;
    }
    const filter = `type = "${type}" ${parentFilter}`.trim();
    console.log({ filter });
    return (
      await user.meili().search("", {
        filter,
        limit: 100,
      })
    ).hits;
  }

  async function getAllTypes(parentId = null) {
    const searchConfig = {
      limit: 0,
      facetsDistribution: ["type"],
    };
    if (parentId) {
      searchConfig.filter = `parent_map_id = ${parentId}`;
    }

    const results = await user.meili().search("", searchConfig);
    return Object.keys(results.facetsDistribution.type || {});
  }

  return {
    type: "logged in",

    count,

    getByType,

    getAllTypes,

    async syncWithRemote() {
      console.log("syncing with remote");
      const localMaps = () => JSON.parse(localStorage.getItem("maps")) || [];
      if (localMaps().length > 0) {
        await saveMaps(localMaps().map(normalizeItem));
        localStorage.setItem("maps-backup", JSON.stringify(localMaps()));
        localStorage.setItem("maps", "[]");
      }
    },

    async clearMaps() {
      alert("Not allowed");
    },

    getCurrentUserMaps,

    saveMaps,

    async getTopLevelMaps() {
      return (
        (
          await user.meili().search("", {
            filter: `parent_map_id = ${nullPlaceholder}`,
          })
        ).hits || []
      );
    },

    setCurrentMainMap(map) {
      //
    },

    async getMainMap() {
      return {} || null;
    },

    async getAllChildrenMaps() {
      return [];
    },

    getMapChildren,

    async getMapParent(child) {
      if (child.parent_map_id && child.parent_map_id !== nullPlaceholder) {
        return await findPoint(child.parent_map_id);
      }
      return null;
    },

    async deletePoint(mapDeleting) {
      axios.get(`/api/delete-object/${mapDeleting.id}`);
      return Promise.resolve();
    },

    findPoint,

    async updatePoint(point) {
      await user.meili().addDocuments([normalizeItem(point)]);
    },

    async storeUserMap(newMap) {
      if (await canCreateMaps()) {
        await user.meili().addDocuments([normalizeItem(newMap)]);
        return newMap;
      }
    },

    async storeManyUserMaps(newMaps) {
      if (await canCreateMaps()) {
        await user.meili().addDocuments(newMaps.map(normalizeItem));
      }
    },
  };
}
