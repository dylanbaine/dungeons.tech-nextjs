import getAllObjectChildren from "../../utils/getAllObjectChildren";
let triggerlimmit = 5;
let loginPromptTriggerCounter = 0;
let loginPromptTrottle;

export default function loggedOutmapRepository() {
  function randomlyPromptUserToLogin() {
    clearInterval(loginPromptTrottle);
    loginPromptTrottle = setTimeout(() => {
      loginPromptTriggerCounter = loginPromptTriggerCounter + 1;
      clearInterval(window.propmptLoginTimout);
      if (loginPromptTriggerCounter > triggerlimmit) {
        loginPromptTriggerCounter = 0;
        if (
          confirm(
            "Liking Dungeons.Tech? Create an account to sync data across all of your devices."
          )
        ) {
          window.location.href = "/login";
        } else {
          triggerlimmit = triggerlimmit + 3;
        }
      }
    }, 500);
  }

  async function getCurrentUserMaps() {
    randomlyPromptUserToLogin();
    return JSON.parse(localStorage.getItem("maps") || "[]");
  }

  async function count() {
    return (await getCurrentUserMaps()).length;
  }

  async function saveMaps(maps) {
    randomlyPromptUserToLogin();
    localStorage.setItem("maps", JSON.stringify(maps));
  }

  async function getTopLevelMaps() {
    const allMaps = await getCurrentUserMaps();
    return (allMaps || []).filter((map) => {
      return !map.parent_map_id;
    });
  }
  async function getMapChildren(parent) {
    randomlyPromptUserToLogin();
    const allMaps = await getCurrentUserMaps();
    return (allMaps || []).filter((map) => {
      return map.parent_map_id == parent.id;
    });
  }

  async function getByType(type, parentId) {
    randomlyPromptUserToLogin();
    const allMaps = parentId
      ? await getMapChildren({ id: parentId })
      : await getCurrentUserMaps();
    return (allMaps || []).filter((map) => {
      return map.type == type;
    });
  }

  async function getAllTypes(parentId) {
    const types = [];
    const allMaps = parentId
      ? await getMapChildren({ id: parentId })
      : await getCurrentUserMaps();
    allMaps.forEach((map) => {
      if (!types.includes(map.type)) {
        types.push(map.type);
      }
    });
    return types;
  }

  return {
    type: "client side, logged out",

    count,

    getByType,

    getAllTypes,

    async syncWithRemote() {},

    async clearMaps() {
      saveMaps([]);
    },

    getCurrentUserMaps,

    saveMaps,

    getTopLevelMaps,

    setCurrentMainMap(map) {
      localStorage.setItem("current-main-map", map.id.toString());
    },

    async getMainMap() {
      const topLevelMaps = await getTopLevelMaps();
      let currentMainMap = localStorage.getItem("current-main-map");
      if (!currentMainMap) {
        const firstToplevelMap = topLevelMaps[0];
        if (!firstToplevelMap) {
          throw "no maps!";
        }
        currentMainMap = firstToplevelMap.id;
        setCurrentMainMap(firstToplevelMap);
      }
      return topLevelMaps.find((m) => m.id == currentMainMap);
    },

    async getAllChildrenMaps() {
      randomlyPromptUserToLogin();
      const allMaps = await getCurrentUserMaps();
      const mainMap = await getMainMap();
      return (allMaps || []).filter((map) => {
        return map.parent_map_id == mainMap.id;
      });
    },

    getMapChildren,

    async getMapParent(child) {
      randomlyPromptUserToLogin();
      const allMaps = await getCurrentUserMaps();
      return (allMaps || []).find((map) => {
        return map.id == child.parent_map_id;
      });
    },

    async deletePoint(mapDeleting) {
      randomlyPromptUserToLogin();
      const allMaps = await getCurrentUserMaps();
      const allChildren = await getAllObjectChildren(mapDeleting, {
        getMapChildren,
      });
      console.log({ allChildren });
      const newMaps = allMaps.filter((storedMap) => {
        if (
          allChildren.find(
            (child) =>
              child.id == storedMap.id ||
              storedMap.parent_map_id == mapDeleting.id
          ) ||
          storedMap.id == mapDeleting.id
        ) {
          return false;
        }
        return true;
      });
      saveMaps(newMaps);
    },

    async findPoint(id) {
      randomlyPromptUserToLogin();
      return (await getCurrentUserMaps()).find((p) => p.id == id) || null;
    },

    async updatePoint(point) {
      randomlyPromptUserToLogin();
      delete point.children;
      const allMaps = await getCurrentUserMaps();
      const newMapsArray = allMaps.map((m) => {
        if (m.id == point.id) {
          return {
            ...m,
            ...point,
          };
        }
        return m;
      });
      saveMaps(newMapsArray);
    },

    async storeUserMap(newMap) {
      const mapsCount = await count();
      if (mapsCount >= 300) {
        const register = confirm(
          "You have reached the maximum number of maps you can store. Please make an account to create more."
        );
        if (register) {
          window.location.href = "/register";
        }
        return {};
      } else {
        randomlyPromptUserToLogin();
        const currentMaps = await getCurrentUserMaps();
        if (!newMap.id) {
          newMap.id = Date.now();
        }
        currentMaps.push(newMap);
        saveMaps(currentMaps);
        return newMap;
      }
    },

    async storeManyUserMaps(newMaps) {
      randomlyPromptUserToLogin();
      const currentMaps = await getCurrentUserMaps();
      const mapsCount = await count();
      if (mapsCount >= 300) {
        const register = confirm(
          "You have reached the maximum number of maps you can store. Please make an account to create more."
        );
        if (register) {
          window.location.href = "/register";
        }
      } else {
        await saveMaps([...currentMaps, ...newMaps]);
      }
    },
  };
}
