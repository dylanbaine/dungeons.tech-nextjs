import axios from 'axios';
import User from './objects/User'
import getCurrentUserToken from './utils/server/getCurrentUserToken';


async function uploadFile(file) {
    const form = new FormData();
    form.append("image", file);
    const { token } = await getCurrentUserToken();
    const uploadRes = await axios
        .post("/api/app/upload-media", form, {
            headers: {
                authorization: `Bearer ${token}`,
                "content-type": "multipart/form-data",
            },
        })
        .then((r) => r.data);
    return uploadRes.imagePath;
}
interface Media {
    id: string;
    title: string;
    description: string;
    url: string | null;
}

export interface MediaRepository {
    getAllMedia(): Promise<Media[]>;
    saveMedia(media: Media): void;
    mediaCount(): Promise<number>;
    getDocumentImages(): Promise<String[]>;
    deleteMedia(id: string): void;
    canCreateMaps(showCTA: boolean): Promise<boolean>;
}

export default function mediaRepository(user: User): MediaRepository {
    const meili = user.meili("media");


    async function canCreateMaps(showCTA = true) {
        const countMedia = await mediaCount();
        const { media } = await user.getSubscriptionFeatures();
        if (countMedia >= media) {
            if (
                showCTA &&
                confirm(
                    "You have reached your storage limit. Update your subscription to create more."
                )
            ) {
                window.location.href = "/pricing";
            }
            return false;
        }
        return true;
    }

    async function mediaCount(): Promise<number> {
        const res = await meili.search("", {
            limit: 0,
        });
        return res.nbHits;
    }

    async function getAllMedia(): Promise<Media[]> {
        const res = await meili.search("", { limit: 300 });
        return res.hits;
    }

    async function saveMedia(data: Media, file): void {
        if (await canCreateMaps()) {
            data.url = await uploadFile(file);
            meili.addDocuments([data]);
        }
    }

    function deleteMedia(id: string): void {
        meili.deleteDocuments([id]);
    }

    async function getDocumentImages(): Promise<String[]> {
        const results = await user.meili().search("", {
            limit: 0,
            facetsDistribution: ["image"]
        });
        return Object.keys(results.facetsDistribution.image);
    }


    return {
        canCreateMaps,
        mediaCount,
        getAllMedia,
        saveMedia,
        deleteMedia,
        getDocumentImages,
    };
}
